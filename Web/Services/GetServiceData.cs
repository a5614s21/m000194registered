﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.CgmhAppService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web.Models;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.Caching;
using System.Configuration;

//using encoding_DLL;

namespace Web.Services
{
    public class GetServiceData : Controller
    {
        public RMS_WS AppServices = new RMS_WS();//宣告api
        //public encoding_DLL.RMS_WS AppServices= new encoding_DLL.RMS_WS();//宣告api




        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;//更改較少資訊Cache使用
        protected float CacheSaveHour = float.Parse(ConfigurationManager.ConnectionStrings["CacheSaveHour"].ConnectionString);//Cache 儲存(小時)

        /// <summary>
        /// 回傳authKey
        /// </summary>
        /// <returns></returns>
        public string GetAuthKey()
        {
            //string AAA = AppServices.WS100("IPL", "IPLcgmh");
            //string BBB = HttpUtility.HtmlEncode(AAA);
            //HttpUtility.HtmlEncode
            JObject authKeyResponse = JObject.Parse(AppServices.WS100("IPL", "IPLcgmh"));
            string authKey = HttpUtility.HtmlEncode(authKeyResponse["resultList"].ToString());
            return authKey;
        }

        /// <summary>
        /// 組合cache名稱
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetCacheName(Dictionary<string, string> data)
        {
            string re = "";
            foreach (KeyValuePair<string, string> item in data)
            {
                re += item.Value;
            }
            return re;

        }


        /// <summary>
        /// WS01 醫院清單
        /// </summary>
        /// <returns></returns>
        public dynamic GetHospital()
        {
            string CacheName = "GetHospital";

            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    //JObject Response = JObject.Parse(AppServices.WS01("zh-TW", GetAuthKey()));
                    //JObject Response = JObject.Parse(AppServices.WS01_2("zh-TW", GetAuthKey()));
                    JObject Response = JObject.Parse(AppServices.WS01_2_EN("zh-TW", GetAuthKey()));

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<HospitalModel> resultList = JsonConvert.DeserializeObject<List<HospitalModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// WS02 取得各院科別
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="deptGrouptID"></param>
        /// <returns></returns>
        public dynamic GetDept(string hospitalID , string deptGrouptID , string types = "WS02_2")
        {

            string CacheName = "GetDept" + hospitalID + deptGrouptID + types;

            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = new JObject();

                    if (types == "WS02_2")
                    {
                        //Response = JObject.Parse(AppServices.WS02_2(hospitalID, "", deptGrouptID, GetAuthKey()));
                        Response = JObject.Parse(AppServices.WS02_2_EN(hospitalID, "", deptGrouptID, GetAuthKey()));
                    }
                    else
                    {
                        Response = JObject.Parse(AppServices.WS02(hospitalID, "", deptGrouptID, GetAuthKey()));
                    }


                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DeptModel> resultList = JsonConvert.DeserializeObject<List<DeptModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }
        public dynamic GetDept_video(string hospitalID, string deptGrouptID, string types = "WS02_2")
        {

            string CacheName = "GetDept_videoshow" + hospitalID + deptGrouptID + types;

            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = new JObject();

                    if (types == "WS02_2")
                    {
                        //Response = JObject.Parse(AppServices.WS02_2_videoshow(hospitalID, "", deptGrouptID, GetAuthKey()));
                        Response = JObject.Parse(AppServices.WS02_2_videoshow_EN(hospitalID, "", deptGrouptID, GetAuthKey()));
                    }
                    else
                    {
                        Response = JObject.Parse(AppServices.WS02(hospitalID, "", deptGrouptID, GetAuthKey()));
                    }


                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DeptModel> resultList = JsonConvert.DeserializeObject<List<DeptModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        //return Response;


                        List<DeptModel> resultList = new List<DeptModel>();
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// WS03 取得門診表看診週別
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetSchedulePeriod(string hospitalID)
        {
            string CacheName = "GetSchedulePeriod" + hospitalID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(AppServices.WS03(hospitalID, GetAuthKey()));
                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<SchedulePeriodModel> resultList = JsonConvert.DeserializeObject<List<SchedulePeriodModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// WS04 查詢門診資料
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="opdBeginDate"></param>
        /// <param name="opdEndDate"></param>
        /// <param name="doctorID"></param>
        /// <param name="deptID"></param>
        /// <returns></returns>
        public dynamic GetOPDSchdule(string hospitalID , string opdBeginDate, string opdEndDate, string doctorID, string deptID)
        {
            //JObject Response = JObject.Parse(AppServices.WS04(hospitalID ,"", opdBeginDate , opdEndDate , doctorID , deptID, GetAuthKey()));
            string CacheName = "GetOPDSchdule" + hospitalID + opdBeginDate + opdEndDate + doctorID + deptID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    //JObject Response = JObject.Parse(AppServices.WS04_2(hospitalID, "", opdBeginDate, opdEndDate, doctorID, deptID, GetAuthKey()));
                    JObject Response = JObject.Parse(AppServices.WS04_2_EN(hospitalID, "", opdBeginDate, opdEndDate, doctorID, deptID, GetAuthKey()));

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<OPDSchduleModel> resultList = JsonConvert.DeserializeObject<List<OPDSchduleModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// WS04 查詢門診資料
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="opdBeginDate"></param>
        /// <param name="opdEndDate"></param>
        /// <param name="doctorID"></param>
        /// <param name="deptID"></param>
        /// <returns></returns>
        public dynamic GetOPDSchdule_video(string hospitalID, string opdBeginDate, string opdEndDate, string doctorID, string deptID)
        {
            //JObject Response = JObject.Parse(AppServices.WS04(hospitalID ,"", opdBeginDate , opdEndDate , doctorID , deptID, GetAuthKey()));
            string CacheName_video = "GetOPDSchdule_video" + hospitalID + opdBeginDate + opdEndDate + doctorID + deptID;
            try
            {
                if (cacheContainer.Get(CacheName_video) == null)
                {
                    //JObject Response = JObject.Parse(AppServices.WS04_2_videoonly(hospitalID, "", opdBeginDate, opdEndDate, doctorID, deptID, GetAuthKey()));
                    JObject Response = JObject.Parse(AppServices.WS04_2_videoonly_EN(hospitalID, "", opdBeginDate, opdEndDate, doctorID, deptID, GetAuthKey()));
                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<OPDSchduleModel> resultList = JsonConvert.DeserializeObject<List<OPDSchduleModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName_video, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName_video);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }



        /// <summary>
        /// 掛號查詢
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetRegQuery(Dictionary<string, string> data)
        {
            //JObject Response = JObject.Parse(
            //    AppServices.WS07(
            //        data["hospitalID"].ToString(), 
            //        "", 
            //        data["isFirst"].ToString(),
            //        data["patNumber"].ToString(), 
            //        data["idNumber"].ToString(),
            //        data["idType"].ToString(), 
            //        data["birthday"].ToString(), 
            //        GetAuthKey())
            //        );

            //JObject Response = JObject.Parse(
            //   AppServices.WS07_videotel(
            //       data["hospitalID"].ToString(),
            //       "",
            //       data["isFirst"].ToString(),
            //       data["patNumber"].ToString(),
            //       data["idNumber"].ToString(),
            //       data["idType"].ToString(),
            //       data["birthday"].ToString(),
            //       GetAuthKey())
            //       );
            //JObject Response = JObject.Parse(
            //   AppServices.WS07_videotel2(
            //       data["hospitalID"].ToString(),
            //       "",
            //       data["isFirst"].ToString(),
            //       data["patNumber"].ToString(),
            //       data["idNumber"].ToString(),
            //       data["idType"].ToString(),
            //       data["birthday"].ToString(),
            //       GetAuthKey())
            //       );

            JObject Response = JObject.Parse(
              AppServices.WS07_videotel2_EN(
                  data["hospitalID"].ToString(),
                  "",
                  data["isFirst"].ToString(),
                  data["patNumber"].ToString(),
                  data["idNumber"].ToString(),
                  data["idType"].ToString(),
                  data["birthday"].ToString(),
                  GetAuthKey())
                  );


            if (Response["isSuccess"].ToString() == "Y")
            {
                List<RegQueryModel> resultList = JsonConvert.DeserializeObject<List<RegQueryModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS08 取消掛號
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDoRegCancel(Dictionary<string, string> data)
        {
            //JObject Response = JObject.Parse(
            //    AppServices.WS08(
            //        data["hospitalID"].ToString(), 
            //        "", 
            //        data["patNumber"].ToString(),
            //        data["idNumber"].ToString(), 
            //        data["idType"].ToString(), 
            //        data["birthday"].ToString(),
            //        data["opdDate"].ToString(),
            //        data["deptID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["doctorID"].ToString(),
            //        data["roomID"].ToString(),               
            //        GetAuthKey(),
            //        data["ip"].ToString())
            //        );
            //JObject Response = JObject.Parse(
            //    AppServices.WS08(
            //        HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
            //        "",
            //        HttpUtility.HtmlEncode(data["patNumber"].ToString()),
            //        HttpUtility.HtmlEncode(data["idNumber"].ToString()),
            //        HttpUtility.HtmlEncode(data["idType"].ToString()),
            //        HttpUtility.HtmlEncode(data["birthday"].ToString()),
            //        HttpUtility.HtmlEncode(data["opdDate"].ToString()),
            //        HttpUtility.HtmlEncode(data["deptID"].ToString()),
            //        HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
            //        HttpUtility.HtmlEncode(data["doctorID"].ToString()),
            //        HttpUtility.HtmlEncode(data["roomID"].ToString()),
            //        GetAuthKey(),
            //        HttpUtility.HtmlEncode(data["ip"].ToString()))
            //        );
            JObject Response = JObject.Parse(
                AppServices.WS08_EN(
                    HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
                    "",
                    HttpUtility.HtmlEncode(data["patNumber"].ToString()),
                    HttpUtility.HtmlEncode(data["idNumber"].ToString()),
                    HttpUtility.HtmlEncode(data["idType"].ToString()),
                    HttpUtility.HtmlEncode(data["birthday"].ToString()),
                    HttpUtility.HtmlEncode(data["opdDate"].ToString()),
                    HttpUtility.HtmlEncode(data["deptID"].ToString()),
                    HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
                    HttpUtility.HtmlEncode(data["doctorID"].ToString()),
                    HttpUtility.HtmlEncode(data["roomID"].ToString()),
                    GetAuthKey(),
                    HttpUtility.HtmlEncode(data["ip"].ToString()))
                    );


            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DoRegCancelModel> resultList = JsonConvert.DeserializeObject<List<DoRegCancelModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS09 取得診間看診進度
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetOPDProgress(Dictionary<string, string> data)
        {
        //    JObject Response = JObject.Parse(
        //        AppServices.WS09(
        //            data["hospitalID"].ToString(),
        //            "",                   
        //            data["deptID"].ToString(),
        //            data["opdTimeID"].ToString(),
        //            GetAuthKey())
        //            );

            JObject Response = JObject.Parse(
                AppServices.WS09_EN(
                    data["hospitalID"].ToString(),
                    "",
                    data["deptID"].ToString(),
                    data["opdTimeID"].ToString(),
                    GetAuthKey())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<OPDProgressModel> resultList = JsonConvert.DeserializeObject<List<OPDProgressModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// WS10 取得診間看診進度依病歷號
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetOPDProgress_Patient(Dictionary<string, string> data)
        {
            //JObject Response = JObject.Parse(
            //    AppServices.WS10(
            //        data["hospitalID"].ToString(),
            //        "",
            //        data["patNumber"].ToString(),
            //        data["idNumber"].ToString(),
            //        data["idType"].ToString(),
            //        data["birthday"].ToString(),
            //        GetAuthKey())
            //        );
            JObject Response = JObject.Parse(
               AppServices.WS10_EN(
                   data["hospitalID"].ToString(),
                   "",
                   data["patNumber"].ToString(),
                   data["idNumber"].ToString(),
                   data["idType"].ToString(),
                   data["birthday"].ToString(),
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<OPDProgressPatientModel> resultList = JsonConvert.DeserializeObject<List<OPDProgressPatientModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }



        /// <summary>
        /// WS11 病歷查詢
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="patNumber"></param>
        /// <param name="idNumber"></param>
        /// <param name="idType"></param>
        /// <param name="birthday"></param>
        /// <returns></returns>
       
        public dynamic PatQuery(string hospitalID, string patNumber, string idNumber, string idType, string birthday)
        {
            //JObject Response = JObject.Parse(AppServices.WS11(hospitalID, "", patNumber, idNumber, idType, birthday, GetAuthKey()));
            JObject Response = JObject.Parse(AppServices.WS11_2_EN(hospitalID, "", patNumber, idNumber, idType, birthday,"", GetAuthKey()));

            //JObject Response = JObject.Parse(AppServices.WS11(HttpUtility.HtmlEncode(hospitalID), "", HttpUtility.HtmlEncode(patNumber) , HttpUtility.HtmlEncode(idNumber) , HttpUtility.HtmlEncode(idType) , HttpUtility.HtmlEncode(birthday), GetAuthKey()));
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<PatQueryModel> resultList = JsonConvert.DeserializeObject<List<PatQueryModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// WS05 復診掛號
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public dynamic DoReg(FormCollection form)
        {
            JObject Response = null;        

            try
            {
                //Response = JObject.Parse(AppServices.WS05(
                //              form["hospitalID"].ToString(),
                //              "",
                //              form["patNumber"].ToString(),
                //              form["idNumber"].ToString(),
                //              form["idType"].ToString(),
                //              form["birthday"].ToString().Replace("-", ""),//birthday
                //              form["isShowName"].ToString().Replace("-", ""), //"Y",
                //              form["opdDate"].ToString(),
                //              form["deptID"].ToString(),
                //              form["opdTimeID"].ToString(),
                //              form["doctorID"].ToString(),
                //              form["roomID"].ToString(),
                //              form["subDoctorID"].ToString(),
                //              GetAuthKey(),
                //               FunctionService.GetIP()
                //              ));
                Response = JObject.Parse(AppServices.WS05_2_EN(
                              form["hospitalID"].ToString(),
                              "",
                              form["patNumber"].ToString(),
                              form["idNumber"].ToString(),
                              form["idType"].ToString(),
                              form["birthday"].ToString().Replace("-", ""),//birthday
                              form["isShowName"].ToString().Replace("-", ""), //"Y",
                              form["opdDate"].ToString(),
                              form["deptID"].ToString(),
                              form["opdTimeID"].ToString(),
                              form["doctorID"].ToString(),
                              form["roomID"].ToString(),
                              form["subDoctorID"].ToString(),
                              GetAuthKey(),
                               FunctionService.GetIP()
                               ,"",""
                              ));
            }
            catch (Exception ex)
            {
                //FunctionService.saveLog("WS05", ex, form);
                FunctionService.saveLog(HttpUtility.HtmlEncode("WS05"), ex, form);
            }


             return Response;
        }


        /// <summary>
        /// WS05 復診掛號-video
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public dynamic DoReg_video(FormCollection form)
        {
            JObject Response = null;

            try
            {
                //Response = JObject.Parse(AppServices.WS05(
                //              form["hospitalID"].ToString(),
                //              "",
                //              form["patNumber"].ToString(),
                //              form["idNumber"].ToString(),
                //              form["idType"].ToString(),
                //              form["birthday"].ToString().Replace("-", ""),//birthday
                //              "Y",
                //              form["opdDate"].ToString(),
                //              form["deptID"].ToString(),
                //              form["opdTimeID"].ToString(),
                //              form["doctorID"].ToString(),
                //              form["roomID"].ToString(),
                //              form["subDoctorID"].ToString(),
                //              GetAuthKey(),
                //               FunctionService.GetIP()
                //              ));
                //Response = JObject.Parse(AppServices.WS05_2_video(
                //             form["hospitalID"].ToString(),
                //             "",
                //             form["patNumber"].ToString(),
                //             form["idNumber"].ToString(),
                //             form["idType"].ToString(),
                //             form["birthday"].ToString().Replace("-", ""),//birthday
                //             "Y",
                //             form["opdDate"].ToString(),
                //             form["deptID"].ToString(),
                //             form["opdTimeID"].ToString(),
                //             form["doctorID"].ToString(),
                //             form["roomID"].ToString(),
                //             form["subDoctorID"].ToString(),
                //             GetAuthKey(),
                //              FunctionService.GetIP(),
                //              "",
                //              ""
                //             ));


                //Response = JObject.Parse(AppServices.WS05_2_video2(
                //            form["hospitalID"].ToString(),
                //            "",
                //            form["patNumber"].ToString(),
                //            form["idNumber"].ToString(),
                //            form["idType"].ToString(),
                //            form["birthday"].ToString().Replace("-", ""),//birthday
                //            form["isShowName"].ToString().Replace("-", ""),
                //            form["opdDate"].ToString(),
                //            form["deptID"].ToString(),
                //            form["opdTimeID"].ToString(),
                //            form["doctorID"].ToString(),
                //            form["roomID"].ToString(),
                //            form["subDoctorID"].ToString(),
                //            form["phone6"].ToString(),
                //            form["address6"].ToString(),
                //            GetAuthKey(),
                //             FunctionService.GetIP(),
                //             "",
                //             ""
                //            ));
                string picidType = form["picidType"].ToString();
                //string picidType0 = form["picidType0"].ToString();
                //string picidType1 = form["picidType1"].ToString();
                //string picidType2 = form["picidType0"].ToString();
                //if (picidType0 == "0") { picidType = "0"; }
                //else if (picidType1 == "1") { picidType = "1"; }
                //else if (picidType2 == "2") { picidType = "2"; }

                var values_type1_date_y = form["type1_date_y"].ToString().PadLeft(4, '0');
                var values_type1_date_m = form["type1_date_m"].ToString().PadLeft(2, '0');
                var values_type1_date_d = form["type1_date_d"].ToString().PadLeft(2, '0');
                var values_type1_date = values_type1_date_y + values_type1_date_m + values_type1_date_d;

                var values_type2_date_y = form["type2_date_y"].ToString().PadLeft(4, '0');
                var values_type2_date_m = form["type2_date_m"].ToString().PadLeft(2, '0');
                var values_type2_date_d = form["type2_date_d"].ToString().PadLeft(2, '0');
                var values_type2_date = values_type2_date_y + values_type2_date_m + values_type2_date_d;

                var upimage1 = "";
                var upimage2 = "";
                var upimage3 = "";
                byte[] file_bytes_1 =new byte[0];
                byte[] file_bytes_2 = new byte[0];
                byte[] file_bytes_3 = new byte[0];





                if (picidType == "0")
                { //非下列狀況個案

                }
                else if (picidType == "1")
                {//快篩陽性個案
                    string values_file_uploader_1 = "[" + form["upfile1"].ToString() + "]";
                    if (values_file_uploader_1 != null)
                    {
                        if (values_file_uploader_1.ToString() != "[]")
                        {
                            file_bytes_1 = values_file_uploader_1.ToString().Trim('[', ']')
                     .Split(',')
                     .Select(x => byte.Parse(x))
                     .ToArray();
                            //dynamic data_up1 = AppData.GetWS241_ROPDDIMG2(values, "file1_1", file_bytes_1);
                            upimage1 = "Y";
                        }
                    }

                    object values_file_uploader_2 = "[" + form["upfile2"].ToString() + "]"; ;
                    if (values_file_uploader_2 != null)
                    {
                        if (values_file_uploader_2.ToString() != "[]")
                        {
                            file_bytes_2 = values_file_uploader_2.ToString().Trim('[', ']')
                      .Split(',')
                      .Select(x => byte.Parse(x))
                      .ToArray();
                            //  dynamic data_up2 = AppData.GetWS241_ROPDDIMG2(values, "file1_2", file_bytes_2);
                            upimage2 = "Y";
                        }
                    }

                    //  if (values_file_uploader_1 ==null) {  errmsge = "快篩陽性個案需上傳附件-1.您的檢測卡匣/片"; }
                    // if (values_file_uploader_2 == null) { errmsge = "快篩陽性個案需上傳附件-2.居家隔離、自主防疫或居家檢疫相關政府文件或通知之影像"; }

                }
                else if (picidType == "2")
                {//確診居家照護個案

                    object values_file_uploader_3 = "["+form["upfile3"].ToString() + "]";
                    if (values_file_uploader_3 != null)
                    {
                        if (values_file_uploader_3.ToString() != "[]")
                        {
                            file_bytes_3 = values_file_uploader_3.ToString().Trim('[', ']')
                       .Split(',')
                       .Select(x => byte.Parse(x))
                       .ToArray();
                            // dynamic data_up3 = AppData.GetWS241_ROPDDIMG2(values, "file1_3", file_bytes_3);
                            upimage3 = "Y";
                        }
                    }

                    //  if (values_file_uploader_3 == null) { errmsge = "確診居家照護個案需上傳附件-隔離通知書、健保快易通確診資訊或衛生局簡訊通知畫面等"; }
                }




                //Response = JObject.Parse(AppServices.WS05_2_video4(
                //           form["hospitalID"].ToString(),
                //           "",
                //           form["patNumber"].ToString(),
                //           form["idNumber"].ToString(),
                //           form["idType"].ToString(),
                //           form["birthday"].ToString().Replace("-", ""),//birthday
                //           form["isShowName"].ToString().Replace("-", ""),
                //           form["opdDate"].ToString(),
                //           form["deptID"].ToString(),
                //           form["opdTimeID"].ToString(),
                //           form["doctorID"].ToString(),
                //           form["roomID"].ToString(),
                //           form["subDoctorID"].ToString(),
                //           form["phone6"].ToString(),
                //           form["address6"].ToString(),
                //           picidType,
                //           values_type1_date,
                //           values_type2_date,
                //           upimage1,
                //           file_bytes_1,
                //           upimage2,
                //           file_bytes_2,
                //           upimage3,
                //           file_bytes_3,
                //           GetAuthKey(),
                //            FunctionService.GetIP(),
                //            "",
                //            ""
                //           ));

                Response = JObject.Parse(AppServices.WS05_2_video4_EN(
                           form["hospitalID"].ToString(),
                           "",
                           form["patNumber"].ToString(),
                           form["idNumber"].ToString(),
                           form["idType"].ToString(),
                           form["birthday"].ToString().Replace("-", ""),//birthday
                           form["isShowName"].ToString().Replace("-", ""),
                           form["opdDate"].ToString(),
                           form["deptID"].ToString(),
                           form["opdTimeID"].ToString(),
                           form["doctorID"].ToString(),
                           form["roomID"].ToString(),
                           form["subDoctorID"].ToString(),
                           form["phone6"].ToString(),
                           form["address6"].ToString(),
                           picidType,
                           values_type1_date,
                           values_type2_date,
                           upimage1,
                           file_bytes_1,
                           upimage2,
                           file_bytes_2,
                           upimage3,
                           file_bytes_3,
                           GetAuthKey(),
                            FunctionService.GetIP(),
                            "",
                            ""
                           ));

            }
            catch (Exception ex)
            {
               // FunctionService.saveLog("WS05", ex, form);
                //FunctionService.saveLog("WS05", ex, form);
                FunctionService.saveLog(HttpUtility.HtmlEncode("WS05"), ex, form);
            }


            return Response;
        }




        /// <summary>
        /// WS06 初診掛號
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public dynamic DoFirstReg(FormCollection form)
        {
            JObject Response = null;

            try
            {

                //Response = JObject.Parse(AppServices.WS06(
                //form["hospitalID"].ToString(),
                //"",
                //"",
                //form["idNumber"].ToString(),
                //form["idType"].ToString(),
                //form["birthday"].ToString().Replace("-",""),//birthday
                //"Y",
                //form["opdDate"].ToString(),
                //form["deptID"].ToString(),
                //form["opdTimeID"].ToString(),
                //form["doctorID"].ToString(),
                //form["roomID"].ToString(),
                //form["subDoctorID"].ToString(),
                //GetAuthKey(),
                //form["name"].ToString(),
                //"",
                //"",
                //"",
                //"",
                //form["telephone"].ToString(),
                //form["cellphone"].ToString(),
                //"",
                //"",
                //form["address"].ToString(),
                // FunctionService.GetIP()
                //));

                Response = JObject.Parse(AppServices.WS06_2_EN(
                form["hospitalID"].ToString(),
                "",
                "",
                form["idNumber"].ToString(),
                form["idType"].ToString(),
                form["birthday"].ToString().Replace("-", ""),//birthday
                "Y",
                form["opdDate"].ToString(),
                form["deptID"].ToString(),
                form["opdTimeID"].ToString(),
                form["doctorID"].ToString(),
                form["roomID"].ToString(),
                form["subDoctorID"].ToString(),
                GetAuthKey(),
                form["name"].ToString(),
                "",
                "",
                "",
                "",
                form["telephone"].ToString(),
                form["cellphone"].ToString(),
                "",
                "",
                form["address"].ToString(),
                 FunctionService.GetIP()
                 ,"",""
                ));
            }
            catch (Exception ex)
            {
                //FunctionService.saveLog("WS06", ex, form);
                FunctionService.saveLog(HttpUtility.HtmlEncode("WS06"), ex, form);
            }
            return Response;

        }


        /// <summary>
        /// WS34Q 依病歷號查詢出病患的聯繫資訊
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public dynamic getPatContactInfo(FormCollection form)
        {
            JObject Response = JObject.Parse(AppServices.WS34Q(
                "",
                "",
                form["patNumber"].ToString(),
                form["idNumber"].ToString(),
                form["idType"].ToString(),
                form["birthday"].ToString(),
                 GetAuthKey()
                ));
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<PatContactInfoModel> resultList = JsonConvert.DeserializeObject<List<PatContactInfoModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }


        }


        /// <summary>
        /// 三個月未到診紀錄
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic getSymptomQuery(Dictionary<string, string> data)
        {

            JObject Response = JObject.Parse(
               AppServices.WS89(
                   data["hospitalID"].ToString(),                   
                   data["chtnoID"].ToString(),            
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<SymptomQueryModel> resultList = JsonConvert.DeserializeObject<List<SymptomQueryModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        /// <summary>
        /// 取得院區重要訊息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetHospitalInfo(string locID)
        {

            //var json = (
            //       AppServices.WS97(
            //       locID,
            //       GetAuthKey())
            //       );
            var json = (
             AppServices.WS97_EN(
             locID,
             GetAuthKey())
             );

            //json = Regex.Replace(json, @"<(.|\n)*?>", "");           
            JObject Response = JObject.Parse(json);

            if (Response["isSuccess"].ToString() == "Y")
            {
                if (Response["message"].ToString() == "Y")
                {
                    List<HospitalInfoModel> resultList = JsonConvert.DeserializeObject<List<HospitalInfoModel>>(Response["resultList"].ToString());
                    return resultList;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// 取得科別說明事項
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDeptInfo(string locID , string dptID)
        {
            string CacheName = "GetDeptInfo" + locID + dptID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
               //     var json = (
               //AppServices.WS98(
               //    locID,
               //    dptID,
               //    GetAuthKey())
               //    );
                    var json = (
            AppServices.WS98_EN(
                locID,
                dptID,
                GetAuthKey())
                );

                    // json = Regex.Replace(json, @"<(.|\n)*?>", "");
                    JObject Response = JObject.Parse(json);

                    if (Response["isSuccess"].ToString() == "Y")
                    {

                        if (Response["message"].ToString() == "Y")
                        {
                            List<DeptInfoModel> resultList = JsonConvert.DeserializeObject<List<DeptInfoModel>>(Response["resultList"].ToString());
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return "";
                        }
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }


        /// <summary>
        /// 取得各科門診看診病症所有科別(我要看哪一科)
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetDPT(string hospitalID)
        {
            string CacheName = "GetDPT" + hospitalID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {

                   // JObject Response = JObject.Parse(AppServices.WS76(
                   //hospitalID,
                   //"",
                   //"",
                   //GetAuthKey()));

                    JObject Response = JObject.Parse(AppServices.WS76_EN(
                 hospitalID,
                 "",
                 "",
                 GetAuthKey()));

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DPTModel> resultList = JsonConvert.DeserializeObject<List<DPTModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 取得各科門診看診病症所有科別(我要看哪一科)-細項
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="bigDptNo"></param>
        /// <returns></returns>
        public dynamic GetSubDPT(string hospitalID , string bigDptNo)
        {
            string CacheName = "GetSubDPT" + hospitalID + bigDptNo;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                   // JObject Response = JObject.Parse(AppServices.WS77(
                   //hospitalID,
                   //"",
                   //bigDptNo,
                   //GetAuthKey()));
                    JObject Response = JObject.Parse(AppServices.WS77_EN(
                  hospitalID,
                  "",
                  bigDptNo,
                  GetAuthKey()));

                    if (Response["isSuccess"].ToString() == "Y")
                    {

                        List<DPTSubModel> resultList = JsonConvert.DeserializeObject<List<DPTSubModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 查詢特殊處理科別資料
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetDeptSp(string locID , string dptID)
        {
            string CacheName = "GetDeptSp" + locID + dptID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               AppServices.WS92(
                   locID,
                   dptID,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        if (Response["message"].ToString() == "Y")
                        {
                            List<DeptSpModel> resultList = JsonConvert.DeserializeObject<List<DeptSpModel>>(Response["resultList"].ToString());
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return "";
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// 取得各類疾病相關就診科別參考之所有病症
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetDoctorProfile(string hospitalID)
        {
            string CacheName = "GetDoctorProfile" + hospitalID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
               //     JObject Response = JObject.Parse(
               //AppServices.WS78(
               //    hospitalID,
               //    "",
               //    "",
               //    GetAuthKey())
               //    );
                    JObject Response = JObject.Parse(
              AppServices.WS78_EN(
                  hospitalID,
                  "",
                  "",
                  GetAuthKey())
                  );


                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DoctorProfileModel> resultList = JsonConvert.DeserializeObject<List<DoctorProfileModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 取得各類疾病相關就診科別參考之某一病症之說明
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="dsNo"></param>
        /// <returns></returns>
        public dynamic GetDoctorSubProfile(string hospitalID , string dsNo)
        {
            string CacheName = "GetDoctorSubProfile" + hospitalID + dsNo;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
               //     JObject Response = JObject.Parse(
               //AppServices.WS79(
               //    hospitalID,
               //    "",
               //    dsNo,
               //    GetAuthKey())
               //    );

                    JObject Response = JObject.Parse(
               AppServices.WS79_EN(
                   hospitalID,
                   "",
                   dsNo,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DoctorProfileSubModel> resultList = JsonConvert.DeserializeObject<List<DoctorProfileSubModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 取得掛號查詢及取消說明事項
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="dsNo"></param>
        /// <returns></returns>
        public dynamic GetQueryInfo(string locID)
        {
            string CacheName = "GetQueryInfo" + locID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {

               //     JObject Response = JObject.Parse(
               //AppServices.WS101(
               //    locID,
               //    GetAuthKey())
               //    );

                    JObject Response = JObject.Parse(
             AppServices.WS101_EN(
                 locID,
                 GetAuthKey())
                 );


                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<QueryInfoModel> resultList = JsonConvert.DeserializeObject<List<QueryInfoModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// 依建議看診科別名稱取得開診院區及科別
        /// </summary>
        /// <param name="dptnm"></param>
        /// <returns></returns>
        public dynamic GetReference(string dptnm)
        {
            string CacheName = "GetReference" + dptnm;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
               //     JObject Response = JObject.Parse(
               //AppServices.WS104(
               //    dptnm,
               //    GetAuthKey())
               //    );

                    JObject Response = JObject.Parse(
              AppServices.WS104_EN(
                  dptnm,
                  GetAuthKey())
                  );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<ReferenceModel> resultList = JsonConvert.DeserializeObject<List<ReferenceModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 取得各類疾病相關就診科別參考之症狀建議看診科別
        /// </summary>
        /// <param name="dsno"></param>
        /// <param name="dsdno"></param>
        /// <returns></returns>
        public dynamic GetRelated(string dsno , string dsdno)
        {
            string CacheName = "GetRelated" + dsno + dsdno;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
               //     JObject Response = JObject.Parse(
               //AppServices.WS103(
               //    dsno,
               //    dsdno,
               //    GetAuthKey())
               //    );

                    JObject Response = JObject.Parse(
               AppServices.WS103_EN(
                   dsno,
                   dsdno,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<RelatedModel> resultList = JsonConvert.DeserializeObject<List<RelatedModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }


        /// <summary>
        /// 進度說明
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetProgressInfo(Dictionary<string, string> data)
        {
            string CacheName = "GetProgressInfo" + data["hospitalID"].ToString();
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                   JObject Response = JObject.Parse(
                   AppServices.WS114_EN(
                   data["hospitalID"].ToString(),             
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<ProgressInfoModel> resultList = JsonConvert.DeserializeObject<List<ProgressInfoModel>>(Response["resultList"].ToString());               
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(1), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }

        public dynamic GetProgressData(Dictionary<string, string> data)
        {
            string CacheName = "GetProgressData" + data["hospitalID"].ToString() + data["language"].ToString() + data["deptGrouptID"].ToString() + data["opdTimeID"].ToString();
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               AppServices.WS115_EN(
                   data["hospitalID"].ToString(),
                   data["language"].ToString(),
                   data["deptGrouptID"].ToString(),
                   data["opdTimeID"].ToString(),
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<ProgressDataModel> resultList = JsonConvert.DeserializeObject<List<ProgressDataModel>>(Response["resultList"].ToString());
                        // resultList = resultList.GroupBy(x => new { x.deptName, x.roomLocation,x.roomID })

                        // var groupData = resultList.GroupBy(x => new { x.doctorID, x.deptGroupID, x.roomID  }).ToList();
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(0.1), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }



        /// <summary>
        /// 取得門診表路徑
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic DeptFileDownload(string hospitalID)
        {
            string CacheName = "DeptFileDownload" + hospitalID;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
                    AppServices.WS116(
                    hospitalID,
                    GetAuthKey())
                    );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DeptFileDownloadModel> resultList = JsonConvert.DeserializeObject<List<DeptFileDownloadModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(1), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// WS141 APP掛號時時公告通知內容
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="dpt"></param>
        /// <param name="drid"></param>
        /// <returns></returns>
        public dynamic GetWS141(string hospitalID, string dpt, string drid)
        {
            //JObject Response = JObject.Parse(AppServices.WS04(hospitalID ,"", opdBeginDate , opdEndDate , doctorID , deptID, GetAuthKey()));
            string CacheName = "GetWS141" + hospitalID + dpt + drid;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(AppServices.WS141(hospitalID, dpt, drid, GetAuthKey()));
                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<WS141Model> resultList = JsonConvert.DeserializeObject<List<WS141Model>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// WS142 於掛號完成時時，呈現緊急通知內容
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="dpt"></param>
        /// <param name="drid"></param>
        /// <returns></returns>
        public dynamic GetWS142(string hospitalID, string dpt, string drid)
        {
            //JObject Response = JObject.Parse(AppServices.WS04(hospitalID ,"", opdBeginDate , opdEndDate , doctorID , deptID, GetAuthKey()));
            string CacheName = "GetWS142" + hospitalID + dpt + drid;
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(AppServices.WS142(hospitalID, dpt, drid, GetAuthKey()));
                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<WS142Model> resultList = JsonConvert.DeserializeObject<List<WS142Model>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// WS143
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="nm"></param>
        /// <returns></returns>
        public dynamic GetWS143(string hospitalID, string nm)
        {

            var json = (
                   AppServices.WS143(
                   hospitalID, nm,
                   GetAuthKey())
                   );

            //json = Regex.Replace(json, @"<(.|\n)*?>", "");           
            JObject Response = JObject.Parse(json);

            if (Response["isSuccess"].ToString() == "Y")
            {
                if (Response["message"].ToString() == "Y")
                {
                    List<WS143Model> resultList = JsonConvert.DeserializeObject<List<WS143Model>>(Response["resultList"].ToString());
                    return resultList;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// WS105
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="chtno"></param>
        /// <param name="idno"></param>
        /// <returns></returns>
        public dynamic GetWS105(string hospitalID, string chtno,string idno)
        {

            //var json = (
            //       AppServices.WS105(
            //       hospitalID, chtno,idno,
            //       GetAuthKey())
            //       );
            var json = (
              AppServices.WS105(
              HttpUtility.HtmlEncode(hospitalID), HttpUtility.HtmlEncode(chtno), HttpUtility.HtmlEncode(idno),
              GetAuthKey())
              );
            //json = Regex.Replace(json, @"<(.|\n)*?>", "");           
            JObject Response = JObject.Parse(json);

            //if (Response["isSuccess"].ToString() == "Y")
                if (HttpUtility.HtmlEncode(Response["isSuccess"].ToString() )== "Y")
                {
                //if (Response["message"].ToString() == "Y")
                    if (HttpUtility.HtmlEncode(Response["message"].ToString()) == "Y")
                    {
                    List<WS105Model> resultList = new List<WS105Model>();
                    try
                    {
                        resultList = JsonConvert.DeserializeObject<List<WS105Model>>(Response["resultList"].ToString());
                    }
                    catch
                    {
                        resultList = JsonConvert.DeserializeObject<List<WS105Model>>(Response[""].ToString());
                    }
                    return resultList;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WSNHIQP701_LOCAL Covid19 疫苗接種名冊 即時比對
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="chtno"></param>
        /// <param name="idno"></param>
        /// <returns></returns>
        public dynamic GetWSNHIQP701_LOCAL2(string hospitalID, string patNumber, string idNumber, string idType, string birthday, string dpid,string sValidSDate)
        {
            //var json=(
            //      AppServices.WSNHIQP701_LOCAL2(
            //      hospitalID, patNumber, idNumber, idType, birthday, dpid, sValidSDate,
            //      GetAuthKey())
            //      );
            var json = (
              AppServices.WSNHIQP701_LOCAL2_EN(
              hospitalID, patNumber, idNumber, idType, birthday, dpid, sValidSDate,
              GetAuthKey())
              );


            //json = Regex.Replace(json, @"<(.|\n)*?>", "");           
            JObject Response = JObject.Parse(json);

            if (Response["isSuccess"].ToString() == "Y")
            {
                if (Response["message"].ToString() == "Y")
                {
                    List<WSNHIQP7012Model> resultList = new List<WSNHIQP7012Model>();
                    try
                    {
                        resultList = JsonConvert.DeserializeObject<List<WSNHIQP7012Model>>(Response["resultList"].ToString());
                    }
                    catch
                    {
                        resultList = JsonConvert.DeserializeObject<List<WSNHIQP7012Model>>(Response[""].ToString());
                    }
                    return resultList;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS241 視訊門診預約
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetWS241(Dictionary<string, string> data)
        {
            //string strhospitalID = data["hospitalID"].ToString();
            //string strpatNumber = data["patNumber"].ToString();
            //string stridNumber = data["idNumber"].ToString();
            //string stridType = data["idType"].ToString();
            //string stropdDate = data["opdDate"].ToString();
            //string strdoctorID = data["doctorID"].ToString();
            //string stropdTimeID = data["opdTimeID"].ToString();
            //string strdeptID = data["deptID"].ToString();
            //string strGetAuthKey = GetAuthKey();
            //string strip = data["ip"].ToString();
            JObject Response = JObject.Parse(
                AppServices.WS241(
                    data["hospitalID"].ToString(),                   
                    data["patNumber"].ToString(),
                    data["idNumber"].ToString(),
                    data["idType"].ToString(),
                    data["opdDate"].ToString(),
                    data["doctorID"].ToString(),
                    data["opdTimeID"].ToString(),
                    data["deptID"].ToString(),   
                    GetAuthKey(),
                    data["ip"].ToString())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS241Model> resultList = JsonConvert.DeserializeObject<List<WS241Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        public dynamic GetWS241_2(Dictionary<string, string> data,string phone,string site)
        {
           string strhospitalID = data["hospitalID"].ToString();
            string strpatNumber = data["patNumber"].ToString();
            string stridNumber = data["idNumber"].ToString();
            string stridType = data["idType"].ToString();
            string stropdDate = data["opdDate"].ToString();
            string strdoctorID = data["doctorID"].ToString();
            string stropdTimeID = data["opdTimeID"].ToString();
            string strdeptID = data["deptID"].ToString();
            string strGetAuthKey = GetAuthKey();
            string strip = data["ip"].ToString();

            //JObject Response = JObject.Parse("");

            //JObject Response = JObject.Parse(
            //    AppServices.WS241_2(
            //        data["hospitalID"].ToString(),
            //        data["patNumber"].ToString(),
            //        data["idNumber"].ToString(),
            //        data["idType"].ToString(),
            //        data["opdDate"].ToString(),
            //        data["doctorID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["deptID"].ToString(),
            //        //data["phone"].ToString(),
            //        //data["site"].ToString(),
            //        phone,
            //        site,
            //        GetAuthKey(),
            //        data["ip"].ToString())
            //        );

            JObject Response = JObject.Parse(
               AppServices.WS241_2(
                   HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
                   HttpUtility.HtmlEncode(data["patNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idType"].ToString()),
                   HttpUtility.HtmlEncode(data["opdDate"].ToString()),
                   HttpUtility.HtmlEncode(data["doctorID"].ToString()),
                   HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
                   HttpUtility.HtmlEncode(data["deptID"].ToString()),
                   //data["phone"].ToString(),
                   //data["site"].ToString(),
                   HttpUtility.HtmlEncode(phone),
                   HttpUtility.HtmlEncode(site),
                   GetAuthKey(),
                   HttpUtility.HtmlEncode(data["ip"].ToString()))
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS241Model> resultList = JsonConvert.DeserializeObject<List<WS241Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        public dynamic GetWS241_3(Dictionary<string, string> data, string phone, string site,string picidType, string image1, byte[] fileimage1, string image2, byte[] fileimage2, string image3, byte[] fileimage3)
        {
            string strhospitalID = data["hospitalID"].ToString();
            string strpatNumber = data["patNumber"].ToString();
            string stridNumber = data["idNumber"].ToString();
            string stridType = data["idType"].ToString();
            string stropdDate = data["opdDate"].ToString();
            string strdoctorID = data["doctorID"].ToString();
            string stropdTimeID = data["opdTimeID"].ToString();
            string strdeptID = data["deptID"].ToString();
            string strGetAuthKey = GetAuthKey();
            string strip = data["ip"].ToString();

            //JObject Response = JObject.Parse("");

            //JObject Response = JObject.Parse(
            //    AppServices.WS241_2(
            //        data["hospitalID"].ToString(),
            //        data["patNumber"].ToString(),
            //        data["idNumber"].ToString(),
            //        data["idType"].ToString(),
            //        data["opdDate"].ToString(),
            //        data["doctorID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["deptID"].ToString(),
            //        //data["phone"].ToString(),
            //        //data["site"].ToString(),
            //        phone,
            //        site,
            //        GetAuthKey(),
            //        data["ip"].ToString())
            //        );

            JObject Response = JObject.Parse(
               AppServices.WS241_3(
                   HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
                   HttpUtility.HtmlEncode(data["patNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idType"].ToString()),
                   HttpUtility.HtmlEncode(data["opdDate"].ToString()),
                   HttpUtility.HtmlEncode(data["doctorID"].ToString()),
                   HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
                   HttpUtility.HtmlEncode(data["deptID"].ToString()),
                   //data["phone"].ToString(),
                   //data["site"].ToString(),
                   HttpUtility.HtmlEncode(phone),
                   HttpUtility.HtmlEncode(site),
                   picidType,
                   image1,
                    fileimage1,
                   image2,
                    fileimage2,
                   image3,
                    fileimage3,
                   GetAuthKey(),
                   HttpUtility.HtmlEncode(data["ip"].ToString()))
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS241Model> resultList = JsonConvert.DeserializeObject<List<WS241Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        public dynamic GetWS241_4(Dictionary<string, string> data, string phone, string site, string picidType, string type1_date, string type2_date, string image1, byte[] fileimage1, string image2, byte[] fileimage2, string image3, byte[] fileimage3)
        {
            string strhospitalID = data["hospitalID"].ToString();
            string strpatNumber = data["patNumber"].ToString();
            string stridNumber = data["idNumber"].ToString();
            string stridType = data["idType"].ToString();
            string stropdDate = data["opdDate"].ToString();
            string strdoctorID = data["doctorID"].ToString();
            string stropdTimeID = data["opdTimeID"].ToString();
            string strdeptID = data["deptID"].ToString();
            string strGetAuthKey = GetAuthKey();
            string strip = data["ip"].ToString();

            //JObject Response = JObject.Parse("");

            //JObject Response = JObject.Parse(
            //    AppServices.WS241_2(
            //        data["hospitalID"].ToString(),
            //        data["patNumber"].ToString(),
            //        data["idNumber"].ToString(),
            //        data["idType"].ToString(),
            //        data["opdDate"].ToString(),
            //        data["doctorID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["deptID"].ToString(),
            //        //data["phone"].ToString(),
            //        //data["site"].ToString(),
            //        phone,
            //        site,
            //        GetAuthKey(),
            //        data["ip"].ToString())
            //        );

            JObject Response = JObject.Parse(
               AppServices.WS241_4(
                   HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
                   HttpUtility.HtmlEncode(data["patNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idType"].ToString()),
                   HttpUtility.HtmlEncode(data["opdDate"].ToString()),
                   HttpUtility.HtmlEncode(data["doctorID"].ToString()),
                   HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
                   HttpUtility.HtmlEncode(data["deptID"].ToString()),
                   //data["phone"].ToString(),
                   //data["site"].ToString(),
                   HttpUtility.HtmlEncode(phone),
                   HttpUtility.HtmlEncode(site),
                   picidType,
                   type1_date,  type2_date,
                   image1,
                    fileimage1,
                   image2,
                    fileimage2,
                   image3,
                    fileimage3,
                   GetAuthKey(),
                   HttpUtility.HtmlEncode(data["ip"].ToString()))
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS241Model> resultList = JsonConvert.DeserializeObject<List<WS241Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        //圖檔上傳
        public dynamic GetWS241_ROPDDIMG(Dictionary<string, string> data, string imagenm, string image1)
        {
            string strhospitalID = data["hospitalID"].ToString();
            string strpatNumber = data["patNumber"].ToString();
            string stridNumber = data["idNumber"].ToString();
            string stridType = data["idType"].ToString();
            string stropdDate = data["opdDate"].ToString();
            string strdoctorID = data["doctorID"].ToString();
            string stropdTimeID = data["opdTimeID"].ToString();
            string strdeptID = data["deptID"].ToString();
            string strGetAuthKey = GetAuthKey();
            string strip = data["ip"].ToString();

            //JObject Response = JObject.Parse("");

            //JObject Response = JObject.Parse(
            //    AppServices.WS241_2(
            //        data["hospitalID"].ToString(),
            //        data["patNumber"].ToString(),
            //        data["idNumber"].ToString(),
            //        data["idType"].ToString(),
            //        data["opdDate"].ToString(),
            //        data["doctorID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["deptID"].ToString(),
            //        //data["phone"].ToString(),
            //        //data["site"].ToString(),
            //        phone,
            //        site,
            //        GetAuthKey(),
            //        data["ip"].ToString())
            //        );

            //JObject Response = JObject.Parse(
            //   AppServices.WS241_ROPDDIMG(
            //       HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
            //       HttpUtility.HtmlEncode(data["patNumber"].ToString()),
            //       HttpUtility.HtmlEncode(data["idNumber"].ToString()),
            //       HttpUtility.HtmlEncode(data["idType"].ToString()),
            //       HttpUtility.HtmlEncode(data["opdDate"].ToString()),
            //       HttpUtility.HtmlEncode(data["doctorID"].ToString()),
            //       HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
            //       HttpUtility.HtmlEncode(data["deptID"].ToString()),
            //       //data["phone"].ToString(),
            //       //data["site"].ToString(),
            //       imagenm,
            //       image1,
            //       GetAuthKey(),
            //       HttpUtility.HtmlEncode(data["ip"].ToString()))
            //       );
            JObject Response = JObject.Parse(
               AppServices.WS241_ROPDDIMG(
                   HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
                   HttpUtility.HtmlEncode(data["patNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idType"].ToString()),
                   HttpUtility.HtmlEncode(data["opdDate"].ToString()),
                   HttpUtility.HtmlEncode(data["doctorID"].ToString()),
                   HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
                   HttpUtility.HtmlEncode(data["deptID"].ToString()),
                   //data["phone"].ToString(),
                   //data["site"].ToString(),
                   imagenm,
                   image1,
                   GetAuthKey(),
                   HttpUtility.HtmlEncode(data["ip"].ToString()))
                   );
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS241Model> resultList = JsonConvert.DeserializeObject<List<WS241Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        public dynamic GetWS241_ROPDDIMG2(Dictionary<string, string> data, string imagenm, byte[] image1)
        {
            string strhospitalID = data["hospitalID"].ToString();
            string strpatNumber = data["patNumber"].ToString();
            string stridNumber = data["idNumber"].ToString();
            string stridType = data["idType"].ToString();
            string stropdDate = data["opdDate"].ToString();
            string strdoctorID = data["doctorID"].ToString();
            string stropdTimeID = data["opdTimeID"].ToString();
            string strdeptID = data["deptID"].ToString();
            string strGetAuthKey = GetAuthKey();
            string strip = data["ip"].ToString();

            //JObject Response = JObject.Parse("");

            //JObject Response = JObject.Parse(
            //    AppServices.WS241_2(
            //        data["hospitalID"].ToString(),
            //        data["patNumber"].ToString(),
            //        data["idNumber"].ToString(),
            //        data["idType"].ToString(),
            //        data["opdDate"].ToString(),
            //        data["doctorID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["deptID"].ToString(),
            //        //data["phone"].ToString(),
            //        //data["site"].ToString(),
            //        phone,
            //        site,
            //        GetAuthKey(),
            //        data["ip"].ToString())
            //        );

            //JObject Response = JObject.Parse(
            //   AppServices.WS241_ROPDDIMG(
            //       HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
            //       HttpUtility.HtmlEncode(data["patNumber"].ToString()),
            //       HttpUtility.HtmlEncode(data["idNumber"].ToString()),
            //       HttpUtility.HtmlEncode(data["idType"].ToString()),
            //       HttpUtility.HtmlEncode(data["opdDate"].ToString()),
            //       HttpUtility.HtmlEncode(data["doctorID"].ToString()),
            //       HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
            //       HttpUtility.HtmlEncode(data["deptID"].ToString()),
            //       //data["phone"].ToString(),
            //       //data["site"].ToString(),
            //       imagenm,
            //       image1,
            //       GetAuthKey(),
            //       HttpUtility.HtmlEncode(data["ip"].ToString()))
            //       );
            JObject Response = JObject.Parse(
               AppServices.WS241_ROPDDIMG2(
                   HttpUtility.HtmlEncode(data["hospitalID"].ToString()),
                   HttpUtility.HtmlEncode(data["patNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idNumber"].ToString()),
                   HttpUtility.HtmlEncode(data["idType"].ToString()),
                   HttpUtility.HtmlEncode(data["opdDate"].ToString()),
                   HttpUtility.HtmlEncode(data["doctorID"].ToString()),
                   HttpUtility.HtmlEncode(data["opdTimeID"].ToString()),
                   HttpUtility.HtmlEncode(data["deptID"].ToString()),
                   //data["phone"].ToString(),
                   //data["site"].ToString(),
                   imagenm,
                   image1,
                   GetAuthKey(),
                   HttpUtility.HtmlEncode(data["ip"].ToString()))
                   );
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS241Model> resultList = JsonConvert.DeserializeObject<List<WS241Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }





        /// <summary>
        /// WS242 視訊門診預約
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetWS242(string hospitalID, string opdvsdat, string rgsdpt, string shf, string drno)//(Dictionary<string, string> data)
        {
            //string strhospitalID = data["hospitalID"].ToString();
            //string strpatNumber = data["patNumber"].ToString();
            //string stridNumber = data["idNumber"].ToString();
            //string stridType = data["idType"].ToString();
            //string stropdDate = data["opdDate"].ToString();
            //string strdoctorID = data["doctorID"].ToString();
            //string stropdTimeID = data["opdTimeID"].ToString();
            //string strdeptID = data["deptID"].ToString();
            //string strGetAuthKey = GetAuthKey();
            //string strip = data["ip"].ToString();
            //JObject Response = JObject.Parse(
            //    AppServices.WS242(
            //        data["hospitalID"].ToString(),                  
            //        data["opdDate"].ToString(),
            //        data["deptID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["doctorID"].ToString(),     
            //        GetAuthKey())
            //        );
            JObject Response = JObject.Parse(
                AppServices.WS242(
                    hospitalID,
                    opdvsdat,
                    rgsdpt,
                    shf,
                    drno,
                    GetAuthKey())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS242Model> resultList = JsonConvert.DeserializeObject<List<WS242Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS244 視訊門診網址啟動註記
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetWS244(Dictionary<string, string> data)
        {
            //string strhospitalID = data["hospitalID"].ToString();
            //string strpatNumber = data["patNumber"].ToString();
            //string stridNumber = data["idNumber"].ToString();
            //string stridType = data["idType"].ToString();
            //string stropdDate = data["opdDate"].ToString();
            //string strdoctorID = data["doctorID"].ToString();
            //string stropdTimeID = data["opdTimeID"].ToString();
            //string strdeptID = data["deptID"].ToString();
            //string strGetAuthKey = GetAuthKey();
            //string strip = data["ip"].ToString();
            JObject Response = JObject.Parse(
                AppServices.WS244(
                    data["hospitalID"].ToString(),
                    data["patNumber"].ToString(),
                    data["idNumber"].ToString(),
                    data["idType"].ToString(),
                    data["opdDate"].ToString(),
                    data["doctorID"].ToString(),
                    data["opdTimeID"].ToString(),
                    data["deptID"].ToString(),
                    GetAuthKey(),
                    data["ip"].ToString())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS244Model> resultList = JsonConvert.DeserializeObject<List<WS244Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// WS245 某種類別是否啟用
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="nm"></param>
        /// <returns></returns>
        public dynamic GetWS245(string hospitalID, string kd)
        {

            var json = (
                   AppServices.WS245(
                   hospitalID, kd,
                   GetAuthKey())
                   );

            //json = Regex.Replace(json, @"<(.|\n)*?>", "");           
            JObject Response = JObject.Parse(json);

            if (Response["isSuccess"].ToString() == "Y")
            {
                if (Response["message"].ToString() == "Y")
                {
                    List<WS245Model> resultList = JsonConvert.DeserializeObject<List<WS245Model>>(Response["resultList"].ToString());
                    return resultList;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// WS0506_cghiqy_Preiqy2 取得整前問卷連結
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetWS0506_cghiqy_Preiqy2(string hospitalID, string drno, string Opdvsdat, string shf, string dpt, string chtno, string dept)//(Dictionary<string, string> data)
        {
            //string strhospitalID = data["hospitalID"].ToString();
            //string strpatNumber = data["patNumber"].ToString();
            //string stridNumber = data["idNumber"].ToString();
            //string stridType = data["idType"].ToString();
            //string stropdDate = data["opdDate"].ToString();
            //string strdoctorID = data["doctorID"].ToString();
            //string stropdTimeID = data["opdTimeID"].ToString();
            //string strdeptID = data["deptID"].ToString();
            //string strGetAuthKey = GetAuthKey();
            //string strip = data["ip"].ToString();
            //JObject Response = JObject.Parse(
            //    AppServices.WS242(
            //        data["hospitalID"].ToString(),                  
            //        data["opdDate"].ToString(),
            //        data["deptID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["doctorID"].ToString(),     
            //        GetAuthKey())
            //        );
            JObject Response = JObject.Parse(
                AppServices.WS0506_cghiqy_Preiqy2(
                    hospitalID,drno,Opdvsdat,shf,dpt,chtno,dept,
                    GetAuthKey())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS0506_cghiqy_Preiqy2Model> resultList = JsonConvert.DeserializeObject<List<WS0506_cghiqy_Preiqy2Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        /// <summary>
        /// WS0506_TOCCXAPP2 取得TOCC問卷
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetWS0506_TOCCXAPP2(string hospitalID, string drno, string Opdvsdat, string shf, string dpt, string chtno, string dept)//(Dictionary<string, string> data)
        {
            //string strhospitalID = data["hospitalID"].ToString();
            //string strpatNumber = data["patNumber"].ToString();
            //string stridNumber = data["idNumber"].ToString();
            //string stridType = data["idType"].ToString();
            //string stropdDate = data["opdDate"].ToString();
            //string strdoctorID = data["doctorID"].ToString();
            //string stropdTimeID = data["opdTimeID"].ToString();
            //string strdeptID = data["deptID"].ToString();
            //string strGetAuthKey = GetAuthKey();
            //string strip = data["ip"].ToString();
            //JObject Response = JObject.Parse(
            //    AppServices.WS242(
            //        data["hospitalID"].ToString(),                  
            //        data["opdDate"].ToString(),
            //        data["deptID"].ToString(),
            //        data["opdTimeID"].ToString(),
            //        data["doctorID"].ToString(),     
            //        GetAuthKey())
            //        );
            JObject Response = JObject.Parse(
                AppServices.WS0506_TOCCXAPP2(
                    hospitalID, drno, Opdvsdat, shf, dpt, chtno, dept,
                    GetAuthKey())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<WS0506_TOCCXAPP2Model> resultList = JsonConvert.DeserializeObject<List<WS0506_TOCCXAPP2Model>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

    }

}
