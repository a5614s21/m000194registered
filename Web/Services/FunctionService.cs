﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using System.IO;
using System.Text;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Collections;

namespace Web.Services
{
    public class FunctionService : Controller
    {
        // GET: FunctionService
     
            /// <summary>
            /// 取日期相差
            /// </summary>
            /// <param name="sDate"></param>
            /// <param name="eDate"></param>
            /// <returns></returns>
        public static int DateDifference(string sDate , string eDate)
        {
            IFormatProvider ifp = new System.Globalization.CultureInfo("zh-TW", true);
            DateTime startDate = DateTime.ParseExact(sDate, "yyyyMMdd", ifp, System.Globalization.DateTimeStyles.None);
            DateTime endDate = DateTime.ParseExact(eDate, "yyyyMMdd", ifp, System.Globalization.DateTimeStyles.None);

            TimeSpan ts2 = endDate - startDate;
            int len = Math.Abs(ts2.Days);



            return len;
        }

        /// <summary>
        /// 格式化 查詢門診資料
        /// </summary>
        /// <param name="OPDSchdules"></param>
        /// <param name="SchedulePeriods"></param>
        /// <param name="doctorID"></param>
        /// <param name="doctorName"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> FormatOPDSchdule(List<OPDSchduleModel> OPDSchdules, List<SchedulePeriodModel> SchedulePeriods , string doctorID, string doctorName)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            if (OPDSchdules != null)
            {
                dic.Add("DoctorGroups", OPDSchdules.GroupBy(x => new { x.doctorID, x.doctorName, x.deptName }));//取所有醫師姓名           

                foreach (SchedulePeriodModel item in SchedulePeriods)
                {

                    string startDate = item.startDate.Substring(0, 4) + "/" + item.startDate.Substring(4, 2) + "/" + item.startDate.Substring(6, 2);
                    string endDate = item.endDate.Substring(0, 4) + "/" + item.endDate.Substring(4, 2) + "/" + item.endDate.Substring(6, 2);

                    for (DateTime dt = DateTime.Parse(startDate); dt <= DateTime.Parse(endDate); dt = dt.AddDays(1.0))
                    {
                        string opdDate = dt.ToString("yyyyMMdd");
                        Dictionary<String, Object> dic2 = new Dictionary<string, object>();


                        var EarlyMorning = OPDSchdules.Where(m => m.opdTimeID == "0").Where(m => m.opdDate == opdDate);
                        var Morning = OPDSchdules.Where(m => m.opdTimeID == "1" || m.opdTimeID == "0").Where(m => m.opdDate == opdDate);
                        var Afternoon = OPDSchdules.Where(m => m.opdTimeID == "2").Where(m => m.opdDate == opdDate);
                        var Night = OPDSchdules.Where(m => m.opdTimeID == "3").Where(m => m.opdDate == opdDate);

                        if(doctorID != "")
                        {
                            EarlyMorning = EarlyMorning.Where(m => m.doctorID == doctorID);
                            Morning = Morning.Where(m => m.doctorID == doctorID);
                            Afternoon = Afternoon.Where(m => m.doctorID == doctorID);
                            Night = Night.Where(m => m.doctorID == doctorID);
                        }
                        if (doctorName != "")
                        {
                            EarlyMorning = EarlyMorning.Where(m => m.doctorName.Contains(doctorName) );
                            Morning = Morning.Where(m => m.doctorName.Contains(doctorName));
                            Afternoon = Afternoon.Where(m => m.doctorName.Contains(doctorName));
                            Night = Night.Where(m => m.doctorName.Contains(doctorName));
                        }
                                               

                        dic2.Add("EarlyMorning", EarlyMorning.ToList());//晨間
                        dic2.Add("Morning", Morning.ToList());//上午
                        dic2.Add("Afternoon", Afternoon.ToList());//下午
                        dic2.Add("Night", Night.ToList());//夜間  
                        dic.Add(opdDate, dic2);//取所有醫師姓名
                    }
                }
            }

            return dic;
        }

        /// <summary>
        /// 格式化 查詢門診資料
        /// </summary>
        /// <param name="OPDSchdules"></param>
        /// <param name="SchedulePeriods"></param>
        /// <param name="doctorID"></param>
        /// <param name="doctorName"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> FormatOPDSchdule_video(List<OPDSchduleModel> OPDSchdules, List<SchedulePeriodModel> SchedulePeriods, string doctorID, string doctorName)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            if (OPDSchdules != null)
            {
                dic.Add("DoctorGroups_video", OPDSchdules.GroupBy(x => new { x.doctorID, x.doctorName, x.deptName }));//取所有醫師姓名           

                foreach (SchedulePeriodModel item in SchedulePeriods)
                {

                    string startDate = item.startDate.Substring(0, 4) + "/" + item.startDate.Substring(4, 2) + "/" + item.startDate.Substring(6, 2);
                    string endDate = item.endDate.Substring(0, 4) + "/" + item.endDate.Substring(4, 2) + "/" + item.endDate.Substring(6, 2);

                    for (DateTime dt = DateTime.Parse(startDate); dt <= DateTime.Parse(endDate); dt = dt.AddDays(1.0))
                    {
                        string opdDate = dt.ToString("yyyyMMdd");
                        Dictionary<String, Object> dic2 = new Dictionary<string, object>();


                        var EarlyMorning = OPDSchdules.Where(m => m.opdTimeID == "0").Where(m => m.opdDate == opdDate);
                        var Morning = OPDSchdules.Where(m => m.opdTimeID == "1" || m.opdTimeID == "0").Where(m => m.opdDate == opdDate);
                        var Afternoon = OPDSchdules.Where(m => m.opdTimeID == "2").Where(m => m.opdDate == opdDate);
                        var Night = OPDSchdules.Where(m => m.opdTimeID == "3").Where(m => m.opdDate == opdDate);

                        if (doctorID != "")
                        {
                            EarlyMorning = EarlyMorning.Where(m => m.doctorID == doctorID);
                            Morning = Morning.Where(m => m.doctorID == doctorID);
                            Afternoon = Afternoon.Where(m => m.doctorID == doctorID);
                            Night = Night.Where(m => m.doctorID == doctorID);
                        }
                        if (doctorName != "")
                        {
                            EarlyMorning = EarlyMorning.Where(m => m.doctorName.Contains(doctorName));
                            Morning = Morning.Where(m => m.doctorName.Contains(doctorName));
                            Afternoon = Afternoon.Where(m => m.doctorName.Contains(doctorName));
                            Night = Night.Where(m => m.doctorName.Contains(doctorName));
                        }


                        dic2.Add("EarlyMorning", EarlyMorning.ToList());//晨間
                        dic2.Add("Morning", Morning.ToList());//上午
                        dic2.Add("Afternoon", Afternoon.ToList());//下午
                        dic2.Add("Night", Night.ToList());//夜間  
                        dic.Add(opdDate, dic2);//取所有醫師姓名
                    }
                }
            }

            return dic;
        }

        public static NameValueCollection getLangData(string lang)
        {
            NameValueCollection langData = new NameValueCollection();
            if (lang != "")
            {
                lang = "_" + lang;
            }
            ResourceManager rm = new ResourceManager("Web.App_GlobalResources.Resource" + lang, Assembly.GetExecutingAssembly());
            ResourceSet resourceSet = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                langData.Add(entry.Key.ToString(), entry.Value.ToString());
            }

            return langData;
        }

        /// <summary>
        /// 回傳週幾
        /// </summary>
        /// <param name="inputDT"></param>
        /// <returns></returns>
        public static string getChtWeek(DateTime inputDT)
        {

            switch (inputDT.DayOfWeek.ToString())
            {
                case "Monday": return "一";
                case "Tuesday": return "二";
                case "Wednesday": return "三";
                case "Thursday": return "四";
                case "Friday": return "五";
                case "Saturday": return "六";
                case "Sunday": return "日";
                default: return "系統無法判斷";
            }
        }

        /// <summary>
        /// 取得台灣縣市
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public static dynamic GetTaiwan(string city)
        {
            List<TaiwanModel> taiwans = new List<TaiwanModel>();
            string json = GetFileJson(System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Content/taiwan.json");
            taiwans = JsonConvert.DeserializeObject<List<TaiwanModel>>(json);

            dynamic re = taiwans;

            if (city != "")
            {
                re = taiwans.Find(m => m.city == city).area;
            }

            return re;
        }

        /// <summary>
        /// 讀取Json檔案
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static string GetFileJson(string filepath)
        {
            string json = string.Empty;
            using (FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader sr = new StreamReader(fs, Encoding.GetEncoding("utf-8")))
                {
                    json = sr.ReadToEnd().ToString();
                }
            }
            //encoding_DLL.FunctionService_dll FunctionService_dll = new encoding_DLL.FunctionService_dll();//宣告api
            //json = FunctionService_dll.GetFileJson(filepath);

            return json;
        }

        /// <summary>
        /// 表單JSON轉換
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        public static Dictionary<String, String> reSubmitFormDataJson(string jsonText)
        {

            Dictionary<String, String> reData = new Dictionary<String, String>();


            dynamic dynObj = JsonConvert.DeserializeObject(jsonText);
            foreach (var item in dynObj)
            {
                if ((string)item.name != "")
                {
                    //if (reData[(string)item.name] == null)
                    if (!reData.ContainsKey((string)item.name))
                    {

                        //  string val = item.value; //string val = JsonConvert.SerializeObject(item.value);

                        string val;
                        //if (item.name == "file-uploader") 
                        //{ 
                        //    val = "";
                        //}
                        //else {  val = item.value; }
                        try
                        {
                            val = item.value;
                        }
                        catch { val = ""; }



                        reData.Add((string)item.name, val);
                    }
                    else
                    {
                        string val = reData[(string)item.name] + "," + item.value;
                        reData.Remove((string)item.name);
                        reData.Add((string)item.name, val);
                    }

                }

            }


            return reData;

        }

        public static Dictionary<String, object> reSubmitFormDataJson2(string jsonText)
        {

            Dictionary<String, object> reData = new Dictionary<String, object>();


            dynamic dynObj = JsonConvert.DeserializeObject(jsonText);
            foreach (var item in dynObj)
            {
                if ((string)item.name != "")
                {
                    //if (reData[(string)item.name] == null)
                    if (!reData.ContainsKey((string)item.name))
                    {

                        //  string val = item.value; //string val = JsonConvert.SerializeObject(item.value);

                      object val;
                       // if (item.name == "file-uploader")
                       // {
                            val =item.value;
                        //}
                       // else { val = item.value; }

                        reData.Add((string)item.name, val);
                    }
                    else
                    {
                        string val = reData[(string)item.name] + "," + item.value;
                        reData.Remove((string)item.name);
                        reData.Add((string)item.name, val);
                    }

                }

            }


            return reData;

        }


        /// <summary>
        /// 取得真實IP
        /// </summary>
        /// <returns></returns>
        public static string GetIP()
        {
            string ip;
            string trueIP = string.Empty;

            //先取得是否有經過代理伺服器
            ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ip))
            {
                //將取得的 IP 字串存入陣列
                string[] ipRange = ip.Split(',');

                //比對陣列中的每個 IP
                for (int i = 0; i < ipRange.Length; i++)
                {
                    //剔除內部 IP 及不合法的 IP 後，取出第一個合法 IP
                    if (ipRange[i].Trim().Substring(0, 3) != "10." &&
                        ipRange[i].Trim().Substring(0, 7) != "192.168" &&
                        ipRange[i].Trim().Substring(0, 7) != "172.16." &&
                        CheckIP(ipRange[i].Trim()))
                    {
                        trueIP = ipRange[i].Trim();
                        break;
                    }
                }

            }
            else
            {
                //沒經過代理伺服器，直接使用 ServerVariables["REMOTE_ADDR"]
                //並經過 CheckIP( ) 的驗證
                trueIP = CheckIP(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]) ?
                     System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : "";
            }

            return trueIP;
        }

        public static bool CheckIP(string strPattern)
        {
            // 繼承自：System.Text.RegularExpressions
            // regular: ^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$
            Regex regex = new Regex("^\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}$");
            Match m = regex.Match(strPattern);

            return m.Success;
        }



        public static void saveLog(string name , Exception ex , FormCollection form)
        {

            #region 產生LOG
            string lines = "";
            string fileName = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Log/"+ name + ".txt";
            if (!System.IO.File.Exists(fileName))
            {
                //建立檔案
                // System.IO.File.Create(fileName);
                using (FileStream fs = System.IO.File.Create(fileName))
                {

                }
            }
         

            StreamReader str = new StreamReader(fileName);
            str.ReadLine();
            lines = str.ReadToEnd();
            str.Close();

            string formVal = "[";

            foreach (var key in form.AllKeys)
            {
                 formVal += key + " = " +form[key] + ",\r\n";
            }
            formVal += "]";

            lines += DateTime.Now.ToString() + "：錯誤訊息 => " + ex.Message + "，";
            lines += "\r\n";
            lines += $"Main exception occurs {ex}.";
            lines += "\r\n";
            lines += $"表單資訊："+ formVal;
            lines += "==========================================================================================================================";
            lines += "\r\n";
            System.IO.File.WriteAllText(fileName, lines);
            #endregion

        }


        public object object_encoding(object obj)
        {

            return obj;
        }
        public string string_encoding(string str)
        {

            return str;
        }
    }
}