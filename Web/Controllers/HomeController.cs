﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Web.CgmhAppService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web.Services;
using Web.Models;
using System.Web.Caching;
using System.Collections;
using System.Collections.Specialized;

namespace Web.Controllers
{
    public class HomeController : Controller
    {

        public GetServiceData AppData = new GetServiceData();

        public List<HospitalModel> Hospitals = new List<HospitalModel>();
        public HospitalModel thisHospital = new HospitalModel();
        public List<DeptModel> Depts = new List<DeptModel>();
        public DeptModel thisDept = new DeptModel();

        public List<DeptModel> Depts_video = new List<DeptModel>();
        public DeptModel thisDept_video = new DeptModel();


        public string hospitalID = "";
        public string deptID = "";
        public string deptID_video = "";

        public string patNumber = "";
        public string idNumber = "";

        public Dictionary<String, String> actived = new Dictionary<string, string>(); 

        public System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;

       [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Hospitals = AppData.GetHospital();//醫院清單  

            if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
            {
                hospitalID = RouteData.Values["id"].ToString();//目前頁面的醫院鍵值

                thisHospital = Hospitals.Find(m => m.Loc == RouteData.Values["id"].ToString());//查詢醫院
                Depts = AppData.GetDept(RouteData.Values["id"].ToString(), "");//科別    

                if (RouteData.Values["deptID"] != null && RouteData.Values["deptID"].ToString() != "")
                {
                    deptID = RouteData.Values["deptID"].ToString();//目前頁面的科別鍵值

                    thisDept = Depts.Find(m => m.deptID == RouteData.Values["deptID"].ToString());
                }
                //------------
                Depts_video = AppData.GetDept_video(RouteData.Values["id"].ToString(), "");//科別    

                if (RouteData.Values["deptID"] != null && RouteData.Values["deptID"].ToString() != "")
                {
                    deptID_video = RouteData.Values["deptID"].ToString();//目前頁面的科別鍵值

                    thisDept_video = Depts_video.Find(m => m.deptID == RouteData.Values["deptID"].ToString());
                }
            }

            //移除掛號紀錄
            if (RouteData.Values["action"] != null && RouteData.Values["action"].ToString() != "Complete")
            {

                if (Session["RegModel"] != null)
                {
                    Session.Remove("RegModel");
                    Session.Remove("form");
                }

            }



                #region 回傳給View

            ViewBag.Hospitals = Hospitals;
                ViewBag.thisHospital = thisHospital;
                ViewBag.thisDept = thisDept;
                ViewBag.deptID = deptID;
            ViewBag.thisDept_video = thisDept_video;
            ViewBag.deptID_video = deptID_video;
            #endregion
        }

        /// <summary>
        /// 首頁
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<string> North = new List<string>();
            List<string> Central = new List<string>();
            List<string> South = new List<string>();

            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            North.Add("1");
            North.Add("2");
            North.Add("3");
            North.Add("5");
            North.Add("B");
            North.Add("E");
            North.Add("N");

            Central.Add("6");
            Central.Add("M");

            South.Add("8");
            South.Add("T");

            ViewBag.North = North;
            ViewBag.Central = Central;
            ViewBag.South = South;


            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            return View();
        }

        /// <summary>
        /// 網路掛號(科別)
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (hospitalID != "" && Depts != null)
            {

                //過濾重複科別主分類
                var DeptsGroups = Depts.Where(m=>m.deptName != null);
                ViewBag.DeptsGroupsTotal = DeptsGroups.GroupBy(x => new { x.deptGroupID, x.deptGroupName });

                string deptGroupID = "";
      
                if (Request["dep"] != null && Request["dep"].ToString() != "")
                {
                    deptGroupID = Request["dep"].ToString();
                    DeptsGroups = DeptsGroups.Where(m => m.deptGroupID == deptGroupID);
                }
               
              
                ViewBag.DeptsGroups = DeptsGroups.GroupBy(x => new { x.deptGroupID, x.deptGroupName });


                //取得醫院資訊
                ViewBag.HospitalInfo = null;
                var HospitalInfo = AppData.GetHospitalInfo(hospitalID);
                if (HospitalInfo.GetType().Name != "JObject" && HospitalInfo.ToString() != "")
                {
                    ViewBag.HospitalInfo = HospitalInfo;

                }

                //於掛號時輸入時，呈現緊急通知內容
                ViewBag.GetWS143 = null;
                var WS143Model = AppData.GetWS143(hospitalID, "1");
                if (WS143Model.GetType().Name != "JObject" && WS143Model.ToString() != "")
                {
                    ViewBag.GetWS143 = WS143Model;
                }

                //ViewBag.GetWS245 = null;
                //var WS245Model = AppData.GetWS245(hospitalID, "VIDEOUSE");
                //if (WS245Model.GetType().Name != "JObject" && WS245Model.ToString() != "")
                //{
                //    ViewBag.GetWS245 = WS245Model;
                //}



                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }

        /// <summary>
        /// 網路掛號(科別)-video
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Register_video()
        {
            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (hospitalID != "" && Depts != null)
            {

                //過濾重複科別主分類
                var DeptsGroups = Depts.Where(m => m.deptName != null);
                ViewBag.DeptsGroupsTotal = DeptsGroups.GroupBy(x => new { x.deptGroupID, x.deptGroupName });

                string deptGroupID = "";

                if (Request["dep"] != null && Request["dep"].ToString() != "")
                {
                    deptGroupID = Request["dep"].ToString();
                    DeptsGroups = DeptsGroups.Where(m => m.deptGroupID == deptGroupID);
                }


                ViewBag.DeptsGroups = DeptsGroups.GroupBy(x => new { x.deptGroupID, x.deptGroupName });


                //取得醫院資訊
                ViewBag.HospitalInfo = null;
                var HospitalInfo = AppData.GetHospitalInfo(hospitalID);
                if (HospitalInfo.GetType().Name != "JObject" && HospitalInfo.ToString() != "")
                {
                    ViewBag.HospitalInfo = HospitalInfo;

                }

                //於掛號時輸入時，呈現緊急通知內容
                ViewBag.GetWS143 = null;
                var WS143Model = AppData.GetWS143(hospitalID, "1");
                if (WS143Model.GetType().Name != "JObject" && WS143Model.ToString() != "")
                {
                    ViewBag.GetWS143 = WS143Model;
                }


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }

        public ActionResult Register_showim()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (hospitalID != "" && Depts != null)
            {

                //過濾重複科別主分類
                var DeptsGroups = Depts.Where(m => m.deptName != null);
                ViewBag.DeptsGroupsTotal = DeptsGroups.GroupBy(x => new { x.deptGroupID, x.deptGroupName });

                string deptGroupID = "";

                if (Request["dep"] != null && Request["dep"].ToString() != "")
                {
                    deptGroupID = Request["dep"].ToString();
                    DeptsGroups = DeptsGroups.Where(m => m.deptGroupID == deptGroupID);
                }


                ViewBag.DeptsGroups = DeptsGroups.GroupBy(x => new { x.deptGroupID, x.deptGroupName });


                //取得醫院資訊
                ViewBag.HospitalInfo = null;
                var HospitalInfo = AppData.GetHospitalInfo(hospitalID);
                if (HospitalInfo.GetType().Name != "JObject" && HospitalInfo.ToString() != "")
                {
                    ViewBag.HospitalInfo = HospitalInfo;

                }

                //於掛號時輸入時，呈現緊急通知內容
                ViewBag.GetWS143 = null;
                var WS143Model = AppData.GetWS143(hospitalID, "1");
                if (WS143Model.GetType().Name != "JObject" && WS143Model.ToString() != "")
                {
                    ViewBag.GetWS143 = WS143Model;
                }


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }




        /// <summary>
        /// 網路掛號(醫師)
        /// </summary>
        /// <returns></returns>
        public ActionResult Department()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;


            ViewBag.addMsg = "";
            ViewBag.addMsgEnd = "N";


            if (hospitalID != "" && deptID != "" && Depts != null)
            {
                //特殊訊息或連結
                var DeptSp = AppData.GetDeptSp(hospitalID, deptID);

                if (DeptSp.GetType().Name != "JObject")
                {
                    if(DeptSp.ToString() != "")
                    {
                        switch (DeptSp[0].kd.ToString())
                        {
                            case "MSG":
                                ViewBag.addMsg = DeptSp[0].msg;
                                break;
                            case "MSGEND":
                                ViewBag.addMsg = DeptSp[0].msg;
                                ViewBag.addMsgEnd = "Y";
                                break;
                            case "URL":
                                return RedirectPermanent(DeptSp[0].msg);
                              //   break;
                               // break;
                        }
                    }
                  
                }



                List<SchedulePeriodModel> SchedulePeriods = AppData.GetSchedulePeriod(hospitalID);//取得門診表看診週別

                ViewBag.OPDSchdulesData = null;


                //科別醫生所有資料
                if (SchedulePeriods != null)
                {
                    string doctorID = "";
                    string doctorName = "";
                    if (Request["id"] != null && Request["id"].ToString() != "")
                    {
                        doctorID = Request["id"].ToString();
                    }
                    if (Request["name"] != null && Request["name"].ToString() != "")
                    {
                        doctorName = Request["name"].ToString();
                    }

                    //建立cache
                //    string cacheKey = "OPDSchdulesData" + thisHospital.Loc + thisDept.deptID;
                    dynamic OPDSchdulesLiat = null;

                    /*  if (cacheContainer.Get(cacheKey) == null)
                      {
                         var temp = AppData.GetOPDSchdule(thisHospital.Loc, "", "", "", thisDept.deptID);
                           //cacheContainer.Insert(cacheKey, temp, null, DateTime.Now.AddHours(12), TimeSpan.Zero, CacheItemPriority.Normal, null);
                           cacheContainer.Insert(cacheKey, temp, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, CacheItemPriority.Normal, null);
                          OPDSchdulesLiat = temp;
                      }
                      else
                      {

                          OPDSchdulesLiat = cacheContainer.Get(cacheKey);
                      }*/

                    OPDSchdulesLiat = AppData.GetOPDSchdule(thisHospital.Loc, "", "", "", thisDept.deptID);

                    if (OPDSchdulesLiat != null && OPDSchdulesLiat.GetType().Name != "JObject")
                    {
                        List<OPDSchduleModel> OPDSchdules = OPDSchdulesLiat;
                        Dictionary<String, Object> OPDSchdulesData = FunctionService.FormatOPDSchdule(OPDSchdules, SchedulePeriods, doctorID, doctorName);
                        ViewBag.OPDSchdulesData = OPDSchdulesData;
                    }

                }


                ViewBag.SchedulePeriods = SchedulePeriods;

                //取得科別資訊
                ViewBag.DeptInfo = null;
                var DeptInfo = AppData.GetDeptInfo(hospitalID, deptID);
                if (DeptInfo.GetType().Name != "JObject" && DeptInfo.ToString() != "")
                {
                    ViewBag.DeptInfo = DeptInfo;

                }


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }


        }

        /// <summary>
        /// 網路掛號(醫師)-video
        /// </summary>
        /// <returns></returns>
        public ActionResult Department_video()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;


            ViewBag.addMsg = "";
            ViewBag.addMsgEnd = "N";


            if (hospitalID != "" && deptID != "" && Depts != null)
            {
                //特殊訊息或連結
                var DeptSp = AppData.GetDeptSp(hospitalID, deptID);

                if (DeptSp.GetType().Name != "JObject")
                {
                    if (DeptSp.ToString() != "")
                    {
                        switch (DeptSp[0].kd.ToString())
                        {
                            case "MSG":
                                ViewBag.addMsg = DeptSp[0].msg;
                                break;
                            case "MSGEND":
                                ViewBag.addMsg = DeptSp[0].msg;
                                ViewBag.addMsgEnd = "Y";
                                break;
                            case "URL":
                                return RedirectPermanent(DeptSp[0].msg);
                                // break;
                                // break;
                        }
                    }

                }



                List<SchedulePeriodModel> SchedulePeriods = AppData.GetSchedulePeriod(hospitalID);//取得門診表看診週別

                ViewBag.OPDSchdulesData = null;


                //科別醫生所有資料
                if (SchedulePeriods != null)
                {
                    string doctorID = "";
                    string doctorName = "";
                    if (Request["id"] != null && Request["id"].ToString() != "")
                    {
                        doctorID = Request["id"].ToString();
                    }
                    if (Request["name"] != null && Request["name"].ToString() != "")
                    {
                        doctorName = Request["name"].ToString();
                    }

                    //建立cache
                    //    string cacheKey = "OPDSchdulesData" + thisHospital.Loc + thisDept.deptID;
                    dynamic OPDSchdulesLiat_video = null;

                    /*  if (cacheContainer.Get(cacheKey) == null)
                      {
                         var temp = AppData.GetOPDSchdule(thisHospital.Loc, "", "", "", thisDept.deptID);
                           //cacheContainer.Insert(cacheKey, temp, null, DateTime.Now.AddHours(12), TimeSpan.Zero, CacheItemPriority.Normal, null);
                           cacheContainer.Insert(cacheKey, temp, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, CacheItemPriority.Normal, null);
                          OPDSchdulesLiat = temp;
                      }
                      else
                      {

                          OPDSchdulesLiat = cacheContainer.Get(cacheKey);
                      }*/

                    //OPDSchdulesLiat = AppData.GetOPDSchdule(thisHospital.Loc, "", "", "", thisDept.deptID);
                  //  OPDSchdulesLiat_video = AppData.GetOPDSchdule_video(thisHospital.Loc, "", "", "", thisDept.deptID);
                    OPDSchdulesLiat_video = AppData.GetOPDSchdule_video(thisHospital.Loc, "", "", "", thisDept_video.deptID);

                    if (OPDSchdulesLiat_video != null && OPDSchdulesLiat_video.GetType().Name != "JObject")
                    {
                        List<OPDSchduleModel> OPDSchdules_video = OPDSchdulesLiat_video;
                        Dictionary<String, Object> OPDSchdulesData_video = FunctionService.FormatOPDSchdule_video(OPDSchdules_video, SchedulePeriods, doctorID, doctorName);
                        ViewBag.OPDSchdulesData_video = OPDSchdulesData_video;
                    }

                }


                ViewBag.SchedulePeriods = SchedulePeriods;

                //取得科別資訊
                ViewBag.DeptInfo = null;
                var DeptInfo = AppData.GetDeptInfo(hospitalID, deptID);
                if (DeptInfo.GetType().Name != "JObject" && DeptInfo.ToString() != "")
                {
                    ViewBag.DeptInfo = DeptInfo;

                }


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }


        }
        /// <summary>
        /// 網路掛號(醫師)-video
        /// </summary>
        /// <returns></returns>
        public ActionResult Department_showim()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;


            ViewBag.addMsg = "";
            ViewBag.addMsgEnd = "N";


            if (hospitalID != "" && deptID != "" && Depts != null)
            {
                //特殊訊息或連結
                var DeptSp = AppData.GetDeptSp(hospitalID, deptID);

                if (DeptSp.GetType().Name != "JObject")
                {
                    if (DeptSp.ToString() != "")
                    {
                        switch (DeptSp[0].kd.ToString())
                        {
                            case "MSG":
                                ViewBag.addMsg = DeptSp[0].msg;
                                break;
                            case "MSGEND":
                                ViewBag.addMsg = DeptSp[0].msg;
                                ViewBag.addMsgEnd = "Y";
                                break;
                            case "URL":
                                return RedirectPermanent(DeptSp[0].msg);
                                // break;
                                // break;
                        }
                    }

                }



                List<SchedulePeriodModel> SchedulePeriods = AppData.GetSchedulePeriod(hospitalID);//取得門診表看診週別

                ViewBag.OPDSchdulesData = null;


                //科別醫生所有資料
                if (SchedulePeriods != null)
                {
                    string doctorID = "";
                    string doctorName = "";
                    if (Request["id"] != null && Request["id"].ToString() != "")
                    {
                        doctorID = Request["id"].ToString();
                    }
                    if (Request["name"] != null && Request["name"].ToString() != "")
                    {
                        doctorName = Request["name"].ToString();
                    }

                    //建立cache
                    //    string cacheKey = "OPDSchdulesData" + thisHospital.Loc + thisDept.deptID;
                    dynamic OPDSchdulesLiat_video = null;

                    /*  if (cacheContainer.Get(cacheKey) == null)
                      {
                         var temp = AppData.GetOPDSchdule(thisHospital.Loc, "", "", "", thisDept.deptID);
                           //cacheContainer.Insert(cacheKey, temp, null, DateTime.Now.AddHours(12), TimeSpan.Zero, CacheItemPriority.Normal, null);
                           cacheContainer.Insert(cacheKey, temp, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, CacheItemPriority.Normal, null);
                          OPDSchdulesLiat = temp;
                      }
                      else
                      {

                          OPDSchdulesLiat = cacheContainer.Get(cacheKey);
                      }*/

                    //OPDSchdulesLiat = AppData.GetOPDSchdule(thisHospital.Loc, "", "", "", thisDept.deptID);
                    OPDSchdulesLiat_video = AppData.GetOPDSchdule_video(thisHospital.Loc, "", "", "", thisDept.deptID);

                    if (OPDSchdulesLiat_video != null && OPDSchdulesLiat_video.GetType().Name != "JObject")
                    {
                        List<OPDSchduleModel> OPDSchdules_video = OPDSchdulesLiat_video;
                        Dictionary<String, Object> OPDSchdulesData_video = FunctionService.FormatOPDSchdule_video(OPDSchdules_video, SchedulePeriods, doctorID, doctorName);
                        ViewBag.OPDSchdulesData_video = OPDSchdulesData_video;
                    }

                }


                ViewBag.SchedulePeriods = SchedulePeriods;

                //取得科別資訊
                ViewBag.DeptInfo = null;
                var DeptInfo = AppData.GetDeptInfo(hospitalID, deptID);
                if (DeptInfo.GetType().Name != "JObject" && DeptInfo.ToString() != "")
                {
                    ViewBag.DeptInfo = DeptInfo;

                }


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }


        }

        /// <summary>
        /// 掛號
        /// </summary>
        /// <returns></returns>
        public ActionResult Checkin()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (hospitalID != "" && deptID != "" && Depts != null 
                && RouteData.Values["opdDate"] != null && RouteData.Values["opdDate"].ToString() != "" 
                && RouteData.Values["doctorID"] != null && RouteData.Values["doctorID"].ToString() != ""
                && RouteData.Values["opdTimeID"] != null && RouteData.Values["opdTimeID"].ToString() != "")
            {
                string deptID = "";
                if(!string.IsNullOrEmpty(RouteData.Values["deptID"].ToString()))
                {
                    deptID = RouteData.Values["deptID"].ToString();
                }



                var OPDSchdules = AppData.GetOPDSchdule(RouteData.Values["id"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["doctorID"].ToString(), deptID);

                if (OPDSchdules != null && OPDSchdules.GetType().Name != "JObject")
                {
                    OPDSchduleModel OPDSchdulesData = new OPDSchduleModel();

                    List<OPDSchduleModel> OPDSchdulesList = OPDSchdules;

                    OPDSchdulesData = OPDSchdulesList.Find(m => m.opdTimeID == RouteData.Values["opdTimeID"].ToString());



                    ViewBag.OPDSchdulesData = OPDSchdulesData;

                    ViewBag.opdTimeID = RouteData.Values["opdTimeID"].ToString();
                    ViewBag.opdTimeView = "晨間";

                    switch (ViewBag.opdTimeID)
                    {
                        case "1":
                            ViewBag.opdTimeView = "上午";
                            break;
                        case "2":
                            ViewBag.opdTimeView = "下午";
                            break;
                        case "3":
                            ViewBag.opdTimeView = "夜間";
                            break;
                    }



                    //於掛號時輸入時，呈現緊急通知內容
                    ViewBag.GetWS141 = null;
                    var WS141Model = AppData.GetWS141(hospitalID, deptID, RouteData.Values["doctorID"].ToString());
                    if (WS141Model.GetType().Name != "JObject" && WS141Model.ToString() != "")
                    {
                        ViewBag.GetWS141 = WS141Model;
                    }

                    



                    if (OPDSchdulesData == null)
                    {
                        return RedirectPermanent(Url.Content("~/"));
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    Session.Remove("errorMsg");
                    Session.Add("errorMsg", OPDSchdules["message"]);
                    return RedirectPermanent(Url.Content("~/CompleteError/" + hospitalID + "/" + deptID) + "?url=" + HttpUtility.HtmlEncode("Department/" + hospitalID + "/" + deptID));


                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }

        /// <summary>
        /// 掛號-video
        /// </summary>
        /// <returns></returns>
        public ActionResult Checkin_video()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (hospitalID != "" && deptID != "" && Depts != null
                && RouteData.Values["opdDate"] != null && RouteData.Values["opdDate"].ToString() != ""
                && RouteData.Values["doctorID"] != null && RouteData.Values["doctorID"].ToString() != ""
                && RouteData.Values["opdTimeID"] != null && RouteData.Values["opdTimeID"].ToString() != "")
            {






                string deptID = "";
                if (!string.IsNullOrEmpty(RouteData.Values["deptID"].ToString()))
                {
                    deptID = RouteData.Values["deptID"].ToString();
                }



                var OPDSchdules = AppData.GetOPDSchdule(RouteData.Values["id"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["doctorID"].ToString(), deptID);

                if (OPDSchdules != null && OPDSchdules.GetType().Name != "JObject")
                {
                    OPDSchduleModel OPDSchdulesData = new OPDSchduleModel();

                    List<OPDSchduleModel> OPDSchdulesList = OPDSchdules;

                    OPDSchdulesData = OPDSchdulesList.Find(m => m.opdTimeID == RouteData.Values["opdTimeID"].ToString());



                    ViewBag.OPDSchdulesData = OPDSchdulesData;

                    ViewBag.opdTimeID = RouteData.Values["opdTimeID"].ToString();
                    ViewBag.opdTimeView = "晨間";

                    switch (ViewBag.opdTimeID)
                    {
                        case "1":
                            ViewBag.opdTimeView = "上午";
                            break;
                        case "2":
                            ViewBag.opdTimeView = "下午";
                            break;
                        case "3":
                            ViewBag.opdTimeView = "夜間";
                            break;
                    }



                    //於掛號時輸入時，呈現緊急通知內容
                    ViewBag.GetWS141 = null;
                    var WS141Model = AppData.GetWS141(hospitalID, deptID, RouteData.Values["doctorID"].ToString());
                    if (WS141Model.GetType().Name != "JObject" && WS141Model.ToString() != "")
                    {
                        ViewBag.GetWS141 = WS141Model;
                    }

                    
                    //取得縣市
                    ViewBag.TaiwanCity = FunctionService.GetTaiwan("");





                    if (OPDSchdulesData == null)
                    {
                        return RedirectPermanent(Url.Content("~/"));
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    Session.Remove("errorMsg");
                    Session.Add("errorMsg", OPDSchdules["message"]);
                    return RedirectPermanent(Url.Content("~/CompleteError/" + hospitalID + "/" + deptID) + "?url=" + HttpUtility.HtmlEncode("Department_video/" + hospitalID + "/" + deptID));


                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }




        public ActionResult WM06tmp(FormCollection form)
        {
            //Z180055538
            //ViewBag.name = "aaa";// WS06tmpData.cnm;
            //ViewBag.tel1 = "0952105115"; //WS06tmpData.tel;
            //ViewBag.tel2 = "0952105116"; //WS06tmpData.cotel;
            //ViewBag.city = "台北市";//WS06tmpData.dwncity;
            //ViewBag.district = "中山區";//; WS06tmpData.dwnarea;
            //ViewBag.address = "aaaaaa";// WS06tmpData.dwnroad;
            string hospitalID_tmp = form["hospitalID"];
            string patNumber_tmp = form["patNumber"];
            string idNumber_tmp = form["idNumber"];

            string ViewBag_name = "";
            string ViewBag_tel1 = "";
            string ViewBag_tel2 = "";
            string ViewBag_city = "";
            string ViewBag_district = "";
            string ViewBag_address = "";
            string ViewBag_lane = "";
            string ViewBag_do = "";
            string ViewBag_no = "";
            string ViewBag_floor = "";
            string ViewBag_floor_no = "";


            if (patNumber_tmp != "" || idNumber_tmp != "")
            {
                List<WS105Model> WS06tmpData = new List<WS105Model>();
                //WS06tmpData = AppData.GetWS105(hospitalID_tmp, patNumber_tmp, idNumber_tmp);
                WS06tmpData = AppData.GetWS105(HttpUtility.HtmlEncode(hospitalID_tmp), HttpUtility.HtmlEncode(patNumber_tmp), HttpUtility.HtmlEncode(idNumber_tmp));
                //WS06tmpData = AppData.GetWS105(hospitalID, patNumber, "Z180055538");
                try {
                    ViewBag_name = WS06tmpData[0].cnm;

                    string tel_tmp = WS06tmpData[0].phone1;
                    if (tel_tmp == "") { tel_tmp = WS06tmpData[0].phone2; }
                    if (tel_tmp.Length >= 2) { ViewBag_tel1 = tel_tmp.Substring(0, 2); }
                    if (tel_tmp.Length > 2) { ViewBag_tel2 = tel_tmp.Substring(2, tel_tmp.Length - 2); }
                    //ViewBag_tel1 = WS06tmpData[0].tel;
                    //ViewBag_tel2 = WS06tmpData[0].cotel;
                    ViewBag_city = WS06tmpData[0].dwncity;
                    ViewBag_district = WS06tmpData[0].dwnarea;
                    //ViewBag_address = WS06tmpData[0].dwnroad + WS06tmpData[0].address;
                    ViewBag_address = WS06tmpData[0].address; //WS06tmpData[0].dwnroad + 
                                                              //int no_address=
                                                              //正過來取
                    int no_do = ViewBag_address.IndexOf("巷");
                    if (no_do > -1)
                    {
                        ViewBag_lane = ViewBag_address.Substring(0, no_do);
                        ViewBag_address = ViewBag_address.Substring(no_do + 1, (ViewBag_address.Length - no_do - 1));
                    }
                    int no_no = ViewBag_address.IndexOf("弄");
                    if (no_no > -1)
                    {
                        ViewBag_do = ViewBag_address.Substring(0, no_no);
                        ViewBag_address = ViewBag_address.Substring(no_no + 1, (ViewBag_address.Length - no_no - 1));
                    }
                    int no_floor = ViewBag_address.IndexOf("號");
                    if (no_floor > -1)
                    {
                        ViewBag_no = ViewBag_address.Substring(0, no_floor);
                        ViewBag_address = ViewBag_address.Substring(no_floor + 1, (ViewBag_address.Length - no_floor - 1));
                    }
                    int no_floor_no = ViewBag_address.IndexOf("樓之");
                    int no_floor_no2 = ViewBag_address.IndexOf("樓");
                    int no_floor_no3 = 0;
                    if (no_floor_no <= -1) { if (no_floor_no2 > -1) { no_floor_no = no_floor_no2; no_floor_no3 = 1; } }

                    if (no_floor_no > -1)
                    {
                        ViewBag_floor = ViewBag_address.Substring(0, no_floor_no);
                        try
                        {
                            ViewBag_floor_no = ViewBag_address.Substring(no_floor_no + 2 - no_floor_no3, (ViewBag_address.Length - no_floor_no - 2 + no_floor_no3));
                        }
                        catch { }
                    }
                    ViewBag_address = WS06tmpData[0].dwnroad;





                    ////倒過來取
                    //int no_floor_no = ViewBag_address.IndexOf("樓之");
                    //int no_floor_no2 = ViewBag_address.IndexOf("樓");
                    ////if (no_floor_no <= -1) { if (no_floor_no2 > -1) { no_floor_no = no_floor_no2; } }
                    //int no_floor_no3 = 0;
                    //if (no_floor_no <= -1) { if (no_floor_no2 > -1) { no_floor_no = no_floor_no2; no_floor_no3 = 1; } }

                    //if (no_floor_no > -1)
                    //{
                    //    ViewBag_floor_no = ViewBag_address.Substring(no_floor_no+2- no_floor_no3);
                    //    ViewBag_address = ViewBag_address.Substring(0, (no_floor_no-2+ no_floor_no3));
                    //}


                    //int no_floor = ViewBag_address.IndexOf("號");
                    //if (no_floor > -1)
                    //{
                    //    ViewBag_floor = ViewBag_address.Substring(no_floor+1);
                    //    ViewBag_address = ViewBag_address.Substring(0, ( no_floor-1));
                    //}
                    //int no_no = ViewBag_address.IndexOf("弄");
                    //if (no_no > -1)
                    //{
                    //    ViewBag_no = ViewBag_address.Substring(no_no+1);
                    //    ViewBag_address = ViewBag_address.Substring(0, (no_no-1));
                    //}
                    //int no_do = ViewBag_address.IndexOf("巷");
                    //if (no_do > -1)
                    //{
                    //    ViewBag_do = ViewBag_address.Substring(no_do+1);
                    //    ViewBag_address = ViewBag_address.Substring(0, ( no_do-1));
                    //}
                    ////int no_lane = ViewBag_address.IndexOf("");
                    ////if (no_lane > -1)
                    ////{
                    ////    ViewBag_lane = ViewBag_address.Substring(no_lane);
                    ////    ViewBag_address = ViewBag_address.Substring(0, (ViewBag_address.Length - no_lane));
                    ////}
                    //ViewBag_lane = ViewBag_address;
                    //ViewBag_address = WS06tmpData[0].dwnroad ;

                    //ViewBag_lane = "A1"; //WS06tmpData.address; //巷
                    // ViewBag_do = "A2"; //WS06tmpData.address; //弄
                    // ViewBag_no = "A3"; //WS06tmpData.address; //號
                    // ViewBag_floor = "A4"; //WS06tmpData.address; //樓之
                    // ViewBag_floor_no = "A5"; //WS06tmpData.address; //樓之後面

                    //var WS141Model = AppData.GetWS141(hospitalID, deptID, RouteData.Values["doctorID"].ToString());
                    //  if (WS141Model.GetType().Name != "JObject" && WS141Model.ToString() != "")
                    //  {
                    //      ViewBag.GetWS141 = WS141Model;
                    //  }

                }
                catch { }
            }

                

            var res = new { name = ViewBag_name, tel1 = ViewBag_tel1, tel2 = ViewBag_tel2, city = ViewBag_city, district = ViewBag_district, address = ViewBag_address
            ,address_lane = ViewBag_lane,address_do = ViewBag_do,address_no = ViewBag_no,address_floor = ViewBag_floor,address_floor_no = ViewBag_floor_no
            };
            return Content(JsonConvert.SerializeObject(res));
        }
        /// <summary>
        /// 掛號確認
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult CheckinConfirm(FormCollection form)
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (hospitalID != "" && deptID != "" && Depts != null
                && RouteData.Values["opdDate"] != null && RouteData.Values["opdDate"].ToString() != ""
                && RouteData.Values["doctorID"] != null && RouteData.Values["doctorID"].ToString() != ""
                && RouteData.Values["opdTimeID"] != null && RouteData.Values["opdTimeID"].ToString() != "")
            {

                //判斷傳入是否有他院病例 -> 取得相關資料
                /*if(form["otherHaveReg"].ToString() == "Y")
                {
                    var data = AppData.getPatContactInfo(form);
                }*/
                

                List<OPDSchduleModel> OPDSchdules = AppData.GetOPDSchdule(RouteData.Values["id"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["doctorID"].ToString(), form["deptID"].ToString());

                //  OPDSchduleModel OPDSchdulesData = OPDSchdules.FirstOrDefault();

                OPDSchduleModel OPDSchdulesData = new OPDSchduleModel();

                List<OPDSchduleModel> OPDSchdulesList = OPDSchdules;

                OPDSchdulesData = OPDSchdulesList.Find(m => m.opdTimeID == RouteData.Values["opdTimeID"].ToString());


                ViewBag.OPDSchdulesData = OPDSchdulesData;

                ViewBag.opdTimeID = RouteData.Values["opdTimeID"].ToString();
                ViewBag.opdTimeView = "晨間";

                switch (ViewBag.opdTimeID)
                {
                    case "1":
                        ViewBag.opdTimeView = "上午";
                        break;
                    case "2":
                        ViewBag.opdTimeView = "下午";
                        break;
                    case "3":
                        ViewBag.opdTimeView = "夜間";
                        break;
                }


                //於掛號時輸入時，呈現緊急通知內容
                ViewBag.WSNHIQP7012Model = null;
                //var GetWSNHIQP7012Model = AppData.GetWSNHIQP701_LOCAL2(hospitalID, form.Keys[0].ToString(), "", "2", "19851122","AAAAAA", ViewBag.opdDate);
                //var GetWSNHIQP7012Model = AppData.GetWSNHIQP701_LOCAL2(hospitalID, "", "P123264498", "2", "19851122", ViewBag.deptID, ViewBag.opdDate);
                //if (GetWSNHIQP7012Model.GetType().Name != "JObject" && GetWSNHIQP7012Model.ToString() != "")
                //{
                //    ViewBag.GetWSNHIQP7012Model = GetWSNHIQP7012Model;
                //}
                 var GetWSNHIQP7012Model=new WSNHIQP7012Model();
                //GetWSNHIQP7012Model.oVaccLstDataMsg="1234";
                



                GetWSNHIQP7012Model.oVaccLstDataMsg = form["NHIQP7012memo"];
                ViewBag.GetWSNHIQP7012Model = GetWSNHIQP7012Model;

               ViewBag.From = form;
                ViewBag.TaiwanCity = FunctionService.GetTaiwan("");

                



                if (OPDSchdulesData == null)
                {
                    return RedirectPermanent(Url.Content("~/"));
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }

        /// <summary>
        /// 掛號確認-video
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult CheckinConfirm_video(FormCollection form)
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (hospitalID != "" && deptID != "" && Depts != null
                && RouteData.Values["opdDate"] != null && RouteData.Values["opdDate"].ToString() != ""
                && RouteData.Values["doctorID"] != null && RouteData.Values["doctorID"].ToString() != ""
                && RouteData.Values["opdTimeID"] != null && RouteData.Values["opdTimeID"].ToString() != "")
            {

                //判斷傳入是否有他院病例 -> 取得相關資料
                /*if(form["otherHaveReg"].ToString() == "Y")
                {
                    var data = AppData.getPatContactInfo(form);
                }*/


               // List<OPDSchduleModel> OPDSchdules = AppData.GetOPDSchdule(RouteData.Values["id"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["doctorID"].ToString(), form["deptID"].ToString());

                List<OPDSchduleModel> OPDSchdules = AppData.GetOPDSchdule_video(RouteData.Values["id"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["opdDate"].ToString(), RouteData.Values["doctorID"].ToString(), form["deptID"].ToString());

                //  OPDSchduleModel OPDSchdulesData = OPDSchdules.FirstOrDefault();

                OPDSchduleModel OPDSchdulesData = new OPDSchduleModel();

                List<OPDSchduleModel> OPDSchdulesList = OPDSchdules;

                OPDSchdulesData = OPDSchdulesList.Find(m => m.opdTimeID == RouteData.Values["opdTimeID"].ToString());


                ViewBag.OPDSchdulesData = OPDSchdulesData;

                ViewBag.opdTimeID = RouteData.Values["opdTimeID"].ToString();
                ViewBag.opdTimeView = "晨間";

                switch (ViewBag.opdTimeID)
                {
                    case "1":
                        ViewBag.opdTimeView = "上午";
                        break;
                    case "2":
                        ViewBag.opdTimeView = "下午";
                        break;
                    case "3":
                        ViewBag.opdTimeView = "夜間";
                        break;
                }


                //於掛號時輸入時，呈現緊急通知內容
                ViewBag.WSNHIQP7012Model = null;
                //var GetWSNHIQP7012Model = AppData.GetWSNHIQP701_LOCAL2(hospitalID, form.Keys[0].ToString(), "", "2", "19851122","AAAAAA", ViewBag.opdDate);
                //var GetWSNHIQP7012Model = AppData.GetWSNHIQP701_LOCAL2(hospitalID, "", "P123264498", "2", "19851122", ViewBag.deptID, ViewBag.opdDate);
                //if (GetWSNHIQP7012Model.GetType().Name != "JObject" && GetWSNHIQP7012Model.ToString() != "")
                //{
                //    ViewBag.GetWSNHIQP7012Model = GetWSNHIQP7012Model;
                //}
                var GetWSNHIQP7012Model = new WSNHIQP7012Model();
                //GetWSNHIQP7012Model.oVaccLstDataMsg="1234";




                GetWSNHIQP7012Model.oVaccLstDataMsg = form["NHIQP7012memo"];
                ViewBag.GetWSNHIQP7012Model = GetWSNHIQP7012Model;

                ViewBag.From = form;
                ViewBag.TaiwanCity = FunctionService.GetTaiwan("");





                if (OPDSchdulesData == null)
                {
                    return RedirectPermanent(Url.Content("~/"));
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }



        /// <summary>
        /// 送出掛號
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void SendToCheckin(FormCollection form)
        {
            dynamic data = null;

            string phone = form["tel1"].ToString() + form["tel2"].ToString();
            if(form["tel3"].ToString() != "")
            {
                phone = phone + "#" + form["tel3"].ToString();
            }

            form["telephone"] = phone;
            form["cellphone"] = phone;
            string address = form["city"].ToString() + form["district"].ToString() + form["address"].ToString();
         
            if(!string.IsNullOrEmpty(form["lane"].ToString()))
            {
                address = address + form["lane"].ToString() + "巷";
            }
            if (!string.IsNullOrEmpty(form["do"].ToString()))
            {
                address = address + form["do"].ToString() + "弄";
            }
            if (!string.IsNullOrEmpty(form["no"].ToString()))
            {
                address = address + form["no"].ToString() + "號";
            }
           
            if (!string.IsNullOrEmpty(form["floor"].ToString()))
            {
                address = address + form["floor"].ToString() + "樓";
            }
            if (!string.IsNullOrEmpty(form["floor_no"].ToString()))
            {
                address = address + "之"+ form["floor_no"].ToString();
            }

            form["address"] = address;

            //初診
            if (form["isFirst"].ToString() == "Y")
            {
                data = AppData.DoFirstReg(form);
            }
            //複診
            else
            {
                //取得是否超過四次未到診
                Dictionary<string, string> SymptomData = new Dictionary<string, string>();
                  SymptomData.Add("hospitalID", form["hospitalID"].ToString());
                  SymptomData.Add("chtnoID", form["patNumber"].ToString());
               // SymptomData.Add("hospitalID", "5");
               // SymptomData.Add("chtnoID", "2666474");

                var SymptomQueryData = AppData.getSymptomQuery(SymptomData);

                int SymptomQueryDataNum = 0;

                if (SymptomQueryData.GetType().Name != "JObject")
                {
                    SymptomQueryDataNum = SymptomQueryData.Count;
                }

                ViewBag.SymptomQueryDataNum = SymptomQueryDataNum;

                //未超過未到診紀錄
                if (SymptomQueryDataNum < 3)
                {
                    data = AppData.DoReg(form);
                }
                else
                {
                    Session.Remove("errorMsg");
                    Session.Add("errorMsg", "您三個月內有四次未到診紀錄，暫停受理網路預約掛號，請至掛號櫃台辦理掛號。");
                    Response.Redirect(Url.Content("~/CompleteError/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()) + "?url="+ HttpUtility.HtmlEncode("Register/" + form["hospitalID"].ToString()));                    
                    return;
                }


              

            }

          
         

            if (data != null && data["isSuccess"] == "Y" )
            {
                List<DoRegModel> resultList = JsonConvert.DeserializeObject<List<DoRegModel>>(data["resultList"].ToString());
                Session.Add("RegModel", resultList);
                Session.Add("form", form);

                Response.Redirect(Url.Content("~/Complete/"+ form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()));
            }
            else
            {
                Session.Remove("errorMsg");
                if(data == null)
                {
                    Session.Add("errorMsg", "未知錯誤，請洽院內管理人員!");
                }
                else
                {
                    Session.Add("errorMsg", data["message"]);
                }
                

                Response.Redirect(Url.Content("~/CompleteError/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()) + "?url=" + HttpUtility.HtmlEncode("Checkin/" + form["hospitalID"].ToString() + "/"+ form["TopDeptID"].ToString() + "/"+ form["opdDate"].ToString() + "/" + form["doctorID"].ToString() + "/" + form["opdTimeID"].ToString()));
            }
        }

        /// <summary>
        /// 送出掛號-video
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void SendToCheckin_video(FormCollection form)
        {
            dynamic data = null;

            string phone = form["tel1"].ToString() + form["tel2"].ToString();
            if (form["tel3"].ToString() != "")
            {
                phone = phone + "#" + form["tel3"].ToString();
            }





            form["telephone"] = phone;
            form["cellphone"] = phone;
            string address = form["city"].ToString() + form["district"].ToString() + form["address"].ToString();

            if (!string.IsNullOrEmpty(form["lane"].ToString()))
            {
                address = address + form["lane"].ToString() + "巷";
            }
            if (!string.IsNullOrEmpty(form["do"].ToString()))
            {
                address = address + form["do"].ToString() + "弄";
            }
            if (!string.IsNullOrEmpty(form["no"].ToString()))
            {
                address = address + form["no"].ToString() + "號";
            }

            if (!string.IsNullOrEmpty(form["floor"].ToString()))
            {
                address = address + form["floor"].ToString() + "樓";
            }
            if (!string.IsNullOrEmpty(form["floor_no"].ToString()))
            {
                address = address + "之" + form["floor_no"].ToString();
            }

            form["address"] = address;


            form["phone6"] = form["tel6"];
            string address6 = form["city6"].ToString()+"," + form["district6"].ToString()+"," + form["address6"].ToString();

            if (!string.IsNullOrEmpty(form["lane6"].ToString()))
            {
                address6 = address6 + form["lane6"].ToString() + "巷";
            }
            if (!string.IsNullOrEmpty(form["do6"].ToString()))
            {
                address6 = address6 + form["do6"].ToString() + "弄";
            }
            if (!string.IsNullOrEmpty(form["no6"].ToString()))
            {
                address6 = address6 + form["no6"].ToString() + "號";
            }

            if (!string.IsNullOrEmpty(form["floor6"].ToString()))
            {
                address6 = address6 + form["floor6"].ToString() + "樓";
            }
            if (!string.IsNullOrEmpty(form["floor_no6"].ToString()))
            {
                address6 = address6 + "之" + form["floor_no6"].ToString();
            }



            form["address6"] = address6;


            



            //初診
            if (form["isFirst"].ToString() == "Y")
            {
                //data = AppData.DoFirstReg(form);

                Session.Remove("errorMsg");
                Session.Add("errorMsg", "視訊專區掛號僅提供複診病患使用。");
                Response.Redirect(Url.Content("~/CompleteError/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()) + "?url=" + HttpUtility.HtmlEncode("Register_video/" + form["hospitalID"].ToString()));
                return;

            }
            //複診
            else
            {
                //取得是否超過四次未到診
                Dictionary<string, string> SymptomData = new Dictionary<string, string>();
                SymptomData.Add("hospitalID", form["hospitalID"].ToString());
                SymptomData.Add("chtnoID", form["patNumber"].ToString());
                // SymptomData.Add("hospitalID", "5");
                // SymptomData.Add("chtnoID", "2666474");

                var SymptomQueryData = AppData.getSymptomQuery(SymptomData);

                int SymptomQueryDataNum = 0;

                if (SymptomQueryData.GetType().Name != "JObject")
                {
                    SymptomQueryDataNum = SymptomQueryData.Count;
                }

                ViewBag.SymptomQueryDataNum = SymptomQueryDataNum;

                //未超過未到診紀錄
                if (SymptomQueryDataNum < 3)
                {
                    data = AppData.DoReg_video(form);
                }
                else
                {
                    Session.Remove("errorMsg");
                    Session.Add("errorMsg", "您三個月內有四次未到診紀錄，暫停受理網路預約掛號，請至掛號櫃台辦理掛號。");
                    Response.Redirect(Url.Content("~/CompleteError/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()) + "?url=" + HttpUtility.HtmlEncode("Register_video/" + form["hospitalID"].ToString()));
                    return;
                }




            }




            if (data != null && data["isSuccess"] == "Y")
            {
                List<DoRegModel> resultList = JsonConvert.DeserializeObject<List<DoRegModel>>(data["resultList"].ToString());
                Session.Add("RegModel_video", resultList);
                Session.Add("RegModel_videomsg", data["message"].ToString());
                Session.Add("form", form);
                //Response.Redirect(Url.Content("~/Complete/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()));
                Response.Redirect(Url.Content("~/Complete_video/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()));
            }
            else
            {
                Session.Remove("errorMsg");
                if (data == null)
                {
                    Session.Add("errorMsg", "未知錯誤，請洽院內管理人員!");
                }
                else
                {
                    Session.Add("errorMsg", data["message"]);
                }


                Response.Redirect(Url.Content("~/CompleteError/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()) + "?url=" + HttpUtility.HtmlEncode("Checkin_video/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString() + "/" + form["opdDate"].ToString() + "/" + form["doctorID"].ToString() + "/" + form["opdTimeID"].ToString()));
            }
        }


        /// <summary>
        /// 掛號成功
        /// </summary>
        /// <returns></returns>
        public ActionResult Complete()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (Session["RegModel"] != null)
            {
              
                List<DoRegModel> data = (List<DoRegModel>)Session["RegModel"];
                FormCollection form = (FormCollection)Session["form"];

                ViewBag.DoRegData = data;
                ViewBag.From = form;

                ViewBag.opdTimeView = "晨間";

                switch (data.FirstOrDefault().opdTimeID)
                {
                    case "1":
                        ViewBag.opdTimeView = "上午";
                        break;
                    case "2":
                        ViewBag.opdTimeView = "下午";
                        break;
                    case "3":
                        ViewBag.opdTimeView = "夜間";
                        break;
                }

                //取得未到診紀錄SymptomQueryData
                Dictionary<string, string> SymptomData = new Dictionary<string, string>();
                SymptomData.Add("hospitalID", form["hospitalID"].ToString());
                SymptomData.Add("chtnoID", form["patNumber"].ToString());
                ViewBag.SymptomQueryData = null;

                var SymptomQueryData = AppData.getSymptomQuery(SymptomData);


                var isVideodptData = AppData.GetWS242(form["hospitalID"].ToString(), data.FirstOrDefault().opdDate, data.FirstOrDefault().deptID, data.FirstOrDefault().opdTimeID, data.FirstOrDefault().doctorID);
                ViewBag.isVideodptData = isVideodptData;

                var v_chtno = form["patNumber"].ToString();
                if (v_chtno == "") { v_chtno = form["idNumber"].ToString(); }


                //診前問診 string hospitalID, string drno, string Opdvsdat, string shf, string dpt, string chtno, string dept
                var iscghiqy_Preiqy2Data = AppData.GetWS0506_cghiqy_Preiqy2(form["hospitalID"].ToString(), data.FirstOrDefault().doctorID, data.FirstOrDefault().opdDate, data.FirstOrDefault().opdTimeID, data.FirstOrDefault().deptID, v_chtno, form["TopDeptID"].ToString());
                ViewBag.iscghiqy_Preiqy2Data = iscghiqy_Preiqy2Data;

                //TOCC問診 string hospitalID, string drno, string Opdvsdat, string shf, string dpt, string chtno, string dept

                //20220621 初診會沒病歷號
                var isTOCCXAPP2Data = AppData.GetWS0506_TOCCXAPP2(form["hospitalID"].ToString(), data.FirstOrDefault().doctorID, data.FirstOrDefault().opdDate, data.FirstOrDefault().opdTimeID, data.FirstOrDefault().deptID, v_chtno, form["TopDeptID"].ToString());
                ViewBag.isTOCCXAPP2Data = isTOCCXAPP2Data;


                if (SymptomQueryData.GetType().Name != "JObject")
                {
                    //int i = 0;
                    /* foreach (SymptomQueryModel item in SymptomQueryData)
                     {
                         if (item.absdat != "" && item.loc != "")
                         {
                             i++;
                         }
                     }
                     if(i > 0)
                     {
                         ViewBag.SymptomQueryData = SymptomQueryData;
                     }*/
                    ViewBag.SymptomQueryData = SymptomQueryData;
                }
                //城市
                ViewBag.TaiwanCity = FunctionService.GetTaiwan("");

                //配合鐘SR那邊診前問卷 需要準備的參數
                //http://10.30.111.14/test/?id=e4983384-da0e-49e9-873c-1631d924d633&
                //Userid -人員代號
                //id1 病歷號
                //id2 門診號
                //id3 醫師代號
                //id4 院區
                //用 DoRegModel 的資料去查詢

                //1:請Query table ripdpreiqysheet
                // 2:http://10.30.111.14/webstructure/?id=d1f0dad0-e502-4546-838c-afbc561b1e60&USERID=A26?id=617332?id=1234567890?id=3542?id=chta1
                //3:請先上土城


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/Register/" + hospitalID));
            }
           

        }


        /// <summary>
        /// 掛號成功-video
        /// </summary>
        /// <returns></returns>
        public ActionResult Complete_video()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            if (Session["RegModel_video"] != null)
            {

                List<DoRegModel> data = (List<DoRegModel>)Session["RegModel_video"];
                FormCollection form = (FormCollection)Session["form"];

                ViewBag.DoRegData = data;
                ViewBag.From = form;

                ViewBag.opdTimeView = "晨間";

                switch (data.FirstOrDefault().opdTimeID)
                {
                    case "1":
                        ViewBag.opdTimeView = "上午";
                        break;
                    case "2":
                        ViewBag.opdTimeView = "下午";
                        break;
                    case "3":
                        ViewBag.opdTimeView = "夜間";
                        break;
                }

                //取得未到診紀錄SymptomQueryData
                Dictionary<string, string> SymptomData = new Dictionary<string, string>();
                SymptomData.Add("hospitalID", form["hospitalID"].ToString());
                SymptomData.Add("chtnoID", form["patNumber"].ToString());
                ViewBag.SymptomQueryData = null;

                var SymptomQueryData = AppData.getSymptomQuery(SymptomData);


                var isVideodptData = AppData.GetWS242(form["hospitalID"].ToString(), data.FirstOrDefault().opdDate, data.FirstOrDefault().deptID, data.FirstOrDefault().opdTimeID, data.FirstOrDefault().doctorID);
                ViewBag.isVideodptData = isVideodptData;

                var v_chtno = form["patNumber"].ToString();
                if (v_chtno == "") { v_chtno = form["idNumber"].ToString(); }


                //診前問診 string hospitalID, string drno, string Opdvsdat, string shf, string dpt, string chtno, string dept
                var iscghiqy_Preiqy2Data = AppData.GetWS0506_cghiqy_Preiqy2(form["hospitalID"].ToString(), data.FirstOrDefault().doctorID, data.FirstOrDefault().opdDate, data.FirstOrDefault().opdTimeID, data.FirstOrDefault().deptID, v_chtno, form["TopDeptID"].ToString());
                ViewBag.iscghiqy_Preiqy2Data = iscghiqy_Preiqy2Data;
                //TOCC問診 string hospitalID, string drno, string Opdvsdat, string shf, string dpt, string chtno, string dept
                var isTOCCXAPP2Data = AppData.GetWS0506_TOCCXAPP2(form["hospitalID"].ToString(), data.FirstOrDefault().doctorID, data.FirstOrDefault().opdDate, data.FirstOrDefault().opdTimeID, data.FirstOrDefault().deptID, v_chtno, form["TopDeptID"].ToString());
                ViewBag.isTOCCXAPP2Data = isTOCCXAPP2Data;



                if (SymptomQueryData.GetType().Name != "JObject")
                {
                    //int i = 0;
                    /* foreach (SymptomQueryModel item in SymptomQueryData)
                     {
                         if (item.absdat != "" && item.loc != "")
                         {
                             i++;
                         }
                     }
                     if(i > 0)
                     {
                         ViewBag.SymptomQueryData = SymptomQueryData;
                     }*/
                    ViewBag.SymptomQueryData = SymptomQueryData;
                }

                string strRegModel_videomsg = Session["RegModel_videomsg"].ToString();
                ViewBag.strRegModel_videomsg = strRegModel_videomsg;
                //Session.Remove("errorMsg");
                //Session.Add("errorMsg", strRegModel_videomsg);
                //Response.Redirect(Url.Content("~/CompleteError/" + form["hospitalID"].ToString() + "/" + form["TopDeptID"].ToString()) + "?url=" + HttpUtility.HtmlEncode("Register_video/" + form["hospitalID"].ToString()));
                //////return;


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/Register_video/" + hospitalID));
            }


        }

        /// <summary>
        /// 掛號失敗
        /// </summary>
        /// <returns></returns>
        public ActionResult CompleteError()
        {
            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;

            ViewBag.message = "掛號失敗，請聯繫相關客服，謝謝!";

            ViewBag.url = "";
            
            if(Request["url"] != null && Request["url"].ToString() != "")
            {
                ViewBag.url = Url.Content("~/" + Request["url"].ToString());
            }


            if (Session["errorMsg"] != null && Session["errorMsg"].ToString() != "")
            {
                ViewBag.message = Session["errorMsg"].ToString();
                Session.Remove("errorMsg");
            }

                return View();


        }


        /// <summary>
        /// 診間進度
        /// </summary>
        /// <returns></returns>
        public ActionResult Progress()
        {
            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            actived.Add("index", "class=\"active\"");
            ViewBag.actived = actived;
            ViewBag.deptGroupID = "";
            if (hospitalID != "" && Depts != null)
            {
                //過濾重複科別主分類
                var DeptsGroups = Depts.GroupBy(x => new { x.deptGroupID, x.deptGroupName });
                ViewBag.DeptsGroups = DeptsGroups;
                var data =  DeptsGroups.ToList();
                ViewBag.ProgressDepts = null;


                //預設科別
                if(Request["dept"] == null || Request["dept"].ToString() == "")
                {
                    DeptModel DepFirstData = data[0].FirstOrDefault();
                    //跑診所會有錯 游data[1].FirstOrDefault(); 改為 data[0].FirstOrDefault();     

                    if (DepFirstData != null)
                    {
                        ViewBag.deptGroupID = DepFirstData.deptGroupID;

                        //依科別類型取細項
                        List<DeptModel> Depts = AppData.GetDept(thisHospital.Loc, DepFirstData.deptGroupID);//科別
                        ViewBag.ProgressDepts = Depts;

                    }
                }
                else
                {
                    ViewBag.deptGroupID = Request["dept"].ToString();
                }


                ViewBag.opdTimeID = "1";

                if (Request["time"] != null && Request["time"].ToString() != "")
                {
                    ViewBag.opdTimeID = Request["time"].ToString();
                }

                Dictionary<string, string> searchData = new Dictionary<string, string>();
                searchData.Add("hospitalID", thisHospital.Loc.ToString());
                searchData.Add("language", "");
                searchData.Add("deptGrouptID", ViewBag.deptGroupID.ToString());           
                searchData.Add("opdTimeID", ViewBag.opdTimeID.ToString());

                ViewBag.ProgressData = null;

               var GetOPDProgress = AppData.GetProgressData(searchData);

                if(GetOPDProgress.GetType().Name !=  "JObject")
                {
                  //  List<ProgressDataModel> ProgressData = AppData.GetProgressData(searchData);
                    ViewBag.ProgressData = GetOPDProgress;
                }



                ViewBag.ProgressInfo = null;
                searchData.Clear();
                searchData.Add("hospitalID", thisHospital.Loc.ToString());

                var GetOPDProgressInfo = AppData.GetProgressInfo(searchData);

                if (GetOPDProgressInfo.GetType().Name != "JObject")
                {                 
                    ViewBag.ProgressInfo = GetOPDProgressInfo;
                }


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/"));
            }
        }

        /// <summary>
        /// 我要看哪一科
        /// </summary>
        /// <returns></returns>
        public ActionResult Reference()
        {
            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            actived.Add("Reference", "class=\"active\"");

            ViewBag.DPT = null;
            
            var DPT = AppData.GetDPT(hospitalID);
            if (DPT.GetType().Name != "JObject")
            {
                ViewBag.DPT = DPT;
            }

            ViewBag.DPTSub = null;

            var DPTSub = AppData.GetSubDPT(hospitalID, "");

            if (DPTSub.GetType().Name != "JObject")
            {
                ViewBag.DPTSub = DPTSub;
            }

            ViewBag.actived = actived;
            return View();
        }

        /// <summary>
        /// 掛號查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Query()
        {
            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            actived.Add("Query", "class=\"active\"");
            ViewBag.actived = actived;
            ViewBag.QueryInfo = null;

            //取得縣市
            ViewBag.TaiwanCity = FunctionService.GetTaiwan("");


            var QueryInfo = AppData.GetQueryInfo(hospitalID);
            if (QueryInfo.GetType().Name != "JObject")
            {
                ViewBag.QueryInfo = QueryInfo;
            }

            return View();
        }


        /// <summary>
        /// 掛號說明
        /// </summary>
        /// <returns></returns>
        public ActionResult Info()
        {
            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            actived.Add("Info", "class=\"active\"");
            ViewBag.actived = actived;
            //return View("info/"+hospitalID);
            return View();
        }
        /// <summary>
        /// 看診病症參考
        /// </summary>
        /// <returns></returns>
        public ActionResult Symptom()
        {
            NameValueCollection langData = new NameValueCollection();
            langData = FunctionService.getLangData("en");//取得語系檔案
            ViewBag.resLang = langData;

            actived.Add("Symptom", "class=\"active\"");
            ViewBag.actived = actived;

            ViewBag.DoctorProfile = null;
            ViewBag.DoctorSubProfile = null;
            ViewBag.DoctorProfileTitle = null;

            ViewBag.Related = null;

            string dsNo = "";

            var DoctorProfile = AppData.GetDoctorProfile(hospitalID);
            if (DoctorProfile.GetType().Name != "JObject")
            {
               
                ViewBag.DoctorProfile = DoctorProfile;
                dsNo = DoctorProfile[0].DSNO;
                if (Request["no"] != null && Request["no"].ToString() != "")
                {
                    dsNo = Request["no"].ToString();
                }

                var DoctorSubProfile = AppData.GetDoctorSubProfile(hospitalID, dsNo);





                if (DoctorSubProfile.GetType().Name != "JObject")
                {

                    //取得各類疾病相關就診科別參考之症狀建議看診科別

                    var Related = AppData.GetRelated(dsNo, DoctorSubProfile[0].DSDNO);

                    if (DoctorSubProfile.GetType().Name != "JObject")
                    {
                        ViewBag.Related = Related;
                    }


                     ViewBag.DoctorSubProfile = DoctorSubProfile;
                    List<DoctorProfileModel> DoctorProfileSearch = (List<DoctorProfileModel>)DoctorProfile;
                    ViewBag.DoctorProfileTitle = DoctorProfileSearch.Find(m => m.DSNO == dsNo).DSNM;


                }
            }




            return View();
        }

        /// <summary>
        /// 清除暫存 
        /// </summary>
        public void ClearCache()
        {
            List<string> cacheKeys = new List<string>();
            IDictionaryEnumerator cacheEnum = HttpRuntime.Cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                cacheKeys.Add(cacheEnum.Key.ToString());
            }
            foreach (string cacheKey in cacheKeys)
            {
                Response.Write(cacheKey.ToString() + "：Clear!<br>");

                HttpRuntime.Cache.Remove(cacheKey);
            }

            Response.Write("All Cache is Clear!<br>");
        }


    
    }

}