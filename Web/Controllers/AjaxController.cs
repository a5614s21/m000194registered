﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Web.Services;
using Web.Models;
using System.Collections;
using System.Text;
using System.IO;
using System.Drawing;

namespace Web.Controllers
{
    public class AjaxController : Controller
    {
        // GET: Ajax
        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [ValidateAntiForgeryToken]
        public string Ajax(FormCollection form)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            GetServiceData AppData = new GetServiceData();

            Dictionary<String, String> getData = FunctionService.reSubmitFormDataJson(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料  
            dic.Add("Func", form["Func"].ToString());

            Dictionary<String, object> getData2 = FunctionService.reSubmitFormDataJson2(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料  


            switch (form["Func"].ToString())
            {

                #region 取得科別醫院
                case "GetHospital":
                    dic["Hospital"] = "";
                    if (getData != null)
                    {
                        dynamic data = AppData.GetReference(getData["dptnm"].ToString());
                        if (data.GetType().Name != "JObject")
                        {
                            dic["Hospital"] = JsonConvert.SerializeObject(data, Formatting.Indented);
                        }
                    }

                 break;
                #endregion
                #region 進度查詢(依身分)

                case "ProgressInputSearch":

                    dic["ProgressData"] = "";
                    dic["nowTime"] = "";
                    dic["ErrorMessage"] = "";
                    dic["codeError"] = 'N';
                    dic["nowTime"] = DateTime.Now.ToString("yyyy/MM/dd tt hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);//2018/07/16 PM 03:34:56


                    if (getData != null)
                    {

                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["codeError"] = "Y";

                        }
                        else
                        {
                            if (getData != null)
                            {
                                if (getData["idType"].ToString() == "1")
                                {
                                    getData["patNumber"] = getData["idNumber"];
                                    getData["idNumber"] = "";
                                }

                                dynamic data = AppData.GetOPDProgress_Patient(getData);

                                if (data.GetType().Name != "JObject")
                                {
                                    dic["ProgressData"] = JsonConvert.SerializeObject(data, Formatting.Indented);
                                 

                                }
                                else
                                {
                                    dic["ErrorMessage"] = data["message"].ToString();
                                }

                            }
                        }
                    }
                    break;

                #endregion


                #region 取消掛號

                case "CancelReg":
                    dic["CancelData"] = "";
                    dic["ErrorMessage"] = "";
                   
                  
                        if (getData != null)
                        {

                            var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getData["json"].ToString());
                            dynamic data = AppData.GetDoRegCancel(values);

                            if (data.GetType().Name != "JObject")
                            {
                                dic["CancelData"] = JsonConvert.SerializeObject(data, Formatting.Indented);
                                dic["CancelID"] = values["deptID"].ToString() + values["opdDate"].ToString() + values["opdTimeID"].ToString();
                            }
                            else
                            {
                                dic["ErrorMessage"] = data["message"].ToString();
                            }

                        }
                    
                    break;

                #endregion
                #region 視訊預約

                case "OrderVideo":
                dic["OrderVideoData"] = "";
                dic["ErrorMessage"] = "";


                    if (getData != null)
                    {
                        var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getData["json"].ToString());
                        //var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getData["json"].ToString());
                        //var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getData["json"].ToString());
                        // dynamic data = AppData.GetDoRegCancel(values);
                        //dynamic data = AppData.GetWS241(values);

                        //var PHONE_SITE = new Object();
                        //PHONE_SITE.phone = $("#tel4").val();
                        //PHONE_SITE.city = $("#city4").val();
                        //PHONE_SITE.district4 = $("#district4").val();
                        //PHONE_SITE.address4 = $("#address4").val();
                        //PHONE_SITE.lane4 = $("#lane4").val();
                        //PHONE_SITE.do4 = $("#do4").val();
                        //PHONE_SITE.no4 = $("#no4").val();
                        //PHONE_SITE.floor4 = $("#floor4").val();
                        //PHONE_SITE.floor_no4 = $("#floor_no4").val();
                        var values_phone = getData["phone"].ToString();

                       


                        var values_city = getData["city"].ToString();
                        var values_district = getData["district"].ToString();
                        var values_lane = getData["lane"].ToString();
                        var values_do = getData["do"].ToString();
                        var values_no = getData["no"].ToString();
                        var values_floor = getData["floor"].ToString();
                        var values_floor_no = getData["floor_no"].ToString();
                        var values_address = getData["address"].ToString(); ;
                        var values_site = "";



                        if (!string.IsNullOrEmpty(values_lane))
                        {
                            values_address = values_address + values_lane + "巷";
                        }
                        if (!string.IsNullOrEmpty(values_do))
                        {
                            values_address = values_address + values_do + "弄";
                        }
                        if (!string.IsNullOrEmpty(values_no))
                        {
                            values_address = values_address + values_no + "號";
                        }

                        if (!string.IsNullOrEmpty(values_floor))
                        {
                            values_address = values_address + values_floor + "樓";
                        }
                        if (!string.IsNullOrEmpty(values_floor_no))
                        {
                            values_address = values_address + "之" + values_floor_no;
                        }

                        //地址處理
                        values_site = values_city +","+ values_district+"," + values_address;
                        var errmsge = "";
                        var upimage1 = "";
                        var upimage2 = "";
                        var upimage3 = "";
                        byte[] file_bytes_1 = new byte[0];
                        byte[] file_bytes_2= new byte[0];
                        byte[] file_bytes_3 = new byte[0];

                        var values_type1_date_y = getData["type1_date_y"].ToString().PadLeft(4, '0'); 
                        var values_type1_date_m = getData["type1_date_m"].ToString().PadLeft(2, '0'); 
                        var values_type1_date_d = getData["type1_date_d"].ToString().PadLeft(2, '0');
                        var values_type1_date = values_type1_date_y + values_type1_date_m + values_type1_date_d;

                        var values_type2_date_y = getData["type2_date_y"].ToString().PadLeft(4, '0');
                        var values_type2_date_m = getData["type2_date_m"].ToString().PadLeft(2, '0');
                        var values_type2_date_d = getData["type2_date_d"].ToString().PadLeft(2, '0');
                        var values_type2_date = values_type2_date_y + values_type2_date_m + values_type2_date_d;


                        //檔案上傳處理
                        //取得選項
                        var values_picidType = getData["picidType"].ToString();
                        if (values_picidType == "0")
                        { //非下列狀況個案

                        }
                        else if (values_picidType == "1")
                        {//快篩陽性個案
                            object values_file_uploader_1 = getData2["file-uploader1"];
                            if (values_file_uploader_1 != null)
                            {
                                file_bytes_1 = values_file_uploader_1.ToString().Trim('[', ']')
                              .Split(',')
                              .Select(x => byte.Parse(x))
                              .ToArray();
                               //dynamic data_up1 = AppData.GetWS241_ROPDDIMG2(values, "file1_1", file_bytes_1);
                                upimage1 = "Y";
                            }                            

                            object values_file_uploader_2 = getData2["file-uploader2"];
                            if (values_file_uploader_2 != null)
                            {
                                file_bytes_2 = values_file_uploader_2.ToString().Trim('[', ']')
                              .Split(',')
                              .Select(x => byte.Parse(x))
                              .ToArray();
                               // dynamic data_up2 = AppData.GetWS241_ROPDDIMG2(values, "file1_2", file_bytes_2);
                                upimage2 = "Y";
                            }
                            
                          //  if (values_file_uploader_1 ==null) {  errmsge = "快篩陽性個案需上傳附件-1.您的檢測卡匣/片"; }
                           // if (values_file_uploader_2 == null) { errmsge = "快篩陽性個案需上傳附件-2.居家隔離、自主防疫或居家檢疫相關政府文件或通知之影像"; }

                        }
                        else if (values_picidType == "2")
                        {//確診居家照護個案

                            object values_file_uploader_3 = getData2["file-uploader3"];
                            if (values_file_uploader_3 != null)
                            {
                                file_bytes_3 = values_file_uploader_3.ToString().Trim('[', ']')
                              .Split(',')
                              .Select(x => byte.Parse(x))
                              .ToArray();
                               // dynamic data_up3 = AppData.GetWS241_ROPDDIMG2(values, "file1_3", file_bytes_3);
                                upimage3 = "Y";
                            }
                            
                          //  if (values_file_uploader_3 == null) { errmsge = "確診居家照護個案需上傳附件-隔離通知書、健保快易通確診資訊或衛生局簡訊通知畫面等"; }
                        }



                        //*******************測試成功
                        //          object values_file_uploader_1= getData2["file-uploader1"];
                        //          byte[] file_bytes_1 = values_file_uploader_1.ToString().Trim('[', ']')
                        //.Split(',')
                        //.Select(x => byte.Parse(x))
                        //.ToArray();                      


                        // dynamic data_up1 = AppData.GetWS241_ROPDDIMG2(values, "file1_1", file_bytes_1);
                        //***************************


                        //20220518先關閉方便測試
                        // dynamic data = AppData.GetWS241_2(values, values_phone, values_site);
                        //dynamic data = AppData.GetWS241_3(values, values_phone, values_site, values_picidType, upimage1, file_bytes_1, upimage2, file_bytes_2, upimage3 ,file_bytes_3);
                        dynamic data = AppData.GetWS241_4(values, values_phone, values_site, values_picidType, values_type1_date, values_type2_date, upimage1, file_bytes_1, upimage2, file_bytes_2, upimage3, file_bytes_3);

                        if (data.GetType().Name != "JObject")
                            {
                                dic["OrderVideoData"] = JsonConvert.SerializeObject(data, Formatting.Indented);
                                dic["OrderVideoID"] = values["deptID"].ToString() + values["opdDate"].ToString() + values["opdTimeID"].ToString();
                            }
                            else
                            {
                                dic["ErrorMessage"] = data["message"].ToString();
                            }
                       

                    }

                    break;

                case "OrderVideo2":
                    dic["OrderVideoData"] = "";
                    dic["ErrorMessage"] = "";


                    if (getData != null)
                    {

                        // getData["ip"]=
                        

                       // var values = getData;// JsonConvert.DeserializeObject<Dictionary<string, string>>(getData["json"].ToString());
                        var values =  JsonConvert.DeserializeObject<Dictionary<string, string>>(getData["json"].ToString());
                        //string STRIP = FunctionService.GetIP();
                        //  (Dictionary<string, string> values)["ip"]=STRIP;
                        //values["ip"] = STRIP;
                        //values["ip"] = "AAA";

                        // dynamic data = AppData.GetDoRegCancel(values);
                        //  dynamic data = AppData.GetWS241(values);


                        var values_phone = getData["phone"].ToString();
                        var values_city = getData["city"].ToString();
                        var values_district = getData["district"].ToString();
                        var values_lane = getData["lane"].ToString();
                        var values_do = getData["do"].ToString();
                        var values_no = getData["no"].ToString();
                        var values_floor = getData["floor"].ToString();
                        var values_floor_no = getData["floor_no"].ToString();
                        var values_address = getData["address"].ToString(); ;
                        var values_site = "";



                        if (!string.IsNullOrEmpty(values_lane))
                        {
                            values_address = values_address + values_lane + "巷";
                        }
                        if (!string.IsNullOrEmpty(values_do))
                        {
                            values_address = values_address + values_do + "弄";
                        }
                        if (!string.IsNullOrEmpty(values_no))
                        {
                            values_address = values_address + values_no + "號";
                        }

                        if (!string.IsNullOrEmpty(values_floor))
                        {
                            values_address = values_address + values_floor + "樓";
                        }
                        if (!string.IsNullOrEmpty(values_floor_no))
                        {
                            values_address = values_address + "之" + values_floor_no;
                        }

                        //地址處理
                        values_site = values_city + "," + values_district + "," + values_address;

                        var errmsge = "";
                        var upimage1 = "";
                        var upimage2 = "";
                        var upimage3 = "";
                        byte[] file_bytes_1 = new byte[0];
                        byte[] file_bytes_2 = new byte[0];
                        byte[] file_bytes_3 = new byte[0];
                        var values_type1_date_y = getData["type1_date_y"].ToString().PadLeft(4, '0');
                        var values_type1_date_m = getData["type1_date_m"].ToString().PadLeft(2, '0');
                        var values_type1_date_d = getData["type1_date_d"].ToString().PadLeft(2, '0');
                        var values_type1_date = values_type1_date_y + values_type1_date_m + values_type1_date_d;

                        var values_type2_date_y = getData["type2_date_y"].ToString().PadLeft(4, '0');
                        var values_type2_date_m = getData["type2_date_m"].ToString().PadLeft(2, '0');
                        var values_type2_date_d = getData["type2_date_d"].ToString().PadLeft(2, '0');
                        var values_type2_date = values_type2_date_y + values_type2_date_m + values_type2_date_d;
                        //檔案上傳處理
                        //取得選項
                        var values_picidType = getData["picidType"].ToString();
                        if (values_picidType == "0")
                        { //非下列狀況個案

                        }
                        else if (values_picidType == "1")
                        {//快篩陽性個案
                            object values_file_uploader_1 = getData2["file-uploader1"];
                            if (values_file_uploader_1 != null)
                            {
                                 file_bytes_1 = values_file_uploader_1.ToString().Trim('[', ']')
                              .Split(',')
                              .Select(x => byte.Parse(x))
                              .ToArray();
                                //dynamic data_up1 = AppData.GetWS241_ROPDDIMG2(values, "file1_1", file_bytes_1);
                                upimage1 = "Y";
                            }

                            object values_file_uploader_2 = getData2["file-uploader2"];
                            if (values_file_uploader_2 != null)
                            {
                                file_bytes_2 = values_file_uploader_2.ToString().Trim('[', ']')
                              .Split(',')
                              .Select(x => byte.Parse(x))
                              .ToArray();
                             //  dynamic data_up2 = AppData.GetWS241_ROPDDIMG2(values, "file1_2", file_bytes_2);
                                upimage2 = "Y";
                            }

                            //  if (values_file_uploader_1 ==null) {  errmsge = "快篩陽性個案需上傳附件-1.您的檢測卡匣/片"; }
                            // if (values_file_uploader_2 == null) { errmsge = "快篩陽性個案需上傳附件-2.居家隔離、自主防疫或居家檢疫相關政府文件或通知之影像"; }

                        }
                        else if (values_picidType == "2")
                        {//確診居家照護個案

                            object values_file_uploader_3 = getData2["file-uploader3"];
                            if (values_file_uploader_3 != null)
                            {
                               file_bytes_3 = values_file_uploader_3.ToString().Trim('[', ']')
                              .Split(',')
                              .Select(x => byte.Parse(x))
                              .ToArray();
                               // dynamic data_up3 = AppData.GetWS241_ROPDDIMG2(values, "file1_3", file_bytes_3);
                                upimage3 = "Y";
                            }

                            //  if (values_file_uploader_3 == null) { errmsge = "確診居家照護個案需上傳附件-隔離通知書、健保快易通確診資訊或衛生局簡訊通知畫面等"; }
                        }


                        //dynamic data = AppData.GetWS241_2(values, values_phone, values_site);
                       // dynamic data = AppData.GetWS241_3(values, values_phone, values_site, values_picidType, upimage1, upimage2, upimage3);
                       // dynamic data = AppData.GetWS241_3(values, values_phone, values_site, values_picidType, upimage1, file_bytes_1, upimage2, file_bytes_2, upimage3, file_bytes_3);
                        dynamic data = AppData.GetWS241_4(values, values_phone, values_site, values_picidType, values_type1_date , values_type2_date, upimage1, file_bytes_1, upimage2, file_bytes_2, upimage3, file_bytes_3);
                        if (data.GetType().Name != "JObject")
                        {
                            dic["OrderVideoData"] = JsonConvert.SerializeObject(data, Formatting.Indented);
                            dic["OrderVideoID"] = values["deptID"].ToString() + values["opdDate"].ToString() + values["opdTimeID"].ToString();
                        }
                        else
                        {
                            dic["ErrorMessage"] = data["message"].ToString();
                        }

                    }

                    break;

                #endregion
                #region 進入視訊

                case "OrderVideoUrl":
                    dic["OrderVideoUurlData"] = "";
                    dic["ErrorMessage"] = "";
                    if (getData != null)
                    {
                        var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getData["json"].ToString());
                        //dynamic data = AppData.GetDoRegCancel(values);


                        string date_videouse = values["videouse"].ToString(); //看診連結
                        string date_videourl = values["videourl"].ToString(); //看診連結
                        string date_videoestiTime = values["videoestiTime"].ToString(); //預定看診時間
                        DateTime time1 = DateTime.ParseExact(date_videoestiTime, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);

                        if (DateTime.Now < time1)
                        { values["videouse"] = "EN"; }else{ AppData.GetWS244(values); values["videouse"] = "";  }

                        //var linkstr = date_videourl;

                            //  System.Diagnostics.Process.Start(linkstr);

                            //dynamic data = AppData.GetWS241(values);

                            //if (data.GetType().Name != "JObject")
                            //{
                            dic["OrderVideoUrlData"] = values;// JsonConvert.SerializeObject(data, Formatting.Indented);
                            dic["OrderVideoUrlID"] = values["deptID"].ToString() + values["opdDate"].ToString() + values["opdTimeID"].ToString();
                        //}
                        //else
                        //{
                        //    dic["ErrorMessage"] = data["message"].ToString();
                        //}

                    }

                    break;

                #endregion
               



                #region 掛號查詢

                case "QuerySearch":

                    dic["QueryList"] = "";
                    dic["codeError"] = 'N';
                    //20200521增加查詢時是初診還複診
                    dic["isFirst"] = "";
                    dic["strChtNo"] = "";
                    dic["strLoc"] = "";
                    dic["strBir"] = "";

                    IFormatProvider ifp = new System.Globalization.CultureInfo("zh-TW", true);

                    if (getData != null)
                    {
                      
                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["codeError"] = "Y";

                        }
                        else
                        {
                            if(getData["idType"].ToString() == "1")
                            {
                                getData["patNumber"] = getData["idNumber"];
                                getData["idNumber"] = "";                               
                            }
                            //20200521增加查詢時是初診還複診 及其他連結資料需要
                            dic["isFirst"] = getData["isFirst"];
                            try
                            {
                                if (getData["idType"].ToString() == "1") 
                                { dic["strChtNo"] = getData["patNumber"].ToString(); }
                                else { dic["strChtNo"] = "x" + getData["idNumber"].ToString(); }
                                //dic["strChtNo"] = "x" + getData["idNumber"].ToString();
                                dic["strLoc"] = getData["hospitalID"].ToString();
                                dic["strBir"] = getData["birthday"].ToString();
                            }
                            catch { }


                            dynamic data = AppData.GetRegQuery(getData);



                            if (data.GetType().Name != "JObject")
                            {
                                List<RegQueryModel> newData = new List<RegQueryModel>();
                                //增加顯示星期幾
                                foreach(RegQueryModel item in data)
                                {
                                    DateTime dt = DateTime.ParseExact(item.opdDate, "yyyyMMdd", ifp, System.Globalization.DateTimeStyles.None);
                                    item.week = FunctionService.getChtWeek(dt);
                                    newData.Add(item);
                                }

                                dic["QueryList"] = JsonConvert.SerializeObject(newData, Formatting.Indented);
                                dic["idNumber"] = getData["idNumber"].ToString();
                                dic["patNumber"] = getData["patNumber"].ToString();
                                dic["idType"] = getData["idType"].ToString();
                                dic["ip"] = FunctionService.GetIP();
                                dic["birthday"] = getData["birthday"].ToString();
                            }
                        }
                      
                    }
                  break;

                #endregion

                #region 取得區域
                case "getDistrict":

                    dic["district"] = "";
                    if (getData != null)
                    {
                        List<TaiwanAreaModel> district = FunctionService.GetTaiwan(getData["city"].ToString());
                        dic["district"] = JsonConvert.SerializeObject(district, Formatting.Indented);
                    }

                    break;
                #endregion
                #region 取得區域
                case "getDistrict4":

                    dic["district4"] = "";
                    if (getData != null)
                    {
                        List<TaiwanAreaModel> district4 = FunctionService.GetTaiwan(getData["city4"].ToString());
                        dic["district4"] = JsonConvert.SerializeObject(district4, Formatting.Indented);
                    }

                    break;
                #endregion
                #region 取得區域
                case "getDistrict5":

                    dic["district5"] = "";
                    if (getData != null)
                    {
                        List<TaiwanAreaModel> district5= FunctionService.GetTaiwan(getData["city5"].ToString());
                        dic["district5"] = JsonConvert.SerializeObject(district5, Formatting.Indented);
                    }

                    break;
                #endregion
                #region 取得區域
                case "getDistrict6":

                    dic["district6"] = "";
                    if (getData != null)
                    {
                        List<TaiwanAreaModel> district6 = FunctionService.GetTaiwan(getData["city6"].ToString());
                        dic["district6"] = JsonConvert.SerializeObject(district6, Formatting.Indented);
                    }

                    break;
                #endregion




                #region 判斷病例
                case "PatQueryChick":

                    dic["PatQuery"] = "";
                    dic["otherHospitalID"] = "";
                    dic["hospitalName"] = "";
                    dic["otherHaveReg"] = "";//他院是否已有病例 
                    dic["isFirst"] = "";

                    dic["codeError"] = "N";

                    dic["assError"] = "N";
                    dic["assErrorMessage"] = "";
                    dic["NHIQP7012memo"] = "";

                    if (getData != null)
                    {

                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["codeError"] = "Y";

                        }
                        else
                        {


                            dic["idNumber"] = getData["idNumber"].ToString();

                            List<HospitalModel> Hospitals = AppData.GetHospital();//醫院清單  


                             
                            //var PatQuery = AppData.PatQuery(
                            //      getData["hospitalID"].ToString(),//hospitalID
                            //      getData["patNumber"].ToString(),//patNumber                            
                            //     getData["idNumber"].ToString(),//idNumber
                            //     getData["idType"].ToString(),//idType
                            //     getData["birthday"].ToString()//birthday
                            //     );

                            var PatQuery = AppData.PatQuery(
                              HttpUtility.HtmlEncode(getData["hospitalID"].ToString()),//hospitalID
                              HttpUtility.HtmlEncode(getData["patNumber"].ToString()),//patNumber                            
                              HttpUtility.HtmlEncode(getData["idNumber"].ToString()),//idNumber
                              HttpUtility.HtmlEncode(getData["idType"].ToString()),//idType
                              HttpUtility.HtmlEncode(getData["birthday"].ToString())//birthday
                              );

                            //該院區為初診-查詢其他院區是否有資料
                            if (PatQuery.GetType().Name != "JObject")
                            {
                                if (PatQuery[0].isFirst == "Y" && getData["hospitalID"].ToString() != "T")
                                {
                                    string isFirst = "Y";

                                    int i = 0;
                                    while (isFirst == "Y" && i < Hospitals.Count)
                                    {
                                        HospitalModel item = (HospitalModel)Hospitals[i];

                                        //PatQuery = AppData.PatQuery(
                                        //            item.Loc,//hospitalID
                                        //            getData["patNumber"].ToString(),//patNumber      
                                        //            getData["idNumber"].ToString(),//idNumber
                                        //            getData["idType"].ToString(),//idType,
                                        //            getData["birthday"].ToString()//birthday
                                        //            );
                                        PatQuery = AppData.PatQuery(
                                                   HttpUtility.HtmlEncode(item.Loc),//hospitalID
                                                   HttpUtility.HtmlEncode(getData["patNumber"].ToString()),//patNumber      
                                                   HttpUtility.HtmlEncode(getData["idNumber"].ToString()),//idNumber
                                                   HttpUtility.HtmlEncode(getData["idType"].ToString()),//idType,
                                                   HttpUtility.HtmlEncode(getData["birthday"].ToString())//birthday
                                                   );


                                        dic["hospitalID"] = item.Loc.ToString();
                                        dic["hospitalName"] = item.hospitaName;
                                        dic["otherHaveReg"] = "Y"; //他院是否已有病例

                                        isFirst = PatQuery[0].isFirst;

                                        i++;
                                    }


                                }
                                dic["PatQuery"] = JsonConvert.SerializeObject(PatQuery, Formatting.Indented);
                                dic["isFirst"] = PatQuery[0].isFirst;
                                if (dic["isFirst"].ToString() == "N" && (getData["idType"].ToString() == "3" || getData["idType"].ToString() == "5"))
                                {
                                    dic["isFirst"] = "Y";
                                }
                                //getData["verification"].ToString();


                                //測試疫苗接口  values["deptID"].ToString() + values["opdDate"].ToString()
                                var WSNHIQP701_LOCAL2Query = AppData.GetWSNHIQP701_LOCAL2(
                                       getData["hospitalID"].ToString(),//hospitalID
                                       getData["patNumber"].ToString(),//patNumber                            
                                       getData["idNumber"].ToString(),//idNumber
                                       getData["idType"].ToString(),//idType
                                       getData["birthday"].ToString()//birthday       
                                                                    // , "10990D"
                                                                     //, "20210322"
                                      , getData["deptID"].ToString()
                                      , getData["opdDate"].ToString()
                                      );
                                if (WSNHIQP701_LOCAL2Query[0].RtnRunYN.ToString() == "Y")
                                {
                                    if (WSNHIQP701_LOCAL2Query[0].RtnCode.ToString() != "00")
                                    {
                                        dic["assError"] = "Y";
                                        dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                                    }
                                    else
                                    {
                                        if (WSNHIQP701_LOCAL2Query[0].oVaccLstData.ToString() == "N")
                                        {
                                            dic["assError"] = "Y";
                                            dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                                            dic["NHIQP7012memo"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;

                                        }
                                        else if (WSNHIQP701_LOCAL2Query[0].oVaccLstData.ToString() == "1")
                                        {
                                            // dic["assError"] = "Y";
                                            dic["NHIQP7012memo"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;

                                        }
                                        else { dic["NHIQP7012memo"] = ""; }
                                    }
                                }


                            }
                            else
                            {

                                dic["assError"] = "Y";
                                dic["assErrorMessage"] = PatQuery["message"].ToString();
                            }
                        }




                        ////測試疫苗接口
                        //var WSNHIQP701_LOCAL2Query = AppData.GetWSNHIQP701_LOCAL2(
                        //       getData["hospitalID"].ToString(),//hospitalID
                        //       getData["patNumber"].ToString(),//patNumber                            
                        //       getData["idNumber"].ToString(),//idNumber
                        //       getData["idType"].ToString(),//idType
                        //       getData["birthday"].ToString()//birthday
                        //       ,"AAAAA"
                        //       ,"20210322"
                        //      );
                        //if (WSNHIQP701_LOCAL2Query[0].RtnRunYN = "Y")
                        //{
                        //    if (WSNHIQP701_LOCAL2Query[0].RtnCode != "00")
                        //    {
                        //        dic["assError"] = "Y";
                        //        dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                        //    }
                        //    else {
                        //        if (WSNHIQP701_LOCAL2Query[0].oVaccLstData == "N")
                        //        {
                        //            dic["assError"] = "Y";
                        //            dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                        //        }
                        //     }
                        //}
                   }
                    break;


                #endregion
                #region 判斷病例
                case "PatQueryChick_video":

                    dic["PatQuery"] = "";
                    dic["otherHospitalID"] = "";
                    dic["hospitalName"] = "";
                    dic["otherHaveReg"] = "";//他院是否已有病例 
                    dic["isFirst"] = "";

                    dic["codeError"] = "N";

                    dic["assError"] = "N";
                    dic["assErrorMessage"] = "";
                    dic["NHIQP7012memo"] = "";

                    if (getData != null)
                    {

                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["codeError"] = "Y";

                        }
                        else
                        {


                            dic["idNumber"] = getData["idNumber"].ToString();

                            List<HospitalModel> Hospitals = AppData.GetHospital();//醫院清單  



                            //var PatQuery = AppData.PatQuery(
                            //      getData["hospitalID"].ToString(),//hospitalID
                            //      getData["patNumber"].ToString(),//patNumber                            
                            //     getData["idNumber"].ToString(),//idNumber
                            //     getData["idType"].ToString(),//idType
                            //     getData["birthday"].ToString()//birthday
                            //     );
                            var PatQuery = AppData.PatQuery(
                                HttpUtility.HtmlEncode(getData["hospitalID"].ToString()),//hospitalID
                                HttpUtility.HtmlEncode(getData["patNumber"].ToString()),//patNumber                            
                                HttpUtility.HtmlEncode(getData["idNumber"].ToString()),//idNumber
                                HttpUtility.HtmlEncode(getData["idType"].ToString()),//idType
                                HttpUtility.HtmlEncode(getData["birthday"].ToString())//birthday
                                );

                            //該院區為初診-查詢其他院區是否有資料
                            if (PatQuery.GetType().Name != "JObject")
                            {
                                if (PatQuery[0].isFirst == "Y" && getData["hospitalID"].ToString() != "T")
                                {
                                    string isFirst = "Y";

                                    int i = 0;
                                    while (isFirst == "Y" && i < Hospitals.Count)
                                    {
                                        HospitalModel item = (HospitalModel)Hospitals[i];

                                        //PatQuery = AppData.PatQuery(
                                        //            item.Loc,//hospitalID
                                        //            getData["patNumber"].ToString(),//patNumber      
                                        //            getData["idNumber"].ToString(),//idNumber
                                        //            getData["idType"].ToString(),//idType,
                                        //            getData["birthday"].ToString()//birthday
                                        //            );
                                        PatQuery = AppData.PatQuery(
                                                    HttpUtility.HtmlEncode(item.Loc),//hospitalID
                                                    HttpUtility.HtmlEncode(getData["patNumber"].ToString()),//patNumber      
                                                    HttpUtility.HtmlEncode(getData["idNumber"].ToString()),//idNumber
                                                    HttpUtility.HtmlEncode(getData["idType"].ToString()),//idType,
                                                    HttpUtility.HtmlEncode(getData["birthday"].ToString())//birthday
                                                    );

                                        dic["hospitalID"] = item.Loc.ToString();
                                        dic["hospitalName"] = item.hospitaName;
                                        dic["otherHaveReg"] = "Y"; //他院是否已有病例

                                        isFirst = PatQuery[0].isFirst;

                                        i++;
                                    }


                                }
                                dic["PatQuery"] = JsonConvert.SerializeObject(PatQuery, Formatting.Indented);
                                dic["isFirst"] = PatQuery[0].isFirst;
                                if (dic["isFirst"].ToString() == "N" && (getData["idType"].ToString() == "3" || getData["idType"].ToString() == "5"))
                                {
                                    dic["isFirst"] = "Y";
                                }
                                //getData["verification"].ToString();


                                //測試疫苗接口  values["deptID"].ToString() + values["opdDate"].ToString()
                                var WSNHIQP701_LOCAL2Query = AppData.GetWSNHIQP701_LOCAL2(
                                       getData["hospitalID"].ToString(),//hospitalID
                                       getData["patNumber"].ToString(),//patNumber                            
                                       getData["idNumber"].ToString(),//idNumber
                                       getData["idType"].ToString(),//idType
                                       getData["birthday"].ToString()//birthday       
                                                                     // , "10990D"
                                                                     //, "20210322"
                                      , getData["deptID"].ToString()
                                      , getData["opdDate"].ToString()
                                      );
                                if (WSNHIQP701_LOCAL2Query[0].RtnRunYN.ToString() == "Y")
                                {
                                    if (WSNHIQP701_LOCAL2Query[0].RtnCode.ToString() != "00")
                                    {
                                        dic["assError"] = "Y";
                                        dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                                    }
                                    else
                                    {
                                        if (WSNHIQP701_LOCAL2Query[0].oVaccLstData.ToString() == "N")
                                        {
                                            dic["assError"] = "Y";
                                            dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                                            dic["NHIQP7012memo"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;

                                        }
                                        else if (WSNHIQP701_LOCAL2Query[0].oVaccLstData.ToString() == "1")
                                        {
                                            // dic["assError"] = "Y";
                                            dic["NHIQP7012memo"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;

                                        }
                                        else { dic["NHIQP7012memo"] = ""; }
                                    }
                                }

                                //視訊門診 僅提供複診病患
                                if (dic["isFirst"].ToString() == "Y")
                                {
                                    dic["assError"] = "Y";
                                    dic["assErrorMessage"] = "視訊門診 僅提供複診病患使用";
                                }


                            }
                            else
                            {

                                dic["assError"] = "Y";
                                dic["assErrorMessage"] = PatQuery["message"].ToString();
                            }
                        }




                        ////測試疫苗接口
                        //var WSNHIQP701_LOCAL2Query = AppData.GetWSNHIQP701_LOCAL2(
                        //       getData["hospitalID"].ToString(),//hospitalID
                        //       getData["patNumber"].ToString(),//patNumber                            
                        //       getData["idNumber"].ToString(),//idNumber
                        //       getData["idType"].ToString(),//idType
                        //       getData["birthday"].ToString()//birthday
                        //       ,"AAAAA"
                        //       ,"20210322"
                        //      );
                        //if (WSNHIQP701_LOCAL2Query[0].RtnRunYN = "Y")
                        //{
                        //    if (WSNHIQP701_LOCAL2Query[0].RtnCode != "00")
                        //    {
                        //        dic["assError"] = "Y";
                        //        dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                        //    }
                        //    else {
                        //        if (WSNHIQP701_LOCAL2Query[0].oVaccLstData == "N")
                        //        {
                        //            dic["assError"] = "Y";
                        //            dic["assErrorMessage"] = WSNHIQP701_LOCAL2Query[0].oVaccLstDataMsg;
                        //        }
                        //     }
                        //}
                    }
                    break;


                    #endregion
            }


            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            //輸出json格式
            return json;
        }
    }

}