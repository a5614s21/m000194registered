﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BackgroundWork work = new BackgroundWork();
            work.StartWork();
        }
        public class BackgroundWork
        {

            public static object oLock = new object();
            private static Timer timer;


            // 開始背景作業
            public void StartWork()
            {
                TimeSpan delayTime = new TimeSpan(0, 0, 5); // 應用程式起動後多久開始執行
                TimeSpan intervalTime = new TimeSpan(0, 1, 0); // 應用程式起動後間隔多久重複執行
                TimerCallback timerDelegate = new TimerCallback(BatchMethod);  // 委派呼叫方法
                timer = new Timer(timerDelegate, null, delayTime, intervalTime);  // 產生 timer
            }

            // 背景批次方法
            private void BatchMethod(object pStatus)
            {

                lock (oLock)
                {
                    try
                    {

                        string CacheCheduleCclearHour = ConfigurationManager.ConnectionStrings["CacheCheduleCclearHour"].ConnectionString;

                        List<string> HourAndMin = CacheCheduleCclearHour.Split(':').ToList();


                        if (DateTime.Now.ToString("HH") == HourAndMin[0])
                        {

                            int LimitMin = int.Parse(HourAndMin[1].ToString()) + 3;

                            if(LimitMin >= 60 )
                            {
                                LimitMin = 0;
                            }


                            if (int.Parse(DateTime.Now.ToString("mm")) >= int.Parse(HourAndMin[1].ToString())
                                && (int.Parse(DateTime.Now.ToString("mm")) <= LimitMin || LimitMin == 0)
                                )
                            {
                                List<string> cacheKeys = new List<string>();
                                IDictionaryEnumerator cacheEnum = HttpRuntime.Cache.GetEnumerator();
                                while (cacheEnum.MoveNext())
                                {
                                    cacheKeys.Add(cacheEnum.Key.ToString());
                                }
                                foreach (string cacheKey in cacheKeys)
                                {
                                    HttpRuntime.Cache.Remove(cacheKey);
                                }
                            }
                          
                        }

                        
                    }
                    catch (Exception ex)
                    { }
                }
            }
        }
    }
}
