﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Web;

namespace Web.Models
{
    /// <summary>
    /// 醫院清單
    /// </summary>
    public partial class HospitalModel
    {

        public string Loc { get; set; }

        public string hospitaName { get; set; }

    }

    /// <summary>
    /// 科別資料
    /// </summary>
    public partial class DeptModel
    {

        public string deptID { get; set; }

        public string deptName { get; set; }

        public string deptGroupID { get; set; }

        public string deptGroupName { get; set; }

    }

    /// <summary>
    /// 門診表看診週別
    /// </summary>
    public partial class SchedulePeriodModel
    {

        public string weekName { get; set; }

        public string startDate { get; set; }

        public string endDate { get; set; }


    }

    /// <summary>
    /// 門診資料
    /// </summary>
    public partial class OPDSchduleModel
    {

        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string weekDay { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string endDate { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string isFull { get; set; }
        public string canReg { get; set; }
        public string memo { get; set; }
    }



    /// <summary>
    /// WS07 掛號查詢
    /// </summary>
    public partial class RegQueryModel
    {

        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string regNumber { get; set; }
        public string memo { get; set; }
        public string calledNumber { get; set; }
        public string estiTime { get; set; }
        public string name { get; set; }
        public string kd { get; set; }
        public string week { get; set; }

        //WS07_videotel 新增欄位
        public string idNumber { get; set; }
        public string spdept { get; set; }
        public string spdeptPhone { get; set; }
        public string WebdeptID { get; set; }
        public string Webdeptnm { get; set; }
        public string weekDay { get; set; }
        public string own { get; set; }
        public string videodpt { get; set; }
        public string videouse { get; set; }
        public string videourl { get; set; }
        public string videoestiTime { get; set; }
        public string teldpt { get; set; }
        public string teluse { get; set; }
        public string telurl { get; set; }
        public string telestiTime { get; set; }

        //電話 地址
        //public string phone { get; set; }
        //public string site { get; set; }

        public string ques { get; set; }

        public string toccques { get; set; }

    }

    /// <summary>
    /// WS08 取消掛號
    /// </summary>
    public partial class DoRegCancelModel
    {

        public string processTime { get; set; }
        public string memo { get; set; }

    }


    /// <summary>
    /// WS11 病歷查詢
    /// </summary>
    public partial class PatQueryModel
    {

        public string patNumber { get; set; }
        public string idNumber { get; set; }
        public string idType { get; set; }
        public string name { get; set; }
        public string isFirst { get; set; }
        public string memo { get; set; }

    }

    /// <summary>
    /// WS05 複診掛號
    /// </summary>
    public partial class DoRegModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string regNumber { get; set; }
        public string estiTime { get; set; }
        public string name { get; set; }
        public string patNumber { get; set; }
        public string memo { get; set; }
        public string isvideodpt { get; set; }
        public string isteldpt { get; set; }
        public string ip { get; set; }
    }

    /// <summary>
    /// WS06 初診掛號
    /// </summary>
    public partial class DoFirstRegModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string regNumber { get; set; }
        public string estiTime { get; set; }
        public string name { get; set; }
        public string patNumber { get; set; }
        public string memo { get; set; }
        public string isvideodpt { get; set; }
        public string isteldpt { get; set; }
    }

    public partial class OPDProgressModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorName { get; set; }
        public string calledNumber { get; set; }
        public string waitCount { get; set; }
        public string regCount { get; set; }
        public string memo { get; set; }
    }

    public partial class OPDProgressPatientModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorName { get; set; }
        public string calledNumber { get; set; }
        public string waitCount { get; set; }
        public string regCount { get; set; }
        public string regNumber { get; set; }
        public string memo { get; set; }
    }



    public partial class PatContactInfoModel
    {
        public string patNumber { get; set; }
        public string email { get; set; }
        public string cell { get; set; }
        public string tel { get; set; }
        public string ofctel { get; set; }
        public string zip { get; set; }
        public string deptID { get; set; }
        public string city { get; set; }
        public string adr { get; set; }
    }


    public partial class SymptomQueryModel
    {

        public string loc { get; set; }
        public string absdat { get; set; }

        public string rgsdpt { get; set; }

        public string rgsdptnm { get; set; }
        public string shf { get; set; }
        public string drno { get; set; }
        public string drnonm { get; set; }
        public string opdvssqno { get; set; }


    }
    public partial class WS105Model
    {
        public string loc { get; set; }
        public string chtno { get; set; }
        public string idno { get; set; }
        public string cnm { get; set; }
        public string sex { get; set; }
        public string bnrdat { get; set; }
        public string naty { get; set; }
        public string careerno { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string areno { get; set; }
        public string dwncity { get; set; }
        public string dwnarea { get; set; }
        public string dwnroad { get; set; }
        public string address { get; set; }
        public string mail { get; set; }
        public string ptrcnm { get; set; }
        public string ptrlt { get; set; }
        public string tel { get; set; }
        public string cotel { get; set; }
        public string dwncity2 { get; set; }
        public string dwnarea2 { get; set; }
        public string dwnroad2 { get; set; }
        public string address2 { get; set; }

    }


    /// <summary>
    /// 取得院區重要訊息
    /// </summary>
    public partial class HospitalInfoModel
    {
        public string SEQ { get; set; }
        public string NM
        {
            get; set;
        }

    }

    /// <summary>
    /// 取得科別說明事項
    /// </summary>
    public partial class DeptInfoModel
    {
        public string SEQ { get; set; }
        public string CN { get; set; }//CN 科別 EX:胃腸肝膽科
        public string NM { get; set; }
    }


    /// <summary>
    /// 取得科別說明事項
    /// </summary>
    public partial class DPTModel
    {
        public string BIGDPTNO { get; set; }
        public string BIGDPT { get; set; }
    }

    /// <summary>
    /// 取得科別說明事項 - 細項
    /// </summary>
    public partial class DPTSubModel
    {
        public string DTLDPNO { get; set; }
        public string DTLDP { get; set; }
        public string XDESC { get; set; }

    }



    /// <summary>
    /// 查詢特殊處理科別資料
    /// </summary>
    public partial class DeptSpModel
    {
        public string loc { get; set; }
        public string dpt { get; set; }
        public string kd { get; set; }
        public string msg { get; set; }
    }



    /// <summary>
    /// 取得各類疾病相關就診科別參考之所有病症
    /// </summary>
    public partial class DoctorProfileModel
    {
        public string DSNO { get; set; }//症狀碼
        public string DSNM { get; set; }//症狀名
    }

    /// <summary>
    /// 取得各類疾病相關就診科別參考之某一病症之說明
    /// </summary>
    public partial class DoctorProfileSubModel
    {
        public string DSDNO { get; set; }//症狀細明碼
        public string DSDDESC { get; set; }//症狀明細描述

        public string DPTS { get; set; }//建議看診科別
    }
    /// <summary>
    /// 取得掛號查詢及取消說明事項
    /// </summary>
    public partial class QueryInfoModel
    {
        public string SEQ { get; set; }//數字
        public string NM { get; set; }//症狀明細描述

    }

    /// <summary>
    /// 依建議看診科別名稱取得開診院區及科別
    /// </summary>
    public partial class ReferenceModel
    {
        public string loc { get; set; }//數字
        public string locnm { get; set; }//症狀明細描述
        public string rgsdpttc { get; set; }//症狀明細描述
    }

    /// <summary>
    /// 取得各類疾病相關就診科別參考之症狀建議看診科別
    /// </summary>
    public partial class RelatedModel
    {
        public string rgsdptcnm { get; set; }
    }

    public partial class ProgressDataModel
    {
        public string deptGroupID { get; set; }
        public string deptGroupName { get; set; }
        public string netdeptID { get; set; }
        public string netdeptName { get; set; }
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string pointId { get; set; }
        public string spointID { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorName { get; set; }
        public string calledNumber { get; set; }
        public string waitCount { get; set; }
        public string regCount { get; set; }
        public string memo { get; set; }
    }
    /// <summary>
    /// 進度說明
    /// </summary>
    public partial class ProgressInfoModel
    {
        public string SEQ { get; set; }
        public string NM { get; set; }

    }
    /// <summary>
    /// 取得門診表路徑
    /// </summary>
    public partial class DeptFileDownloadModel
    {
        public string loc { get; set; }
        public string date_ym { get; set; }
        public string filename { get; set; }
    }

    /// <summary>
    /// 台灣縣市
    /// </summary>
    public partial class TaiwanModel
    {
        public string city { get; set; }
        public string en { get; set; }
        public string Weather { get; set; }
        public List<TaiwanAreaModel> area { get; set; }
    }



    public partial class TaiwanAreaModel
    {
        public string zip { get; set; }
        public string text { get; set; }
    }


    /// <summary>
    /// 於掛號時輸入時，呈現緊急通知內容
    /// </summary>
    public partial class WS141Model
    {
        public string NM { get; set; }//通知內容
    }
    /// <summary>
    /// 於掛號完成時時，呈現緊急通知內容
    /// </summary>
    public partial class WS142Model
    {
        public string NM { get; set; }//通知內容
    }
    /// <summary>
    /// 取得首頁緊急提示文字
    /// </summary>
    public partial class WS143Model
    {
        public string NM { get; set; }//通知內容
    }

    public partial class WSNHIQP701Model
    {
        public string sLoc { get; set; }
        public string sHospId { get; set; }
        public string sPatId { get; set; }
        public string sValidSDate { get; set; }
        public string sClientRandom { get; set; }
        public string sSignature { get; set; }
        public string sSamId { get; set; }
        public string RtnCode { get; set; }
        public string oVaccLstData { get; set; }
        public string RtnRunYN { get; set; }
        public string oVaccLstDataMsg { get; set; }
    }
    public partial class WSNHIQP7012Model
    {
        public string sLoc { get; set; }
        public string sHospId { get; set; }
        public string sPatId { get; set; }
        public string sValidSDate { get; set; }
        public string sClientRandom { get; set; }
        public string sSignature { get; set; }
        public string sSamId { get; set; }
        public string RtnCode { get; set; }
        public string oVaccLstData { get; set; }
        public string RtnRunYN { get; set; }
        public string dpid { get; set; }
        public string oVaccLstDataMsg { get; set; }
    }
    /// <summary>
    /// 門診視訊會議室預約
    /// </summary>
    public partial class WS241Model
    {
        public string isSuccess { get; set; }
        public string message { get; set; }
        public string meetingUrl { get; set; }
        public string meetingTitle { get; set; }
    }
    /// <summary>
    /// 判斷該診是否是視訊門診
    /// </summary>
    public partial class WS242Model
    {
        public string isvideodpt { get; set; }
    }
    /// <summary>
    /// 門診視訊會議室 啟動註記
    /// </summary>
    public partial class WS244Model
    {
        public string isSuccess { get; set; }
        public string message { get; set; }
    }

    /// <summary>
    /// 判斷某個類型的項目是否啟用
    /// </summary>
    public partial class WS245Model
    {
        public string kd { get; set; }
        public string isuse { get; set; }
    }


    public partial class DoRegOpdnoModel
    {
        //opdvssqno, drno, opdvsdat, shf, loc, rgsdpt, week, chtno, rgsty, opdno, databasenm
        public string opdvssqno { get; set; }
        public string drno { get; set; }
        public string opdvsdat { get; set; }
        public string shf { get; set; }
        public string loc { get; set; }
        public string rgsdpt { get; set; }
        public string week { get; set; }
        public string chtno { get; set; }
        public string rgsty { get; set; }
        public string opdno { get; set; }
        public string databasenm { get; set; }
    }


    /// <summary>
    /// 判斷某個類型的項目是否啟用
    /// </summary>
    public partial class WS0506_cghiqy_Preiqy2Model
    {
        public string Preiqy_sid { get; set; }
        public string Preiqy_URL { get; set; }
    }

    /// <summary>
    /// 判斷某個類型的項目是否啟用
    /// </summary>
    public partial class WS0506_TOCCXAPP2Model
    {
        public string Preiqy_sid { get; set; }
        public string Preiqy_URL { get; set; }
    }
}
