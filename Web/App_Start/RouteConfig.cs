﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
           name: "ClearCache",
           url: "ClearCache",
           defaults: new { controller = "Home", action = "ClearCache", id = UrlParameter.Optional }
       );

            routes.MapRoute(
             name: "Ajax",
             url: "Ajax",
             defaults: new { controller = "Ajax", action = "Ajax" }
         );


            routes.MapRoute(
               name: "Progress",
               url: "Progress/{id}/{deptID}/{opdTimeID}",
               defaults: new { controller = "Home", action = "Progress", id = UrlParameter.Optional, deptID = UrlParameter.Optional, opdTimeID = UrlParameter.Optional }
           );


        
         routes.MapRoute(
              name: "Default",
              url: "{action}/{id}",
              defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
          );




            routes.MapRoute(
           name: "DefaultDeptID",
           url: "{action}/{id}/{deptID}",
           defaults: new { controller = "Home", action = UrlParameter.Optional, id = UrlParameter.Optional, deptID = UrlParameter.Optional }
       );


            routes.MapRoute(
               name: "Checkin",
               url: "{action}/{id}/{deptID}/{opdDate}/{doctorID}/{opdTimeID}",
               defaults: new { controller = "Home", action = UrlParameter.Optional, id = UrlParameter.Optional, deptID = UrlParameter.Optional, opdDate = UrlParameter.Optional, doctorID = UrlParameter.Optional, opdTimeID = UrlParameter.Optional }
           );


            /*
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            */
        }
    }
}
