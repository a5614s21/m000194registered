﻿$(document).ready(function () {
    //病例查詢
    $("body").delegate(".closeCopyData", "click", function () {

        $('#isFirst').val('Y');
        $('#CheckinForm').submit();
    });



    //我要看哪一科取得分院
    $("body").delegate(".GetHospital", "click", function () {

        //$('.openModel').trigger("click");

        setLoadPlayer('');


        var valsTemp = new Array();


        var arr = new Object();
        arr.name = "dptnm";
        arr.value = $(this).attr('data-sub');
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "DTLDPNO";
        arr.value = $(this).attr('data-no');
        valsTemp.push(arr);


        var vals = JSON.stringify(valsTemp);
        useAjax('GetHospital', vals, 'ReferenceForm');

    });

    //使用讀取中

    $('.useLoading').click(function () {

        setLoadPlayer('');

    });
    //重製驗證碼

    $('#captcha').click(function () {

        var src = $('#captcha').attr('src').split('?');

        $('#captcha').attr('src', src[0] + "?d=" + Date.now());

    });




    //病例查詢
    $("body").delegate(".PatQueryChick", "click", function () {

        $('#birthday').val($('#birthday').val().replace(/\//g, '').replace(/-/g, ''));




        if ($('#idNumber').val() == '') {

            swal({
                title: "系統訊息",
                text: "請輸入相關證號!",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#e7505a",
                confirmButtonText: "確定!",
                closeOnConfirm: false,
                cancelButtonText: "取消",
            });
            return false;
        }
        else if ($('#idNumber').val() != '') {

            if (!checkQuery('CheckinForm')) {
                return false;
            }

        }

        if ($('#birthday').val() == '' || $('#birthday').val().length != 8) {

            swal({
                title: "出生日期格式錯誤",
                text: "範例：出生日期為1985年11月25日，請輸入19851125 共8碼!",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#e7505a",
                confirmButtonText: "確定!",
                closeOnConfirm: false,
                cancelButtonText: "取消",
            });
            return false;
        }



        setLoadPlayer('');

        // $('#birthday').val($('#birthday').val().replace(/\//g, '').replace(/-/g, ''));

        var valsTemp = new Array();


        var arr = new Object();
        arr.name = "hospitalID";
        arr.value = $('#hospitalID').val();
        valsTemp.push(arr);


        var idType = "";

        $('.idType').each(function () {

            if ($(this).prop("checked")) {
                idType = $(this).val();
            }

        });


        if (idType == "1") {
            arr = new Object();
            arr.name = "patNumber";
            arr.value = $('#idNumber').val();
            valsTemp.push(arr);

            arr = new Object();
            arr.name = "idNumber";
            arr.value = "";
            valsTemp.push(arr);

        }
        else {
            arr = new Object();
            arr.name = "patNumber";
            arr.value = "";
            valsTemp.push(arr);

            arr = new Object();
            arr.name = "idNumber";
            arr.value = $('#idNumber').val();
            valsTemp.push(arr);
        }



        arr = new Object();
        arr.name = "idType";
        arr.value = idType;
        valsTemp.push(arr);




        arr = new Object();
        arr.name = "birthday";
        arr.value = $('#birthday').val();
        valsTemp.push(arr);


        arr = new Object();
        arr.name = "verification";
        arr.value = $('#verification').val();
        valsTemp.push(arr);


        /*
        arr = new Object();
        arr.name = "isFull";
        arr.value = $('#isFull').val();
        valsTemp.push(arr);



        arr = new Object();
        arr.name = "canReg";
        arr.value = $('#canReg').val();
        valsTemp.push(arr);


       


        */
        arr = new Object();
        arr.name = "deptID";
        arr.value = $('#deptID').val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "opdDate";
        arr.value = $('#opdDate').val();
        valsTemp.push(arr);

        // arr = new Object();
        //arr.name = "deptID";
        //arr.value = $(this).attr('deptID');
        //valsTemp.push(arr);

        // arr = new Object();
        //arr.name = "opdDate";
        //arr.value = $(this).attr('opdDate');
        //valsTemp.push(arr);


        var vals = JSON.stringify(valsTemp);
        useAjax('PatQueryChick', vals, 'CheckinForm');

    });

    //病例查詢
    $("body").delegate(".PatQueryChick_video", "click", function () {

        $('#birthday').val($('#birthday').val().replace(/\//g, '').replace(/-/g, ''));




        if ($('#idNumber').val() == '') {

            swal({
                title: "系統訊息",
                text: "請輸入相關證號!",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#e7505a",
                confirmButtonText: "確定!",
                closeOnConfirm: false,
                cancelButtonText: "取消",
            });
            return false;
        }
        else if ($('#idNumber').val() != '') {

            if (!checkQuery('CheckinForm')) {
                return false;
            }

        }

        if ($('#birthday').val() == '' || $('#birthday').val().length != 8) {

            swal({
                title: "出生日期格式錯誤",
                text: "範例：出生日期為1985年11月25日，請輸入19851125 共8碼!",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#e7505a",
                confirmButtonText: "確定!",
                closeOnConfirm: false,
                cancelButtonText: "取消",
            });
            return false;
        }



        setLoadPlayer('');

        // $('#birthday').val($('#birthday').val().replace(/\//g, '').replace(/-/g, ''));

        var valsTemp = new Array();


        var arr = new Object();
        arr.name = "hospitalID";
        arr.value = $('#hospitalID').val();
        valsTemp.push(arr);


        var idType = "";

        $('.idType').each(function () {

            if ($(this).prop("checked")) {
                idType = $(this).val();
            }

        });


        if (idType == "1") {
            arr = new Object();
            arr.name = "patNumber";
            arr.value = $('#idNumber').val();
            valsTemp.push(arr);

            arr = new Object();
            arr.name = "idNumber";
            arr.value = "";
            valsTemp.push(arr);

        }
        else {
            arr = new Object();
            arr.name = "patNumber";
            arr.value = "";
            valsTemp.push(arr);

            arr = new Object();
            arr.name = "idNumber";
            arr.value = $('#idNumber').val();
            valsTemp.push(arr);
        }



        arr = new Object();
        arr.name = "idType";
        arr.value = idType;
        valsTemp.push(arr);




        arr = new Object();
        arr.name = "birthday";
        arr.value = $('#birthday').val();
        valsTemp.push(arr);


        arr = new Object();
        arr.name = "verification";
        arr.value = $('#verification').val();
        valsTemp.push(arr);


        /*
        arr = new Object();
        arr.name = "isFull";
        arr.value = $('#isFull').val();
        valsTemp.push(arr);



        arr = new Object();
        arr.name = "canReg";
        arr.value = $('#canReg').val();
        valsTemp.push(arr);


       


        */
        arr = new Object();
        arr.name = "deptID";
        arr.value = $('#deptID').val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "opdDate";
        arr.value = $('#opdDate').val();
        valsTemp.push(arr);

        // arr = new Object();
        //arr.name = "deptID";
        //arr.value = $(this).attr('deptID');
        //valsTemp.push(arr);

        // arr = new Object();
        //arr.name = "opdDate";
        //arr.value = $(this).attr('opdDate');
        //valsTemp.push(arr);


        var vals = JSON.stringify(valsTemp);
        useAjax('PatQueryChick_video', vals, 'CheckinForm');

    });


    //取得縣市
    $("body").delegate("#city", "change", function () {
        debugger;
        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "city";
        arr.value = $('#city').val();
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('getDistrict', vals, 'CheckinForm');
    });

    //取得縣市-掛號查詢
    $("body").delegate("#city4", "change", function () {
    //    debugger;
        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "city4";
        arr.value = $('#city4').val();
        valsTemp.push(arr);

        var vals4 = JSON.stringify(valsTemp);
        useAjax('getDistrict4', vals4, 'OrderVideoForm');
    });



    //取得縣市-視訊掛號 輸入身分證 病歷號
    $("body").delegate("#city5", "change", function () {
           debugger;
        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "city5";
        arr.value = $('#city5').val();
        valsTemp.push(arr);

        var vals5 = JSON.stringify(valsTemp);
        useAjax('getDistrict5', vals5, 'OrderVideoForm2');
    });

    $("body").delegate("#city6", "change", function () {
        //debugger;
        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "city6";
        arr.value = $('#city6').val();
        valsTemp.push(arr);

        var vals6 = JSON.stringify(valsTemp);
        useAjax('getDistrict6', vals6, 'CheckinForm');
    });






    //開啟取消掛號視窗
    $("body").delegate(".openCheckCancelReg", "click", function () {

        $('.CancelReg').attr('data-value', '');
        $('.CancelReg').attr('data-value', $(this).attr('data-value'));


    });
    //取消掛號
    $("body").delegate(".CancelReg", "click", function () {

        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "json";
        arr.value = $(this).attr('data-value');
        valsTemp.push(arr);


        var vals = JSON.stringify(valsTemp);
        useAjax('CancelReg', vals, 'CancelForm');

    });

    //開啟視訊門診預約視窗
    $("body").delegate(".openCheckOrderVideo", "click", function () {

        $('.OrderVideo').attr('data-value', '');
        $('.OrderVideo').attr('data-value', $(this).attr('data-value'));

    });
    //門診視訊預約
    $("body").delegate(".OrderVideo", "click", function () {
        //alert('AAAAA');
       // return;

        //var valsPHONE = $(this).attr('tel4');

        //var obj1 = JSON.parse($(this).attr('data-value'));

       



        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "json";
        arr.value = $(this).attr('data-value');

      //  arr.PHONE = PHONE.phone ;
        
        valsTemp.push(arr);      

         //var PHONE_SITE = new Object();
        //PHONE_SITE.phone = $("#tel4").val();
        //PHONE_SITE.city = $("#city4").val();
        //PHONE_SITE.district4 = $("#district4").val();
        //PHONE_SITE.address4 = $("#address4").val();
        //PHONE_SITE.lane4 = $("#lane4").val();
        //PHONE_SITE.do4 = $("#do4").val();
        //PHONE_SITE.no4 = $("#no4").val();
        //PHONE_SITE.floor4 = $("#floor4").val();
        //PHONE_SITE.floor_no4 = $("#floor_no4").val();

        arr = new Object();
        arr.name = "phone";
        arr.value = $("#tel4").val();
        valsTemp.push(arr);       

        arr = new Object();
        arr.name = "city";
        arr.value = $("#city4").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "district";
        arr.value = $("#district4").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "address";
        arr.value = $("#address4").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "lane";
        arr.value = $("#lane4").val();
        valsTemp.push(arr);        

        arr = new Object();
        arr.name = "do";
        arr.value = $("#do4").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "no";
        arr.value = $("#no4").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "floor";
        arr.value = $("#floor4").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "floor_no";
        arr.value = $("#floor_no4").val();
        valsTemp.push(arr);

        //20220518-----增加檔案上傳相關的
        arr = new Object();
        arr.name = "picidType";
        //arr.value = $("#picidType0").check.val();
        arr.value = $("input:radio[name=picidType]:checked").val()
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "type1_date_y";
        arr.value = $("#type1_date_y").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type1_date_m";
        arr.value = $("#type1_date_m").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type1_date_d";
        arr.value = $("#type1_date_d").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "type2_date_y";
        arr.value = $("#type2_date_y").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type2_date_m";
        arr.value = $("#type2_date_m").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type2_date_d";
        arr.value = $("#type2_date_d").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "file-uploader1";
        arr.value = file_update1;
        valsTemp.push(arr)
        arr = new Object();
        arr.name = "file-uploader2";
        arr.value =  file_update2;
        valsTemp.push(arr)
        arr = new Object();
        arr.name = "file-uploader3";
        arr.value =  file_update3;
        valsTemp.push(arr)
        //--------------

        var vals = JSON.stringify(valsTemp);

        


       


        //var obj1 = JSON.parse(vals.replace("'","\""));

        


        useAjax('OrderVideo', vals, 'OrderVideoForm');



    });

    //開啟視訊門診預約視窗
    $("body").delegate(".openCheckOrderVideo2", "click", function () {

        $('.OrderVideo2').attr('data-value', '');
        $('.OrderVideo2').attr('data-value', $(this).attr('data-value'));

    });

    //開啟診前問卷
    $("body").delegate(".openprediagnostic", "click", function () {
        
       // valsTemp.push(arr);
        //http://10.30.111.14/test/?id=e4983384-da0e-49e9-873c-1631d924d633&
        //Userid -人員代號
        //id1 病歷號
        //id2 門診號
        //id3 醫師代號
        //id4 院區
      

        var linkstr = "http://10.30.111.14/test/?id=e4983384-da0e-49e9-873c-1631d924d633&Userid=A26?id=123?id=1234567890?id=0169?id=chta1";
        window.open(linkstr);
    });



    //門診視訊預約
    $("body").delegate(".OrderVideo2", "click", function () {
        //alert('AAAAA');
        // return;
        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "json";
        arr.value = $(this).attr('data-value');
        valsTemp.push(arr);

        //var PHONE_SITE = new Object();
        //PHONE_SITE.phone = $("#tel4").val();
        //PHONE_SITE.city = $("#city4").val();
        //PHONE_SITE.district4 = $("#district4").val();
        //PHONE_SITE.address4 = $("#address4").val();
        //PHONE_SITE.lane4 = $("#lane4").val();
        //PHONE_SITE.do4 = $("#do4").val();
        //PHONE_SITE.no4 = $("#no4").val();
        //PHONE_SITE.floor4 = $("#floor4").val();
        //PHONE_SITE.floor_no4 = $("#floor_no4").val();

        arr = new Object();
        arr.name = "phone";
        arr.value = $("#tel5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "city";
        arr.value = $("#city5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "district";
        arr.value = $("#district5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "address";
        arr.value = $("#address5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "lane";
        arr.value = $("#lane5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "do";
        arr.value = $("#do5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "no";
        arr.value = $("#no5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "floor";
        arr.value = $("#floor5").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "floor_no";
        arr.value = $("#floor_no5").val();
        valsTemp.push(arr);

        //20220518-----增加檔案上傳相關的
        arr = new Object();
        arr.name = "picidType";
        //arr.value = $("#picidType0").check.val();
        arr.value = $("input:radio[name=picidType]:checked").val()
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "type1_date_y";
        arr.value = $("#type1_date_y").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type1_date_m";
        arr.value = $("#type1_date_m").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type1_date_d";
        arr.value = $("#type1_date_d").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "type2_date_y";
        arr.value = $("#type2_date_y").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type2_date_m";
        arr.value = $("#type2_date_m").val();
        valsTemp.push(arr);
        arr = new Object();
        arr.name = "type2_date_d";
        arr.value = $("#type2_date_d").val();
        valsTemp.push(arr);

        arr = new Object();
        arr.name = "file-uploader1";
        arr.value = file_update1;//$("#file-uploader").val(); //$("#file-uploader").val();
        valsTemp.push(arr)
        arr = new Object();
        arr.name = "file-uploader2";
        arr.value = file_update2;
        valsTemp.push(arr)
        arr = new Object();
        arr.name = "file-uploader3";
        arr.value = file_update3;
        valsTemp.push(arr)
        //--------------





        var vals = JSON.stringify(valsTemp);
        useAjax('OrderVideo2', vals, 'OrderVideoForm2');

    });



    //開啟視訊門診預約視窗
    $("body").delegate(".openOrderVideoUrl", "click", function () {

        $('.OrderVideoUrl').attr('data-value', '');
        $('.OrderVideoUrl').attr('data-value', $(this).attr('data-value'));

    });
    //門診視訊預約
    $("body").delegate(".OrderVideoUrl", "click", function () {
        //alert('AAAAA');
        // return;
        var valsTemp = new Array();
        var arr = new Object();
        arr.name = "json";
        arr.value = $(this).attr('data-value');
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('OrderVideoUrl', vals, 'OrderVideoUrlForm');

    });





    $("#CheckinForm").each(function () {


        $("#CheckinForm").validate({


            submitHandler: function (form) {


                $('.submitBut').attr("disabled", true);

                return true;
            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');

                element.next("._formErrorMsg").html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');


            },
            success: function (error) {

                var findID = $('#' + error[0].id.replace('-error', ''));

                //console.log(error[0].id);

                $(findID).attr('style', '');
                findID.next(".formNotice").html('');
                return false;

            }
        });

    });

    $("#QueryForm").each(function () {


        $("#QueryForm").validate({


            submitHandler: function (form) {

                setLoadPlayer('');
                $('.submitBut').attr("disabled", true);


                if (!checkQuery('QueryForm')) {
                    setLoadPlayer('none');
                    $('.submitBut').attr("disabled", false);
                    return false;
                }

                //return true;
                $('#birthday').val($('#birthday').val().replace(/\//g, '').replace(/-/g, ''));
                var valsTemp = $("#QueryForm").find('input:not([name*=__RequestVerificationToken]),select,radio,checkbox,textarea').serializeArray();

                var vals = JSON.stringify(valsTemp);
                useAjax('QuerySearch', vals, 'QueryForm');


                return false;
            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');

                element.find('div ._formErrorMsg').html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');


            },
            success: function (error) {

                var findID = $('#' + error[0].id.replace('-error', ''));

                //console.log(error[0].id);

                $(findID).attr('style', '');
                findID.next(".formNotice").html('');
                return false;

            }
        });

    });

    $("#ProgressForm").each(function () {


        $("#ProgressForm").validate({


            submitHandler: function (form) {

                setLoadPlayer('');
                $('.submitBut').attr("disabled", true);

                if (!checkQuery('ProgressForm')) {
                    setLoadPlayer('none');
                    $('.submitBut').attr("disabled", false);
                    return false;
                }
                //return true;
                $('#birthday').val($('#birthday').val().replace(/\//g, '').replace(/-/g, ''));
                var valsTemp = $("#ProgressForm").find('input:not([name*=__RequestVerificationToken]),select,radio,checkbox,textarea').serializeArray();

                var vals = JSON.stringify(valsTemp);
                useAjax('ProgressInputSearch', vals, 'ProgressForm');


                return false;
            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');

                element.find('div ._formErrorMsg').html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');


            },
            success: function (error) {

                var findID = $('#' + error[0].id.replace('-error', ''));

                //console.log(error[0].id);

                $(findID).attr('style', '');
                findID.next(".formNotice").html('');
                return false;

            }
        });

    });
});



function useAjax(ACT, needVal, formName,cb) {

    var form = $('#' + formName);
    var token = $('input[name="__RequestVerificationToken"]', form).val();

    $.ajax({
        type: 'POST',
        url: $('#ajaxRoot').val(),
        data: {
            __RequestVerificationToken: token,
            Func: ACT,
            Val: encodeURI(needVal)
        },
        dataType: 'json',
        beforeSend: function () {



        },
        success: function (json) {

            setLoadPlayer('none');

            switch (json.Func) {

                //取得科別醫院
                case "GetHospital":



                    if (json.Hospital != "") {

                        console.log(json.Hospital);


                        var Hospital = $.parseJSON(json.Hospital);

                        $('#hospitalList').html('');

                        for (var i = 0; i < Hospital.length; i++) {

                            $('#hospitalList').append('<div class="text f18 mb20 wide10"><a href="' + $('#urlRoot').val() + 'Department/' + Hospital[i].loc + '/' + Hospital[i].rgsdpttc + '">' + Hospital[i].locnm + '</a></div>');
                        }

                        $('.openModel').trigger("click");
                    }

                    break;

                //進度查詢(依身分)
                case "ProgressInputSearch":



                    $('.submitBut').attr("disabled", false);
                    if (json.codeError == "Y") {

                        swal({
                            title: "系統訊息",
                            text: "驗證碼比對錯誤，請重新輸入",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    $('#regView').show();
                    $('#regViewList').html('');
                    $('#nowTime').text(json.nowTime);

                    if (json.ProgressData != "") {
                        var ProgressDataList = $.parseJSON(json.ProgressData);
                        debugger;
                        var copyData = '<tr><td data-title="掛號科別">{$deptName}</td><td class="fool" data-title="看診位置"> <div>{$roomLocation} </div></td><td data-title="醫師名">{$doctorName}</td><td class="f20 orange" data-title="目前看診號碼">{$calledNumber} </td></tr>';

                        for (var i = 0; i < ProgressDataList.length; i++) {
                            var addData = copyData;
                            addData = addData.replace('{$deptName}', ProgressDataList[i].deptName);
                            addData = addData.replace('{$doctorName}', ProgressDataList[i].doctorName);
                            addData = addData.replace('{$roomLocation}', ProgressDataList[i].roomLocation);
                            addData = addData.replace('{$calledNumber}', ProgressDataList[i].calledNumber);

                            $('#regViewList').append(addData);
                        }


                    }
                    else {
                        //
                        $('#regViewList').html('<tr><td colspan="4">' + json.ErrorMessage + '!</td></tr >');
                    }

                    break;

                //取消掛號
                case "CancelReg":

                    $('.modal-close').trigger("click");


                    if (json.CancelData != "") {
                        //success


                        swal({
                            title: "系統訊息",
                            text: "您的掛號已取消!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });

                        $('#' + json.CancelID).remove();

                        return false;

                    } else {

                        //error

                        swal({
                            title: "系統訊息",
                            text: json.ErrorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    break;
                //視訊預約
                case "OrderVideo":

                    $('.modal-close').trigger("click");
                    

                    if (json.OrderVideoData != "") {
                        //success
                        var x = JSON.parse(json.OrderVideoData)
                        var typestr = "warning";
                        var txtypestr = "";
                        if (x[0].isSuccess == "Y") { typestr = "success"; txtypestr ="請再次[重新查詢  您的掛號紀錄]，再點入藍色[門診視訊]按鈕後，即會呈現視訊連結畫面供您看診。"} else { typestr = "error"; }

                        swal({
                            title: "系統訊息",
                            text: x[0].message + txtypestr,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });

                        //$('#' + json.CancelID).remove();

                        return false;

                    } else {

                        //error

                        swal({
                            title: "系統訊息",
                            text: json.ErrorMessage,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    break;

                case "OrderVideo2":

                    $('.modal-close').trigger("click");


                    if (json.OrderVideoData != "") {
                        //success
                        var x = JSON.parse(json.OrderVideoData)
                        var typestr = "warning";
                        if (x[0].isSuccess == "Y") { typestr = "success"; } else { typestr = "error"; }

                        swal({
                            title: "系統訊息",
                            text: x[0].message,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });

                        //$('#' + json.CancelID).remove();

                        return false;

                    } else {

                        //error

                        swal({
                            title: "系統訊息",
                            text: json.ErrorMessage,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    break;

                //進入視訊
                case "OrderVideoUrl":

                    $('.modal-close').trigger("click");
                    // 


                    if (json.OrderVideoUrlData != "") {

                        //success
                        //var x = JSON.parse()
                        var typestr = "error";
                        //if (x[0].success == "Y") { typestr = "success"; }

                        var linkstr = json.OrderVideoUrlData.videourl;// x[0].videourl;
                        //window.open(linkstr);

                       if (json.OrderVideoUrlData.videouse != "EN") {
                            //if (json.OrderVideoUrlData.videouse == "EN") {
                            //    var vals = json.OrderVideoUrlData;
                            //    $('.OrderVideoUrlOpen').attr('data-value', $(this).attr('data-value'));


                                //var valsTemp = new Array();
                                //var arr = new Object();
                                //arr.name = "json";
                                //arr.value = $(this).attr('data-value');
                                //valsTemp.push(arr);

                               // var vals = JSON.stringify(valsTemp);
                                //useAjax('OrderVideoUrlOpen', vals, 'OrderVideoUrlOpen');


                            window.open(linkstr);
                        }
                        else {


                            swal({
                                title: "系統訊息",
                                text: "還未到預定看診前60分鐘內",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#e7505a",
                                confirmButtonText: "確定!",
                                closeOnConfirm: false,
                                cancelButtonText: "取消"
                            });
                            return false;
                        }
                        //$('#' + json.CancelID).remove();

                       

                    } else {

                        //error

                        swal({
                            title: "系統訊息",
                            text: json.ErrorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    break;


                //進入視訊專區
                case "RegisterVideoUrl":

                    $('.modal-close').trigger("click");
                    // 


                    if (json.OrderVideoUrlData != "") {

                      
                        var typestr = "error";
                        var linkstr = json.OrderVideoUrlData.videourl;// x[0].videourl;
                        

                        if (json.OrderVideoUrlData.videouse != "EN") {
                            //if (json.OrderVideoUrlData.videouse == "EN") {
                            //    var vals = json.OrderVideoUrlData;
                            //    $('.OrderVideoUrlOpen').attr('data-value', $(this).attr('data-value'));


                            //var valsTemp = new Array();
                            //var arr = new Object();
                            //arr.name = "json";
                            //arr.value = $(this).attr('data-value');
                            //valsTemp.push(arr);

                            // var vals = JSON.stringify(valsTemp);
                            //useAjax('OrderVideoUrlOpen', vals, 'OrderVideoUrlOpen');


                            window.open(linkstr);
                        }
                       
                        //$('#' + json.CancelID).remove();



                    } else {

                       
                        return false;
                    }

                    break;



                //病例查詢
                case "QuerySearch":

                    $('.submitBut').attr("disabled", false);
                    if (json.codeError == "Y") {

                        swal({
                            title: "系統訊息",
                            text: "驗證碼比對錯誤，請重新輸入",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    $('#regView').show();
                    $('#regViewList').html('');
                    //debugger;
                    if (json.isFirst == "Y") {
                        $("#getudt").show();
                        $("#getudt").on("click", function () {
                            var linkstr = "https://register1.cgmh.org.tw/refs/PatientHistory3_2.aspx?Id=" + json.strChtNo + "&Loc=" + json.strLoc + "&Bir=" + json.strBir;
                            window.open(linkstr);
                        });


                    }
                    else { $("#getudt").hide(); }
                    if (json.QueryList != "") {
                        var QueryList = $.parseJSON(json.QueryList);

                        var name = "";
                        var copyData = '<tr id="{$id}"><td>{$hospitaName}</td><td class="orange">{$opdDate}</td><td>{$opdTimeID}</td><td>{$deptName}</td><td>{$doctorName}</td><td>{$regNumber} </td>';
                        var copyData2 = '<td>{$area}</td><td>{$estiTime}</td><td class="sm"> <a class="btn cancel openCheckCancelReg" href="#modal-cancel" data-value="{$Json}" data-lity>取消掛號</a></td></tr>';

                        //video_N
                        //var copyData_N = '<tr id="{$id}"><td>{$hospitaName}</td><td class="orange">{$opdDate}</td><td>{$opdTimeID}</td><td>{$deptName}</td><td >{$doctorName}</td><td>{$regNumber} </td><td></td><td></td><td>{$area}</td><td>{$estiTime}</td><td class="sm"> <a class="btn cancel openCheckCancelReg" href="#modal-cancel" data-value="{$Json}" data-lity>取消<br />掛號</a></td></tr>';
                        var copyData_N = '<td></td>';

                        //video_Y
                        //var copyData_Y = '<tr id="{$id}"><td>{$hospitaName}</td><td class="orange">{$opdDate}</td><td>{$opdTimeID}</td><td>{$deptName}</td><td >{$doctorName}</td><td>{$regNumber} </td><td> <a class="btn video openCheckOrderVideo" href="#modal-video" data-value="{$Json}" data-lity>視訊<br />預約</a></td><td></td><td>{$area}</td><td>{$estiTime}</td><td class="sm"> <a class="btn cancel openCheckCancelReg" href="#modal-cancel" data-value="{$Json}" data-lity>取消<br />掛號</a></td></tr>';
                        var copyData_Y = '<td> <a class="btn video openCheckOrderVideo" href="#modal-video" data-value="{$Json}" data-lity>視訊<br />預約</a></td>';

                        //video_E
                        //var copyData_E = '<tr id="{$id}"><td>{$hospitaName}</td><td class="orange">{$opdDate}</td><td>{$opdTimeID}</td><td>{$deptName}</td><td >{$doctorName}</td><td>{$regNumber} </td><td> <a class="btn video openOrderVideoUrl" href="#modal-videourl" data-value="{$Json}" data-lity>門診<br />視訊</a></td><td></td><td>{$area}</td><td>{$estiTime}</td><td class="sm"> <a class="btn cancel openCheckCancelReg" href="#modal-cancel" data-value="{$Json}" data-lity>取消<br />掛號</a></td></tr>';
                        var copyData_E = '<td> <a class="btn video openOrderVideoUrl" href="#modal-videourl" data-value="{$Json}" data-lity>門診<br />視訊</a></td>';

                        var copyData_ques = '';
                        var copyData_toccques = '';

                        for (var i = 0; i < QueryList.length; i++) {

                            name = QueryList[i].name;
                            var addData = copyData;
                            if (QueryList[i].videouse == "N") { addData = addData +  copyData_N; }
                            else if (QueryList[i].videouse == "Y") { addData = addData +  copyData_Y; }
                            else if (QueryList[i].videouse == "E") { addData = addData + copyData_E; }


                            addData = addData + '<td>';
                            //判斷診前問卷資料有沒有內容
                            if (QueryList[i].ques != "") { copyData_ques = '<a class="btn video" href={$ques} target="_blank">診前<br />問卷</a>';}
                            else { copyData_ques = ''; }
                            addData = addData + copyData_ques;
                            if (QueryList[i].toccques != "") { copyData_toccques= '<a class="btn video" href={$toccques} target="_blank">TOCC<br />問卷</a>'; }
                            else { copyData_toccques = ''; }
                            addData = addData + copyData_toccques;
                            addData = addData + '</td>';




                          //  addData = addData + copyData_ques;

                            addData = addData+ copyData2;


                            QueryList[i].hospitalID = $('#hospitalID').val();
                            QueryList[i].patNumber = json.patNumber;
                            QueryList[i].idNumber = json.idNumber;
                            QueryList[i].idType = json.idType;
                            QueryList[i].ip = json.ip;
                            QueryList[i].birthday = json.birthday;
                            // QueryList[i].roomLocation = json.roomLocation;


                            addData = addData.replace('{$id}', QueryList[i].deptID + QueryList[i].opdDate + QueryList[i].opdTimeID);

                            addData = addData.replace('{$hospitaName}', $('#hospitaName').text());

                            var opdDate = QueryList[i].opdDate.substr(0, 4) + "-" + QueryList[i].opdDate.substr(4, 2) + "-" + QueryList[i].opdDate.substr(6, 2);
                            addData = addData.replace('{$opdDate}', opdDate + '(' + QueryList[i].week + ')');
                            var opdTimeID = "上午";

                            switch (QueryList[i].opdTimeID) {
                                case "2":
                                    opdTimeID = "下午";
                                    break;
                                case "3":
                                    opdTimeID = "夜間";
                                    break;
                            }
                            addData = addData.replace('{$opdTimeID}', opdTimeID);
                            addData = addData.replace('{$deptName}', QueryList[i].deptName);
                            addData = addData.replace('{$doctorName}', QueryList[i].doctorName);
                            addData = addData.replace('{$regNumber}', QueryList[i].regNumber);
                            addData = addData.replace('{$area}', QueryList[i].roomLocation);
                            // addData = addData.replace('{$info}', QueryList[i].memo);
                            addData = addData.replace('{$estiTime}', QueryList[i].estiTime);
                            //addData = addData.replace('{$Json}', JSON.stringify(QueryList[i]).replace(/\"/g, "'"));
                            addData = addData.replace(/\{\$Json\}/g, JSON.stringify(QueryList[i]).replace(/\"/g, "'"));


                            addData = addData.replace('{$ques}', QueryList[i].ques);
                            addData = addData.replace('{$toccques}', QueryList[i].toccques);
                            $('#regViewList').append(addData);
                        }

                        $('#name').text(name);
                    }
                    else {
                        //
                        $('#regViewList').html('<tr><td colspan="8">查無掛號資訊!</td></tr >');
                        $('#showName').hide();

                    }
                    break;



                //判斷病例
                case "PatQueryChick":


                    setLoadPlayer('none');


                    $('.submitBut').attr("disabled", false);
                    if (json.codeError == "Y") {

                        swal({
                            title: "系統訊息",
                            text: "驗證碼比對錯誤，請重新輸入",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    if (json.assError == "Y") {

                        swal({
                            title: "系統訊息",
                            text: json.assErrorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    if (json.isFirst == "N" && $('#isFull').val() == "Y" && $('#canReg').val() == "Y") {

                        swal({
                            title: "系統訊息",
                            text: "抱歉，您非初診病人，醫師本日目前已額滿!! 請選擇其他門診重新掛號",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }


                  


                    var PatQuery = $.parseJSON(json.PatQuery);

                    //$('#isFirst').val(PatQuery[0].isFirst);
                    $('#patNumber').val(PatQuery[0].patNumber);
                    $('#name').val(PatQuery[0].name);
                    $('#otherHaveReg').val(json.otherHaveReg);//紀錄他院有病例打開跨區訊息
                    $('#otherHospitalID').val(json.otherHospitalID);//紀錄他院有病例打開跨區訊息(分院ID)

                    $('#isFirst').val(json.isFirst);


                    //用來呈現打針又在其他醫院預約的訊息
                    if (json.assError == "N") {
                        if (json.NHIQP7012memo != "") {
                            // json.PatQuery
                            //$('#name').val(json.assErrorMessage);
                            //PatQuery[0].memo = json.assErrorMessage;
                            $('#NHIQP7012memo').val(json.NHIQP7012memo );
                        }
                    }
                    //-------------


                    if (PatQuery[0].isFirst == "N") {


                        //他院有病例打開跨區訊息
                        if (json.otherHaveReg == "Y" && $('#hospitalID').val() != json.hospitalID) {

                            $('.openModel').trigger("click");

                            $('#hospitalName').text(json.hospitalName);
                            $('#idNo').text(json.idNumber);
                        }
                        else {
                            //相同分院直接送出
                            $('#patNumber').val(PatQuery[0].patNumber);
                            form.submit();

                        }




                    }
                    else {
                        form.submit();
                    }



                    break;

                //判斷病例
                case "PatQueryChick_video":

                    setLoadPlayer('none');


                    $('.submitBut').attr("disabled", false);
                    if (json.codeError == "Y") {

                        swal({
                            title: "系統訊息",
                            text: "驗證碼比對錯誤，請重新輸入",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    if (json.assError == "Y") {

                        swal({
                            title: "系統訊息",
                            text: json.assErrorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    }

                    //if ($('#isFull').val() == "Y" && $('#canReg').val() == "Y") {
                        if ( $('#canReg').val() == "N") {
                        swal({
                            title: "系統訊息",
                            text: "抱歉，醫師本日目前已額滿!! 請選擇其他門診重新掛號",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消"
                        });
                        return false;
                    } 
                    //視訊僅提供複診病患使用
                    //if (json.isFirst == "Y" ) {

                    //    swal({
                    //        title: "系統訊息",
                    //        text: "抱歉，您是初診病人，視訊門診僅提供複診病患使用!! ",
                    //        type: "error",
                    //        showCancelButton: false,
                    //        confirmButtonColor: "#e7505a",
                    //        confirmButtonText: "確定!",
                    //        closeOnConfirm: false,
                    //        cancelButtonText: "取消"
                    //    });
                    //    return false;
                    //} 





                    var PatQuery = $.parseJSON(json.PatQuery);

                    //$('#isFirst').val(PatQuery[0].isFirst);
                    $('#patNumber').val(PatQuery[0].patNumber);
                    $('#name').val(PatQuery[0].name);
                    $('#otherHaveReg').val(json.otherHaveReg);//紀錄他院有病例打開跨區訊息
                    $('#otherHospitalID').val(json.otherHospitalID);//紀錄他院有病例打開跨區訊息(分院ID)

                    $('#isFirst').val(json.isFirst);


                    //用來呈現打針又在其他醫院預約的訊息
                    if (json.assError == "N") {
                        if (json.NHIQP7012memo != "") {
                            // json.PatQuery
                            //$('#name').val(json.assErrorMessage);
                            //PatQuery[0].memo = json.assErrorMessage;
                            $('#NHIQP7012memo').val(json.NHIQP7012memo);
                        }
                    }
                    //-------------


                    if (PatQuery[0].isFirst == "N") {


                        //他院有病例打開跨區訊息
                        if (json.otherHaveReg == "Y" && $('#hospitalID').val() != json.hospitalID) {

                            $('.openModel').trigger("click");

                            $('#hospitalName').text(json.hospitalName);
                            $('#idNo').text(json.idNumber);
                        }
                        else {
                            //相同分院直接送出
                            $('#patNumber').val(PatQuery[0].patNumber);
                            form.submit();

                        }




                    }
                    else {
                        form.submit();
                    }



                    break;


                //區域
                case "getDistrict":
                    var District = $.parseJSON(json.district);
                 
                    $('#district').html('<option selected value="">選擇區/鎮/鄉</option>');

                    for (var i = 0; i < District.length; i++) {
                        var text = '<option value="' + District[i].text + '">' + District[i].text + '</option>';
                        $('#district').append(text);
                    }
                    if (custcallbk != null) {
                        custcallbk();
                    }
                    break;
                //區域-掛號查詢
                case "getDistrict4":
                    var District4 = $.parseJSON(json.district4);

                    $('#district4').html('<option selected value="">選擇區/鎮/鄉</option>');

                    for (var i = 0; i < District4.length; i++) {
                        var text = '<option value="' + District4[i].text + '">' + District4[i].text + '</option>';
                        $('#district4').append(text);
                    }
                    if (custcallbk != null) {
                        custcallbk();
                    }
                    break;
                //區域-視訊掛號的 輸入身分證那邊
                case "getDistrict5":
                    var District5 = $.parseJSON(json.district5);

                    $('#district5').html('<option selected value="">選擇區/鎮/鄉</option>');

                    for (var i = 0; i < District5.length; i++) {
                        var text = '<option value="' + District5[i].text + '">' + District5[i].text + '</option>';
                        $('#district5').append(text);
                    }
                    if (custcallbk != null) {
                        custcallbk();
                    }
                    break;
                case "getDistrict6":
                    var District6 = $.parseJSON(json.district6);

                    $('#district6').html('<option selected value="">選擇區/鎮/鄉</option>');

                    for (var i = 0; i < District6.length; i++) {
                        var text = '<option value="' + District6[i].text + '">' + District6[i].text + '</option>';
                        $('#district6').append(text);
                    }
                    if (custcallbk != null) {
                        custcallbk();
                    }
                    break;
                //
                //case "getprediagnostic":
                //    $("#getprediagnostic").on("click", function () {
                //        //var linkstr = "https://register1.cgmh.org.tw/refs/PatientHistory3_2.aspx?Id=" + json.strChtNo + "&Loc=" + json.strLoc + "&Bir=" + json.strBir;
                //        var linkstr = "http://lnkwww.cgmh.org.tw/page/operate.htm";
                //        window.open(linkstr);
                //    });
                //    break;

            }


        },
        complete: function () { //生成分頁條

        },
        error: function () {
            //alert("讀取錯誤!");
        }
    });

}

//判斷送出掛號查詢等證號判斷
function checkQuery(form) {

    var idType = $('input[name=idType]:checked', '#' + form).val();
    var isOK = "Y";
    var Msg = "";

    switch (idType) {

        case "1":
            var re = /^[0-9] .?[0-9]*/;//判斷字串是否為數字//判斷正整數/[1−9] [0−9]∗]∗/ 
            if (isNaN($("input[name$='idNumber']").val())) {

                Msg = "病歷號碼僅為純數字!";
                isOK = "N";
            }

            break;

        case "2":
            if (!checkID($("input[name$='idNumber']").val())) {
                Msg = "身份證字號驗證錯誤，請確認是否輸入錯誤!";
                isOK = "N";
            }

            break;

        case "4":
            if (!check_resident_ID($("input[name$='idNumber']").val()) && !check_resident_ID2021($("input[name$='idNumber']").val())) {
                Msg = "居留證號驗證錯誤，請確認是否輸入錯誤!";
                isOK = "N";
            }

            break;
    }

    if (isOK == "N") {
        swal({
            title: "系統訊息",
            text: Msg,
            type: "error",

            showCancelButton: false,
            confirmButtonColor: "#e7505a",
            confirmButtonText: "確定!",
            closeOnConfirm: false,
            cancelButtonText: "取消",
        });
        return false;
    }
    else {
        return true;
    }
}

//身分證字號判斷
function checkID(id) {
    tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
    A1 = new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
    A2 = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
    Mx = new Array(9, 8, 7, 6, 5, 4, 3, 2, 1, 1);

    if (id.length != 10) return false;
    i = tab.indexOf(id.charAt(0));
    if (i == -1) return false;
    sum = A1[i] + A2[i] * 9;

    for (i = 1; i < 10; i++) {
        v = parseInt(id.charAt(i));
        if (isNaN(v)) return false;
        sum = sum + v * Mx[i];
    }
    if (sum % 10 != 0) return false;
    return true;
}

//居留證驗證
function check_resident_ID(id) {

    //驗證填入身分證字號長度及格式
    if (id.length != 10) {

        return false;

    }

    //格式，用正則表示式比對第一個字母是否為英文字母

    if (isNaN(id.substr(2, 8)) ||

        (!/^[A-Z]$/.test(id.substr(0, 1))) || (!/^[A-Z]$/.test(id.substr(1, 1)))) {
        return false;
    }

    var idHeader = "ABCDEFGHJKLMNPQRSTUVXYWZIO"; //按照轉換後權數的大小進行排序
    //這邊把身分證字號轉換成準備要對應的

    id = (idHeader.indexOf(id.substring(0, 1)) + 10) +
        '' + ((idHeader.indexOf(id.substr(1, 1)) + 10) % 10) + '' + id.substr(2, 8);
    //開始進行身分證數字的相乘與累加，依照順序乘上1987654321

    snumber = parseInt(id.substr(0, 1)) +
        parseInt(id.substr(1, 1)) * 9 +
        parseInt(id.substr(2, 1)) * 8 +
        parseInt(id.substr(3, 1)) * 7 +
        parseInt(id.substr(4, 1)) * 6 +
        parseInt(id.substr(5, 1)) * 5 +
        parseInt(id.substr(6, 1)) * 4 +
        parseInt(id.substr(7, 1)) * 3 +
        parseInt(id.substr(8, 1)) * 2 +
        parseInt(id.substr(9, 1));
    checkNum = parseInt(id.substr(10, 1));

    //模數 - 總和/模數(10)之餘數若等於第九碼的檢查碼，則驗證成功
    // console.log(10 - snumber % 10);
    //20201103修正0的比對
    if (snumber % 10 == 0) { snumber = 10; } else { snumber = snumber % 10; }


   // if ((10 - snumber % 10) == checkNum) {
    if ((10 - snumber ) == checkNum) {

        return true;
    }

    else {

        return false;

    }

}
function check_resident_ID2021(id2) {
    //2021新式居留證驗證方式
    //驗證填入身分證字號長度及格式
    if (id2.length != 10) {

        return false;

    }

    //格式，用正則表示式比對第一個字母是否為英文字母

    if (isNaN(id2.substr(2, 8)) ||

        (!/^[A-Z]$/.test(id2.substr(0, 1))) || (!/^[8-9]$/.test(id2.substr(1, 1)))) {
        return false;
    }

    var idHeader = "ABCDEFGHJKLMNPQRSTUVXYWZIO"; //按照轉換後權數的大小進行排序
    //這邊把身分證字號轉換成準備要對應的

    //id = (idHeader.indexOf(id.substring(0, 1)) + 10) +
    //    '' + ((idHeader.indexOf(id.substr(1, 1)) + 10) % 10) + '' + id.substr(2, 8);
    id2 = (idHeader.indexOf(id2.substring(0, 1)) + 10) +
        '' + id2.substr(1, 9);
    //id = (idHeader.indexOf(id.substring(0, 1)) + 10) +
    //    '' + (parseInt((id.substr(1, 1))+ 10) % 10) + '' + id.substr(2, 8);
    //開始進行身分證數字的相乘與累加，依照順序乘上1987654321

    snumber2 = parseInt(id2.substr(0, 1)) +
        parseInt(id2.substr(1, 1)) * 9 +
        parseInt(id2.substr(2, 1)) * 8 +
        parseInt(id2.substr(3, 1)) * 7 +
        parseInt(id2.substr(4, 1)) * 6 +
        parseInt(id2.substr(5, 1)) * 5 +
        parseInt(id2.substr(6, 1)) * 4 +
        parseInt(id2.substr(7, 1)) * 3 +
        parseInt(id2.substr(8, 1)) * 2 +
        parseInt(id2.substr(9, 1));
    checkNum2 = parseInt(id2.substr(10, 1));

    //模數 - 總和/模數(10)之餘數若等於第九碼的檢查碼，則驗證成功
    // console.log(10 - snumber % 10);
    //20201103修正0的比對
    if (snumber2 % 10 == 0) { snumber2 = 10; } else { snumber2 = snumber2 % 10; }


    // if ((10 - snumber % 10) == checkNum) {
    if ((10 - snumber2) == checkNum2) {

        return true;
    }

    else {

        return false;

    }

}


function setLoadPlayer(view) {
    if (view == 'none') {
        $.unblockUI();
    }
    else {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}





var keyStr = "ABCDEFGHIJKLMNOP" +
    "QRSTUVWXYZabcdef" +
    "ghijklmnopqrstuv" +
    "wxyz0123456789+/" +
    "=";

const fileUploader1 = document.querySelector('[data-target="file-uploader1"]');
fileUploader1.addEventListener("change", handleFileUpload1);
var file_update1;
const fileUploader2 = document.querySelector('[data-target="file-uploader2"]');
fileUploader2.addEventListener("change", handleFileUpload2);
var file_update2;
const fileUploader3 = document.querySelector('[data-target="file-uploader3"]');
fileUploader3.addEventListener("change", handleFileUpload3);
var file_update3;


//------------------------------------------------------
async function handleFileUpload1(e) {
    try {
        const file = e.target.files[0];
        const beforeUploadCheck = await beforeUpload(file);
        if (!beforeUploadCheck.isValid) throw beforeUploadCheck.errorMessages;

        const arrayBuffer = await getArrayBuffer(file);
        file_update1 = Array.from(new Uint8Array(arrayBuffer));
        $('#upfile1').attr("value", file_update1.toString());
        //console.log("upfile1: ", file_update1.toString());
    } catch (error) {
        alert(error);
        e.target.value = '';
        //console.log("Catch Error: ", error);
    } finally {
       // e.target.value = '';  // reset input file
       // setUploading(false);
    }
}
async function handleFileUpload2(e) {
    try {
        const file = e.target.files[0];
        const beforeUploadCheck = await beforeUpload(file);
        if (!beforeUploadCheck.isValid) throw beforeUploadCheck.errorMessages;
        const arrayBuffer = await getArrayBuffer(file);
        file_update2 = Array.from(new Uint8Array(arrayBuffer));
        $('#upfile2').attr("value", file_update2.toString());
    } catch (error) {
        alert(error);
        e.target.value = '';
        //console.log("Catch Error: ", error);
    } finally {
        // e.target.value = '';  // reset input file
        // setUploading(false);
    }
}
async function handleFileUpload3(e) {
    try {
        const file = e.target.files[0];
        const beforeUploadCheck = await beforeUpload(file);
        if (!beforeUploadCheck.isValid) throw beforeUploadCheck.errorMessages;

        const arrayBuffer = await getArrayBuffer(file);
        file_update3 = Array.from(new Uint8Array(arrayBuffer));
        $('#upfile3').attr("value", file_update3.toString());
    } catch (error) {
        alert(error);
        e.target.value = '';
        //console.log("Catch Error: ", error);
    } finally {
        // e.target.value = '';  // reset input file
        // setUploading(false);
    }
}

// STEP 3: change file object into ArrayBuffer
function getArrayBuffer(fileObj) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        // Get ArrayBuffer when FileReader on load
        reader.addEventListener("load", () => {
            resolve(reader.result);
        });

        // Get Error when FileReader on error
        reader.addEventListener("error", () => {
            reject("error occurred in getArrayBuffer");
        });

        // read the blob object as ArrayBuffer
        // if you nedd Base64, use reader.readAsDataURL
        reader.readAsArrayBuffer(fileObj);
    });
}
function beforeUpload(fileObject) {
    return new Promise(resolve => {
        const validFileTypes = ["image/jpg", "image/jpeg", "image/png"];
        const isValidFileType = validFileTypes.includes(fileObject.type);
        let errorMessages = [];

        if (!isValidFileType) {
            errorMessages.push("You can only upload JPG or PNG file!");
        }

        const isValidFileSize = fileObject.size / 1024 / 1024 < 2;
        if (!isValidFileSize) {
            errorMessages.push("Image must smaller than 2MB!");
        }

        resolve({
            isValid: isValidFileType && isValidFileSize,
            errorMessages: errorMessages.join("\n")
        });
    });
}

//// STEP 4: upload file throguth AJAX
//// - use "new Uint8Array()"" to change ArrayBuffer into TypedArray
//// - TypedArray is not a truely Array,
////   use "Array.from()" to change it into Array
//function uploadFileAJAX(arrayBuffer) {
//    // correct it to your own API endpoint
//    return fetch("https://jsonplaceholder.typicode.com/posts/", {
//        headers: {
//            version: 1,
//            "content-type": "application/json"
//        },
//        method: "POST",
//        body: JSON.stringify({
//            imageId: 1,
//            icon: Array.from(new Uint8Array(arrayBuffer))
//        })
//    })
//        .then(res => {
//            if (!res.ok) {
//                throw res.statusText;
//            }
//            return res.json();
//        })
//        .then(data => data)
//        .catch(err => console.log("err", err));
//}

function ab2str_16(buf) {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
}
//function ab2str_32(buf) {
//    return String.fromCharCode.apply(null, new Uint32Array(buf));
//}
function str2ab(str) {
    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

function Utf8ArrayToStr(array) {
    var out, i, len, c;
    var char2, char3;

    out = "";
    len = array.length;
    i = 0;
    while (i < len) {
        c = array[i++];
        switch (c >> 4) {
            case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                // 0xxxxxxx
                out += String.fromCharCode(c);
                break;
            case 12: case 13:
                // 110x xxxx   10xx xxxx
                char2 = array[i++];
                out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                break;
            case 14:
                // 1110 xxxx  10xx xxxx  10xx xxxx
                char2 = array[i++];
                char3 = array[i++];
                out += String.fromCharCode(((c & 0x0F) << 12) |
                    ((char2 & 0x3F) << 6) |
                    ((char3 & 0x3F) << 0));
                break;
        }
    }

    return out;
}
//function decodeUtf8(arrayBuffer) {
//    var result = "";
//    var i = 0;
//    var c = 0;
//    var c1 = 0;
//    var c2 = 0;

//    var data = new Uint8Array(arrayBuffer);

//    // If we have a BOM skip it
//    if (data.length >= 3 && data[0] === 0xef && data[1] === 0xbb && data[2] === 0xbf) {
//        i = 3;
//    }

//    while (i < data.length) {
//        c = data[i];

//        if (c < 128) {
//            result += String.fromCharCode(c);
//            i++;
//        } else if (c > 191 && c < 224) {
//            if (i + 1 >= data.length) {
//                throw "UTF-8 Decode failed. Two byte character was truncated.";
//            }
//            c2 = data[i + 1];
//            result += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
//            i += 2;
//        } else {
//            if (i + 2 >= data.length) {
//                throw "UTF-8 Decode failed. Multi byte character was truncated.";
//            }
//            c2 = data[i + 1];
//            c3 = data[i + 2];
//            result += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
//            i += 3;
//        }
//    }
//    return result;
//}
function decodeUtf8(arrayBuffer) {
    var result = "";
    var i = 0;
    var c = 0;
    var c1 = 0;
    var c2 = 0;

    var data = new Uint8Array(arrayBuffer);

    // If we have a BOM skip it
    if (data.length >= 3 && data[0] === 0xef && data[1] === 0xbb && data[2] === 0xbf) {
        i = 3;
    }

    while (i < data.length) {
        c = data[i];

        if (c < 128) {
            result += String.fromCharCode(c);
            i++;
        } else if (c > 191 && c < 224) {
            if (i + 1 >= data.length) {
                throw "UTF-8 Decode failed. Two byte character was truncated.";
            }
            c2 = data[i + 1];
            result += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        } else {
            if (i + 2 >= data.length) {
                throw "UTF-8 Decode failed. Multi byte character was truncated.";
            }
            c2 = data[i + 1];
            c3 = data[i + 2];
            result += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
    return result;
}

function encodeUTF8(s) {
    var i = 0, bytes = new Uint8Array(s.length * 4);
    for (var ci = 0; ci != s.length; ci++) {
        var c = s.charCodeAt(ci);
        if (c < 128) {
            bytes[i++] = c;
            continue;
        }
        if (c < 2048) {
            bytes[i++] = c >> 6 | 192;
        } else {
            if (c > 0xd7ff && c < 0xdc00) {
                if (++ci >= s.length)
                    throw new Error('UTF-8 encode: incomplete surrogate pair');
                var c2 = s.charCodeAt(ci);
                if (c2 < 0xdc00 || c2 > 0xdfff)
                    throw new Error('UTF-8 encode: second surrogate character 0x' + c2.toString(16) + ' at index ' + ci + ' out of range');
                c = 0x10000 + ((c & 0x03ff) << 10) + (c2 & 0x03ff);
                bytes[i++] = c >> 18 | 240;
                bytes[i++] = c >> 12 & 63 | 128;
            } else bytes[i++] = c >> 12 | 224;
            bytes[i++] = c >> 6 & 63 | 128;
        }
        bytes[i++] = c & 63 | 128;
    }
    return bytes.subarray(0, i);
}

// Unmarshals a string from an Uint8Array.
function decodeUTF8(bytes) {
    var i = 0, s = '';
    while (i < bytes.length) {
        var c = bytes[i++];
        if (c > 127) {
            if (c > 191 && c < 224) {
                if (i >= bytes.length)
                    throw new Error('UTF-8 decode: incomplete 2-byte sequence');
                c = (c & 31) << 6 | bytes[i++] & 63;
            } else if (c > 223 && c < 240) {
                if (i + 1 >= bytes.length)
                    throw new Error('UTF-8 decode: incomplete 3-byte sequence');
                c = (c & 15) << 12 | (bytes[i++] & 63) << 6 | bytes[i++] & 63;
            } else if (c > 239 && c < 248) {
                if (i + 2 >= bytes.length)
                    throw new Error('UTF-8 decode: incomplete 4-byte sequence');
                c = (c & 7) << 18 | (bytes[i++] & 63) << 12 | (bytes[i++] & 63) << 6 | bytes[i++] & 63;
            } else throw new Error('UTF-8 decode: unknown multibyte start 0x' + c.toString(16) + ' at index ' + (i - 1));
        }
        if (c <= 0xffff) s += String.fromCharCode(c);
        else if (c <= 0x10ffff) {
            c -= 0x10000;
            s += String.fromCharCode(c >> 10 | 0xd800)
            s += String.fromCharCode(c & 0x3FF | 0xdc00)
        } else throw new Error('UTF-8 decode: code point 0x' + c.toString(16) + ' exceeds UTF-16 reach');
    }
    return s;
}
//-----------------------------------------------

//function handleFileUpload(event) {
//         // console.log(event);
//         if (event.target.files.length == 0) { return; }
//         var selectedFile = event.target.files[0];
//         var reader = InitExcelFr(function (res) {
//            // console.log(res);
//             var byte = res;            
//            // update1 = byte;
//           //  var enc = new TextDecoder("utf-8");
//            // var arr = new Uint8Array(byte);
//             // update1 = enc.decode(arr);
//             update1 = ab2str_16(res);
//          //   var enc = new TextDecoder("utf-8");
//           //  update1 = enc.decode(byte);
//         });
//         reader.readAsBinaryString(selectedFile);
//     }
//function InitExcelFr(loadCb) {
//    var reader = new FileReader();
//    reader.onload = function (event) {
//        var data = event.target.result;

//        loadCb(data);
//    }
//    reader.onerror = function (e) {
//        if (vueloader != null) { vueloader.close(); }
//        app.$alert("檔案讀取失敗:" + event.target.error.code);
//    }
//    return reader;
//}